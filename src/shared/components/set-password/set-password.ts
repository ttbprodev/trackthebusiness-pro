import { Component } from '@angular/core';
import { Service } from '../../utils/service';
import { NavController } from 'ionic-angular';
import { LoginPage } from '../login/login';

/**
 * Generated class for the SetPasswordComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'set-password',
  templateUrl: 'set-password.html'
})
export class SetPassword {

  text: string;
  email: any;
  empId: any;
  roles: any;
  password: any;
  password2: any;
  formData: any;

  constructor(public service: Service,
    public navCtrl: NavController) {
    console.log('Hello SetPasswordComponent Component');
    if (location.search != "") {
      this.email = location.search.split('?')[1].split("&")[0].split("emaill=")[1];
      this.empId = location.search.split('?')[1].split("&")[1].split("empId=")[1];
      this.roles = location.search.split('?')[1].split("&")[2].split("roles=")[1];
    }
  }

  createAccount() {
    this.formData = new FormData();
    this.formData.append('email', this.email);
    this.formData.append('userId', this.empId);
    this.formData.append('roles', this.roles);
    // let userInfo = [{email: this.email, password: this.password, userId: this.empId}];
    if (this.password === this.password2 && this.password != "" && this.password2 != "" && this.password != undefined && this.password2 != undefined){
      this.formData.append('password', this.password);
      this.service.editingUser(this.formData).subscribe(
        response => {
          console.log("Password setting response :"+response);
          alert("Account created  Successfully. Login into your account.");
          this.navCtrl.setRoot(LoginPage);
        },
        error => {
          // console.log("Password setting error :"+error);
          alert("Invalid details..");
        }
      );
    } else if (this.password == "" || this.password2 == ""){
      alert("Password fields shouldn't be empty");
    } 
    else {
      alert("Passwords didn't match. Re-enter Passwords!");
    }
  }
}
