import { Component } from "@angular/core";
import { NavController } from "ionic-angular";
import { Service } from "../../utils/service";
import { LoginPage } from "../login/login";

@Component({
    selector: 'app-signup',
    templateUrl: './signup.html'
})
export class Signup {
    userPlan: any = "2";
    userName: any;
    userEmail: any = "managebots123@gmail.com";
    password: any = "password";
    phoneNumber: any = "9676769660";
    city: any;
    pincode: any;
    formData: any;
    constructor(
        public navCtrl: NavController,
        public service: Service
    ) {

    }
    selectUserPlan() {
        console.log("selected user plan is: " + this.userPlan);
    }
    userSignup() {
        this.formData = new FormData;
        // this.formData.append("userName", this.userName);
        this.formData.append("email", this.userEmail);
        this.formData.append("password", this.password);
        this.formData.append("mobile", this.phoneNumber);
        // this.formData.append("city", this.city);
        // this.formData.append("pincode", this.pincode);
        this.formData.append("userPlan", this.userPlan);
        this.formData.append("roles", "1");
        this.service.registerUser(this.formData).subscribe(
            res => {
                console.log(res);
            },
            err => {
                console.log(err);
            }
        )
    }
    openLoginPage() {
        this.navCtrl.setRoot(LoginPage);
    }
}