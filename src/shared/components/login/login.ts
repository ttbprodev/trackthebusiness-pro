import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Service } from '../../utils/service';
import { DataStore } from '../../../shared/utils/data-store';
import { DashboardView } from '../../../features/dashboard-view/dashboard-view';
import { Registration } from '../registration/registration';
import { BusinessView } from '../../../features/business/business-view/business-view';
import { Signup } from '../signup/signup';

@Component({
    selector: 'page-login',
    templateUrl: 'login.html',
})
export class LoginPage {
    email: any = "managebots@gmail.com";
    password: any = "password";
    loginDetails: any = [];
    formData: any;
    csrf_token: any;
    csrf_header: any;
    businessId: any;

    constructor(public navCtrl: NavController,
        public navParams: NavParams,
        public service: Service,
        private dataStore: DataStore) {
        // this.getToken();
    }

    logForm() {
        this.formData = new FormData();
        this.formData.append('email', this.email);
        this.formData.append('password', this.password);
        console.log(this.formData);
        this.loginDetails = { "email": this.email, "password": this.password };
        this.service.postLoginDetails(this.formData).subscribe(
            response => {
                if (response.status == 200) {
                    let res = response.json();
                    this.navCtrl.setRoot(DashboardView);
                    // this.dataStore.currentHeaderTitle = "Dashboard";
                    // window.sessionStorage.setItem('userData', res);
                    this.dataStore.loginMetaData = res;
                    this.dataStore.userRole = res.userProfiles[0].id;
                    this.dataStore.userPlan = res.userPlanObj[0].id;
                    this.getBusinessList();
                    // this.navCtrl.setRoot(DashboardView);
                    // window.sessionStorage.setItem('userData', res);
                    // Venki: The below logic of role management - yet to be done.   

                    // var role = (JSON.parse(result.message)).roles;
                    // if (role == 1) {
                    //     if ((JSON.parse(result.message)).emailAddress != undefined) {
                    //         var mail = (JSON.parse(result.message)).emailAddress;
                    //         this.navCtrl.setRoot(BusinessView, mail);
                    //     } else if ((JSON.parse(result.message)).email != undefined) {
                    //         var mail = (JSON.parse(result.message)).email;
                    //         this.navCtrl.setRoot(BusinessView, mail);
                    //     }
                    // }
                    // else {
                    //     var role = (JSON.parse(result.message)).roles;
                    //     this.navCtrl.setRoot(ProductList, result);
                    //     alert("Sales Page");
                    //     console.log("Sales Page");
                    // }
                }
                else {
                    alert("Please enter correct credentials !!");
                    // this.navCtrl.setRoot(LoginPage);
                }
            },
            error => {
                alert("Error While login !!" + error);
                // this.navCtrl.setRoot(LoginPage);
            });
    }
    // getToken() {
    //     this.service.get().subscribe(
    //         response => {
    //             console.log(response);
    //             var data = response.json();
    //             // this.csrf_token = data["csrf_token"];
    //             this.dataStore.csrf_token = data["csrf_token"];
    //             this.csrf_header = data.csrf_header;
    //         });
    // }
    registerUser() {
        this.navCtrl.setRoot(Signup);
    }
    getBusinessList() {
        this.service.getBusinessList().subscribe(
            response => {
                this.dataStore.businessList = response.json();
                if (this.dataStore.businessList.length == 0) {
                    //   this.navCtrl.setRoot(BusinessView);
                    this.navCtrl.setRoot(DashboardView);
                }
                else {
                    let defaultBusinessId = this.dataStore.businessList[0].businessId;
                    this.navCtrl.setRoot(DashboardView, { businessId: defaultBusinessId });
                }
            }, error => {
                alert("Service Error");
                console.log(error);
            });
    }
}