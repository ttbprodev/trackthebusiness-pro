import { Component } from "@angular/core";
import { LoginPage } from "../login/login";
import { NavController } from "ionic-angular";
import { Service } from "../../utils/service";
import { Signup } from "../signup/signup";

@Component({
    selector: 'registration-page',
    templateUrl: './registration.html'
})
export class Registration {
    userName: any = "venki";
    email: any = "saivenkat.554@gmail.com";
    phoneNumber: any = "9676769660";
    city: any = "Bangalore";
    pincode: any = "560016";
    userDetails = [];
    formData: any;

    constructor(
        public navCtrl: NavController,
        public service: Service,
    ) { }

    registerAccount() {
        this.userDetails = [{ UserName: this.userName, Email: this.email, PhoneNumber: this.phoneNumber, City: this.city, Pincode: this.pincode }];
        console.log(this.userDetails);
        // this.navCtrl.setRoot(LoginPage);
        this.formData = new FormData;
        this.formData.append("userName", this.userName);
        this.formData.append("email", this.email);
        this.formData.append("phoneNumber", this.phoneNumber);
        this.formData.append("city", this.city);
        this.formData.append("pincode", this.pincode);
        this.formData.append("roles", "1");

        this.service.registerUser(this.formData).subscribe(
            res => {
                console.log("Response: " + res);
                if (res.status == 200) {
                    alert(res.json().message);
                    // this.navCtrl.setRoot(LoginPage);
                }
            },
            err => {
                console.log("Error: " + err);
            }
        )
        // alert("Users Registered Successfully..!");
    }
    loginPage() {
        // this.navCtrl.setRoot(LoginPage);
    }
    openSignUp() {
        this.navCtrl.setRoot(Signup);
    }
}