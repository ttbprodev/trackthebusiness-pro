import { Component } from '@angular/core';
// import {NavController, Popover,PopoverController} from 'ionic-angular';
import { PopoverController, NavController, ViewController, AlertController } from "ionic-angular";
import { DashboardView } from '../../../../features/dashboard-view/dashboard-view';
import { DataStore } from '../../../utils/data-store';
import { LoginPage } from '../../login/login';

@Component({
  selector: 'header-popover',
  templateUrl: 'header-popover.html'
  
})
export class PopoverPage {
  
  constructor(public popoverCtrl: PopoverController,
    public navCtrl: NavController,
   public viewctrl:ViewController, 
   public dataStore: DataStore,
   private alertCtrl: AlertController) {}


  SignOutPage(){
    this.navCtrl.setRoot(DashboardView);
  }

  signoutConfirm() {
    let alert = this.alertCtrl.create({
      title: 'Confirm Signout!',
      message: 'Are you sure want to signout?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.dataStore.currentHeaderTitle = '';
            this.navCtrl.setRoot(LoginPage);
          }
        }
      ]
    });
    alert.present();
  }

  EditProfile(){
    this.navCtrl.setRoot(DashboardView);
  }

  close() {
    this.viewctrl.dismiss();
  }
}
