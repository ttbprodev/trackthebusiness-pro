import { Component, Input } from "@angular/core";
import { PopoverController } from "ionic-angular";
import { PopoverPage } from '../header/header-popover/header-popover';
import { SettingPopover } from "./setting-popover/setting-popover";
import { Service } from "../../utils/service";
import { DataStore } from "../../utils/data-store";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CompleteTestService } from "../../utils/complete-test-service/complete-test-service";
import { AutoCompleteService } from "ionic2-auto-complete";
import { RequestOptions, Headers, Http } from "@angular/http";
@Component({
    selector: 'app-header',
    templateUrl: './app-header.html'
})
export class AppHeader {

    @Input() headerTitle: string;
    private searchQuery: string = '';
    private items: string[];
    showList: boolean = false;
    private options: string[];
    selected: string;
    option:any;
    constructor(public popoverCtrl: PopoverController,
        public service: Service,
        public dataStore: DataStore,

    ) {
        this.getOption();
        this.initializeItems();
    }
    initializeItems() {
        this.items = [
            'Amsterdam',
            'Berlin',
            'Bueno Aires',
            'Madrid',
            'Paris'
        ];
    }
    getOption()
    {
        this.options = [
            'All',
            'Branch',
            'Employee',
            'Products',
            'Contact',
            'Order',
            'Sales'
        ];
    }
    profilePopover(myEvent) {
        let popover = this.popoverCtrl.create(PopoverPage);
        let ev = {
            target: {
                getBoundingClientRect: () => {
                    return {
                        top: '50',
                        left: '30',
                        height: '200px',
                        width: '80',
                        bottom: '70',
                    };
                }
            }
        };

        popover.present({ ev });
        //   ev: myEvent;
        // popover.present({
        //     ev: myEvent
        // });
    }
    settingPopover(myEvent) {
        let popover = this.popoverCtrl.create(SettingPopover);
        popover.present({
            ev: myEvent
        });
    }
    getItems(ev: any) {
        // Reset items back to all of the items
        this.initializeItems();

        // set val to the value of the searchbar
        let val = ev.target.value;

        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {

            // Filter the items
            this.items = this.items.filter((item) => {
                return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
            });

            // Show the results
            this.showList = true;
        } else {

            // hide the results when the query is empty
            this.showList = false;
        }
    }
    getallfilter(event) {

    }
}
