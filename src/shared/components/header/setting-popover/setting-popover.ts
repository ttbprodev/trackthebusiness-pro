import { Component } from '@angular/core';
// import {NavController, Popover,PopoverController} from 'ionic-angular';
import { PopoverController, NavController, ViewController } from "ionic-angular";
import { DashboardView } from '../../../../features/dashboard-view/dashboard-view';


@Component({
  selector: 'setting-popover',
  templateUrl: 'setting-popover.html',


})
export class SettingPopover {
  constructor(public popoverCtrl: PopoverController,
    public navCtrl: NavController,
    public viewctrl: ViewController) { }


  gotohelp() {
    this.navCtrl.setRoot(DashboardView);
  }
  gotosupport() {
    this.navCtrl.setRoot(DashboardView);
  }
  close() {
    this.viewctrl.dismiss();
  }
}
