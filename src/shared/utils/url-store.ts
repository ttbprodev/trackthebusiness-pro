/**
 * Author: Easwar K
 *
 * About the file:
 *
 * This file should contain all the strings used in the application.
 * There should not be any hardcoded strings in the application.
 * The variable scope should always public static.
 * The variable name should be capital letters and can be seperated by _.
 * Access the url from other classes like URLStore.GET_PRODUCTS
 * First segment of the variable name should specify the methos. This can be GET, POST, DELETE and so on.
 *
 */


export class URLStore {
    // Products
    public static GET_PRODUCTS = 'product/getAll/';
    public static GET_PRODUCT = 'product/get/';
    public static Add_CATEGORY = 'product/addCategory';
    public static Add_SUBCATEGORY = 'product/addSubCategory';
    public static GET_CATEGORY = 'product/getAllCategories/';
    public static GET_SUB_CATEGORY = '/product/getAllSubCategories/';
    public static GET_SUBCATEGORY = 'product/getAllSubCategories/';
    public static POST_PRODUCT = 'product/addAndUpdate';
    public static POST_MULTIPLE_PRODUCTS = 'product/importProducts/';
    public static UPDATE_PRODUCT = 'product/addAndUpdate';
    public static DELETE_PRODUCT = 'product/delete';
    public static FILTER_BY_PRODUCT = "product/filterByProducts";
    public static PRODUCT_NAME = "product/getProductName";
    public static PRODUCTS_COUNTER = "product/getProductsCount/";
    public static MULTIPLE_DELETE_PRODUCT = 'product/deleteMultipleProducts/';
    public static GET_PRODUCTS_WITH_CATEGORIES = 'product/getAllCategoriesAndCategoriesAndProducts/';

    // Business
    public static POST_BUSINESS = 'business/addOrUpdate';
    public static GET_BUSINESS = 'business/get/';
    public static GET_BUSINESS_LIST = 'business/getAll/';
    public static DELETE_BUSINESS = 'business/delete/';
    public static ALL_BUSINESS_SEARCH_NAME = 'business/getAllSearchByName/';

    //Branch
    public static POST_BRANCH = 'branch/addOrUpdate';
    public static GET_BRANCHES_LIST = 'branch/getAll/';
    public static GET_BRANCH = 'branch/get/';
    public static DELETE_BRANCH = 'branch/delete/';
    public static DELETE_MULTIPLE_BRANCHES = 'branch/deleteMultipleBranches/';
    public static POST_MULTIPLE_BRANCH = 'branch/importBranches';

    // Users
    public static POST_USER = 'user/addUser';
    public static GET_USERS = 'user/getAllUsers/';
    public static DELETE_USER = 'user/delete/';
    public static EDIT_USER = 'user/addOrUpdate';
    public static LOGIN_TOKEN = 'getCsrfToken';
    public static ACCESS_TOKEN = "?access_token=";
    public static REFRESH_TOKEN = "?grant_type=refresh_token&refresh_token=";

    // Employees

    public static MULTIPLE_ADD_EMPLOYEE = 'employee/addMultipleEmployees';
    public static GET_EMPLOYEE = 'employee/get/';
    public static GET_EMPLOYEE_LIST = 'employee/getAllEmployeesBranchWise/';
    public static DELETE_EMPLOYEE = 'employee/delete/';
    public static EDIT_EMPLOYEE = 'employee/addOrUpdate/';
    public static POST_EMPLOYEE = 'employee/addOrUpdate';
    public static MULTIPLE_DELETE_EMPLOYEE = 'employee/deleteMultipleUsers/';


    //Order
    public static GET_ORDERS = 'myorder/orderSummary/';
    public static EDII_ORDERS = 'myorder/get/';
    public static DELETE_ORDERS = 'myorder/delete';
    public static UPDATE_ORDERS = 'myorder/addOrUpdate';
    public static ADD_ORDERS = 'myorder/addOrUpdate';
    public static FILTER_BY_ORDERS = "myorder/filterByOrders";
    public static FILTER_BY_RANGE = "myorder/filterByAll";
    public static FILTER_BY_MIN_MAX_QTY_PRICE = "myorder/minAndMaxPriceAndQuantity";
    public static ORDER_HISTORY = "myorder/orderHistory/";
    public static FILTER_BY_RANGE_QTY = "myorder/minAndMaxPriceAndQuantity/";
    public static orderCounter = "myorder/getOrdersCount";
    public static PRICETAG = "product/addAditionalPriceAndUnitPrice";
    public static DELETE_MULTIPLE_ORDERS = 'myorder/deleteMultipleOrders/';
    
    // Reports
    public static GET_ORDERS_REPORTS = 'myorder/getAllOrdersDateWise/';
    public static GET_SALES_REPORTS = 'mysales/getAllSalesDateWise/';

    // Customer
    public static POST_CUSTOMER = 'customer/addOrUpdate/';
    public static GET_CUSTOMER_LIST = 'customer/getAll/';
    public static GET_CUSTOMER = 'customer/get/';
    public static UPDATE_CUSTOMER = 'customer/addOrUpdate';
    public static DELETE_CUSTOMER = 'customer/delete/';
    public static ADD_MULTIPLE_CUSTOMER = 'customer/importCustomers';
    public static DELETE_MULTIPLE_CUSTOMER = 'customer/deleteMultipleCustomers/';

    //DashBoard
    public static TOTAL_PRODUCTS = 'product/getProductsCount';
    public static TOTAL_BRANCHES = 'branch/getBranchesCount';
    public static TOTAL_Employees = 'employee/getEmployeesCounts';
    public static TOTAL_SALES = 'myorder/getAllOrdersCount';
    public static GET_DATEWISE_PRODUCTS = 'product/getProductsDateWise';
    public static GET_DATEWISE_ORDERS = 'myorder/getOrdersDateWise';
    public static GET_DATEWISE_BRANCHES = 'branch/getBranchesDateWise';
    public static TOTAL_Employees_BRANCHWISE = 'employee/getEmployeesCounts';
    public static TOTAL_PRODUCTS_BRANCHWISE = 'product/getProductsCount';

    public static GET_COUNTRIES = 'user/getCountries';
    public static GET_STATES = 'user/getStates/';
    public static GET_CITIES = 'user/getCities/';

    //sales
    public static ADD_POS = 'mysales/addorUpdate';
    public static GET_BILLNO = 'mysales/getBillNo';
    public static GET_INVOICE = 'mysales/salesSummary/';

}


