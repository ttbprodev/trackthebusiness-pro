import { AutoCompleteService } from 'ionic2-auto-complete';
import { Http, RequestOptions, Headers } from '@angular/http';
import { Injectable } from "@angular/core";
import 'rxjs/add/operator/map'
import { DataStore } from '../data-store';
import { Contacts, Contact, ContactField, ContactName, ContactFieldType, IContactFindOptions, ContactFindOptions } from '@ionic-native/contacts';
/*
  Generated class for the CompleteTestServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CompleteTestService implements AutoCompleteService {
  requestOptions = new RequestOptions({
    headers: new Headers(),
    withCredentials: true
  });
  labelAttribute = "name";
  formValueAttribute = "numericCode";
  constructor(public dataStore: DataStore,
    private contacts: Contacts,
    private http: Http) {

  }
  getResults(keyword: string) {
    console.log(this.dataStore.filteredContact);
    this.dataStore.filteredContact.forEach(contactSearch => {
      const option = new ContactFindOptions();
      option.filter = keyword;
      option.hasPhoneNumber = true;
      const fields: ContactFieldType[] = ['name'];
      contactSearch = this.contacts.find(fields, option);
      console.log(contactSearch);
      return contactSearch;

    });

    //     getResults(keyword:string) {
    //   return this.http.get("http://166.62.44.165:8080/TrackTheBusiness/business/getAllSearchByName/"+keyword+"/"+ this.dataStore.selectedBusiness.businessId)
    //     .map(
    //       result =>
    //       {
    //         console.log(result);
    //         return result.json()
    //           .filter(item => item.name.toLowerCase().startsWith(keyword.toLowerCase()) )
    //       });
    // }
  }
}