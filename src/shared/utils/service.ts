/**
 * Author: Easwar K
 *
 * About the file:
 *
 * This file is to handle web services.
 *
 */
import { Injectable } from "@angular/core";
import { Http, RequestOptions, Headers } from "@angular/http";
import { URLStore } from "../utils/url-store";
import { Constants } from "../utils/constants";
import { DataStore } from "../utils/data-store";


@Injectable()
export class Service {
    requestOptions = new RequestOptions({
        headers: new Headers(),
        withCredentials: true
    });
    constructor(private http: Http,
        private dataStore: DataStore) { }


    // Product API
    addProduct(ProductDetails: any) {

        return this.http.post(Constants.BASE_URL + URLStore.POST_PRODUCT, ProductDetails, this.requestOptions);
    }
    getProduct() {
        return this.http.get(Constants.BASE_URL + URLStore.GET_PRODUCTS + this.dataStore.selectedBusiness.businessId, this.requestOptions);
    }
    getProductAndCategories() {
        return this.http.get(Constants.BASE_URL + URLStore.GET_PRODUCTS_WITH_CATEGORIES + this.dataStore.selectedBusiness.businessId, this.requestOptions);
    }

    getProductById(id) {

        return this.http.get(Constants.BASE_URL + URLStore.GET_PRODUCT + id, this.requestOptions);
    }
    deleteProduct(id) {

        return this.http.get(Constants.BASE_URL + URLStore.DELETE_PRODUCT + "/" + id, this.requestOptions);
    }
    updateProduct(id, body) {

        return this.http.post(Constants.BASE_URL + URLStore.UPDATE_PRODUCT + "/" + id, body, this.requestOptions);
    }
    getProductByName(productName) {

        return this.http.get(Constants.BASE_URL + URLStore.FILTER_BY_PRODUCT + "/" + productName + "/" + this.dataStore.selectedBusiness.businessId, this.requestOptions);
    }
    getProductName(productName) {
        return this.http.get(Constants.BASE_URL + URLStore.PRODUCT_NAME + "/" + productName + "/" + this.dataStore.selectedBusiness.businessId, this.requestOptions);
    }
    addMultipleProducts(jsonProduct) {
        return this.http.post(Constants.BASE_URL + URLStore.POST_MULTIPLE_PRODUCTS, jsonProduct, this.requestOptions);
    }
    deleteMultipleProducts(selectedProductsIds) {
        return this.http.get(Constants.BASE_URL + URLStore.MULTIPLE_DELETE_PRODUCT + selectedProductsIds, this.requestOptions);
    }
    addCategory(CategoryDetails) {
        return this.http.post(Constants.BASE_URL + URLStore.Add_CATEGORY, CategoryDetails, this.requestOptions);
    }
    addSubCategory(SubCategoryDetails) {
        return this.http.post(Constants.BASE_URL + URLStore.Add_SUBCATEGORY, SubCategoryDetails, this.requestOptions);
    }
    getCategory() {
        return this.http.get(Constants.BASE_URL + URLStore.GET_CATEGORY + this.dataStore.selectedBusiness.businessId, this.requestOptions);
    }
    getSubCategory(categoryId) {
        return this.http.get(Constants.BASE_URL + URLStore.GET_SUBCATEGORY + categoryId, this.requestOptions);
    }
    getProductCount() {
        return this.http.get(Constants.BASE_URL + URLStore.PRODUCTS_COUNTER + this.dataStore.selectedBusiness.businessId, this.requestOptions);
    }
    addNewBusiness(businessDetails) {

        return this.http.post(Constants.BASE_URL + URLStore.POST_BUSINESS, businessDetails, this.requestOptions);
    }
    getBusiness(businessId) {

        return this.http.get(Constants.BASE_URL + URLStore.GET_BUSINESS + businessId, this.requestOptions);
    }
    deleteBusiness(businessId) {

        return this.http.get(Constants.BASE_URL + URLStore.DELETE_BUSINESS + businessId, this.requestOptions);
    }
    getBusinessList() {
        let userId = this.dataStore.loginMetaData.userId;
        return this.http.get(Constants.BASE_URL + URLStore.GET_BUSINESS_LIST + userId, this.requestOptions);
    }

    postLoginDetails(loginDetails) {

        return this.http.post(Constants.BASE_URL + "user/login", loginDetails, this.requestOptions);
    }
    registerUser(registrationForm) {
        return this.http.post(Constants.BASE_URL + URLStore.EDIT_USER, registrationForm, this.requestOptions);
    }
    addingUser(userDetails) {
        return this.http.post(Constants.BASE_URL + URLStore.POST_USER, userDetails, this.requestOptions);
    }
    ListingUsers(businessId) {
        return this.http.get(Constants.BASE_URL + URLStore.GET_USERS + businessId, this.requestOptions);
    }
    deletingUser(userId) {

        return this.http.get(Constants.BASE_URL + URLStore.DELETE_USER + userId, this.requestOptions);
    }
    // for get ordersummary
    postOrderDetails(orderDetails) {
        return this.http.post(Constants.BASE_URL + URLStore.ADD_ORDERS, orderDetails, this.requestOptions);
    }

    getOrders() {
        return this.http.get(Constants.BASE_URL + URLStore.GET_ORDERS + this.dataStore.selectedBusiness.businessId + "/business", this.requestOptions);
    }

    // Report

    getOrdersReports(id, fromDate, toDate) {
        return this.http.get(Constants.BASE_URL + URLStore.GET_ORDERS_REPORTS + id + "/" + fromDate + "/" + toDate + "/", this.requestOptions);
    }

    getSalesReports(id, fromDate, toDate, type) {
        return this.http.get(Constants.BASE_URL + URLStore.GET_SALES_REPORTS + id + "/" + fromDate + "/" + toDate + "/" + type, this.requestOptions);
    }


    getOrderDetails(orderId) {
        return this.http.get(Constants.BASE_URL + URLStore.UPDATE_ORDERS + orderId, this.requestOptions);
    }
    deleteOrder(orderId) {
        return this.http.get(Constants.BASE_URL + URLStore.DELETE_ORDERS + "/" + orderId, this.requestOptions);
    }

    deleteMultipleOrders(selectedOrderIds) {
        return this.http.get(Constants.BASE_URL + URLStore.DELETE_MULTIPLE_ORDERS + selectedOrderIds, this.requestOptions);
    }
    getOrdersById(orderId) {
        return this.http.get(Constants.BASE_URL + URLStore.EDII_ORDERS + orderId, this.requestOptions);
    }

    getOrderByName(productName) {

        let orderStatus = "pending";
        if (productName != "") {
            return this.http.get(Constants.BASE_URL + URLStore.FILTER_BY_ORDERS + "/" + productName + "/" + this.dataStore.selectedBusiness.businessId + "/" + orderStatus, this.requestOptions);
        }
        else {

            return this.getOrders();
        }
    }
    getfilterByRange(startDate, endDate, minPrice, maxPrice, minQty, maxQty) {

        let orderStatus = "pending";
        return this.http.get(Constants.BASE_URL + URLStore.FILTER_BY_RANGE + "?" + "startDate=" + startDate + "&endDate=" + endDate + "&minPrice=" + minPrice + "&maxPrice=" + maxPrice + "&minQty=" + minQty + "&maxQty=" + maxQty + "&businessId=" + this.dataStore.selectedBusiness.businessId + "&orderStatus=" + orderStatus, this.requestOptions);
    }

    getMinMaxQtyPrice() {

        let orderStatus = "pending";
        return this.http.get(Constants.BASE_URL + URLStore.FILTER_BY_MIN_MAX_QTY_PRICE + "/" + this.dataStore.selectedBusiness.businessId + "/" + orderStatus, this.requestOptions);

    }
    getOrderHistory() {
        return this.http.get(Constants.BASE_URL + URLStore.ORDER_HISTORY + this.dataStore.selectedBusiness.businessId, this.requestOptions);
    }
    getFilterHistory(productName) {
        let orderStatus = "received";
        if (productName != "") {
            return this.http.get(Constants.BASE_URL + URLStore.FILTER_BY_ORDERS + "/" + productName + "/" + this.dataStore.selectedBusiness.businessId + "/" + orderStatus, this.requestOptions);
        }
        else {
            return this.getOrderHistory();
        }
    }
    getfilterByRangeOrderHistory(startDate, endDate, minPrice, maxPrice, minQty, maxQty, orderStatus: string) {

        return this.http.get(Constants.BASE_URL + URLStore.FILTER_BY_RANGE + "?" + "startDate=" + startDate + "&endDate=" + endDate + "&minPrice=" + minPrice + "&maxPrice=" + maxPrice + "&minQty=" + minQty + "&maxQty=" + maxQty + "&businessId=" + this.dataStore.selectedBusiness.businessId + "&orderStatus=" + orderStatus, this.requestOptions);
    }
    minAndMaxPriceAndQuantity(orderStatus: string) {

        return this.http.get(Constants.BASE_URL + URLStore.FILTER_BY_RANGE_QTY + "/" + this.dataStore.selectedBusiness.businessId + "/" + orderStatus, this.requestOptions);
    }
    editingUser(userData) {

        return this.http.post(Constants.BASE_URL + URLStore.EDIT_USER, userData, this.requestOptions);
    }
    orderCounterTabs(orderStatus: string) {
        var data = this.http.get(Constants.BASE_URL + URLStore.orderCounter + "/" + this.dataStore.selectedBusiness.businessId + "/" + orderStatus, this.requestOptions);
        return data;
    }

    // For customers 
    getCustomer() {

        return this.http.get(Constants.BASE_URL + URLStore.GET_CUSTOMER_LIST + this.dataStore.selectedBusiness.businessId, this.requestOptions);
    }
    addNewCustomer(customerDetails) {

        return this.http.post(Constants.BASE_URL + URLStore.POST_CUSTOMER, customerDetails, this.requestOptions);
    }
    getCustomerById(custId) {

        return this.http.get(Constants.BASE_URL + URLStore.GET_CUSTOMER + custId, this.requestOptions);
    }
    deleteCustomer(custId) {
        return this.http.get(Constants.BASE_URL + URLStore.DELETE_CUSTOMER + custId, this.requestOptions);
    }
    addMultipleCustomers(jsondata) {

        return this.http.post(Constants.BASE_URL + URLStore.ADD_MULTIPLE_CUSTOMER, jsondata, this.requestOptions);
    }

    deleteMultipleCustomers(selectedCustomersIds) {
        return this.http.post(Constants.BASE_URL + URLStore.DELETE_MULTIPLE_CUSTOMER + selectedCustomersIds, this.requestOptions);
    }
    // Getting Session Token 
    get() {
        return this.http.get(Constants.BASE_URL + URLStore.LOGIN_TOKEN, this.requestOptions);
    }

    /// DashBoard
    totalNumberOfEmployees(businessId) {
        var data = this.http.get(Constants.BASE_URL + URLStore.TOTAL_Employees + "/" + businessId+"/business", this.requestOptions);
        return data;
    }
    totalEmpBranchWise(branchId) {
        var data = this.http.get(Constants.BASE_URL + URLStore.TOTAL_Employees_BRANCHWISE + "/" + branchId+"/branch", this.requestOptions);
        return data;
    }
    totalNumberOfProducts(businessId) {
        var data = this.http.get(Constants.BASE_URL + URLStore.TOTAL_PRODUCTS + "/" + businessId, this.requestOptions);
        return data;
    }
    totalProductBranchWise(branchId) {
        var data = this.http.get(Constants.BASE_URL + URLStore.TOTAL_PRODUCTS_BRANCHWISE + "/" + branchId, this.requestOptions);
        return data;
    }
    totalNumberOfBranches(businessId) {
        var data = this.http.get(Constants.BASE_URL + URLStore.TOTAL_BRANCHES + "/" + businessId, this.requestOptions);
        return data;
    }

    totalNumberOfSales(businessId) {
        var data = this.http.get(Constants.BASE_URL + URLStore.TOTAL_SALES + "/" + businessId, this.requestOptions);
        return data;
    }
    getProductlist(businessId) {
        return this.http.get(Constants.BASE_URL + URLStore.GET_PRODUCTS + "/" + businessId, this.requestOptions);
    }
    filterproducts(value) {

        return this.http.get(Constants.BASE_URL + URLStore.GET_DATEWISE_PRODUCTS + "/" + value, this.requestOptions);

    }
    filterorders(value) {

        return this.http.get(Constants.BASE_URL + URLStore.GET_DATEWISE_ORDERS + "/" + value, this.requestOptions);

    }
    filterbranches(value) {

        return this.http.get(Constants.BASE_URL + URLStore.GET_DATEWISE_BRANCHES + "/" + value, this.requestOptions);

    }



    //Branches
    addNewBranch(branchData) {
        return this.http.post(Constants.BASE_URL + URLStore.POST_BRANCH, branchData, this.requestOptions);
    }

    getBranchesList(businessId) {
        if (businessId == undefined) {
            businessId = this.dataStore.businessList[0].businessId;
        }
        return this.http.get(Constants.BASE_URL + URLStore.GET_BRANCHES_LIST + businessId, this.requestOptions);
    }
    getBranchDetails(branchId) {
        return this.http.get(Constants.BASE_URL + URLStore.GET_BRANCH + branchId, this.requestOptions);
    }
    deleteBranch(branchId) {
        return this.http.get(Constants.BASE_URL + URLStore.DELETE_BRANCH + branchId, this.requestOptions);
    }
    addMultipleBranches(multipleBranchesData) {
        return this.http.post(Constants.BASE_URL + URLStore.POST_MULTIPLE_BRANCH, multipleBranchesData, this.requestOptions);
    }
    // Employees
    addEmployee(EmployeeDetails) {
        return this.http.post(Constants.BASE_URL + URLStore.POST_EMPLOYEE, EmployeeDetails, this.requestOptions);
    }
    getEmployeesList() {
        // if (branchId == undefined) {
        //     branchId = this.dataStore.selectedBranch.branchId;
        // }
        // businessId=this.dataStore.selectedBusiness.businessId;
        return this.http.get(Constants.BASE_URL + URLStore.GET_EMPLOYEE_LIST + this.dataStore.selectedBranch.branchId, this.requestOptions);
    }
    getEmployeesListByBranch(brId) {
        // if (branchId == undefined) {
        //     branchId = this.dataStore.selectedBranch.branchId;
        // }
        // businessId=this.dataStore.selectedBusiness.businessId;
        return this.http.get(Constants.BASE_URL + URLStore.GET_EMPLOYEE_LIST + brId, this.requestOptions);
    }
    getEmployeeDetails(empId) {
        return this.http.get(Constants.BASE_URL + URLStore.GET_EMPLOYEE + empId, this.requestOptions);
    }
    editEmployees(empId) {
        return this.http.post(Constants.BASE_URL + URLStore.EDIT_EMPLOYEE + empId, this.requestOptions);
    }
    deleteEmployees(empId) {
        return this.http.get(Constants.BASE_URL + URLStore.DELETE_EMPLOYEE + empId, this.requestOptions);
    }
    addMultipleEmployees(jsondata) {
        return this.http.post(Constants.BASE_URL + URLStore.MULTIPLE_ADD_EMPLOYEE, jsondata, this.requestOptions);
    }
    deleteMultipleEmployees(empId) {
        return this.http.get(Constants.BASE_URL + URLStore.MULTIPLE_DELETE_EMPLOYEE + empId, this.requestOptions);
    }
    deleteMultipleBranches(selectedBranchesIds) {
        return this.http.get(Constants.BASE_URL + URLStore.DELETE_MULTIPLE_BRANCHES + selectedBranchesIds, this.requestOptions);
    }

    getCountries() {
        return this.http.get(Constants.BASE_URL + URLStore.GET_COUNTRIES, this.requestOptions);
    }
    getStates(countryId) {
        return this.http.get(Constants.BASE_URL + URLStore.GET_STATES + countryId, this.requestOptions);
    }
    getCities(stateId) {
        return this.http.get(Constants.BASE_URL + URLStore.GET_CITIES + stateId, this.requestOptions);
    }
    // SearchBar 
    getBusinesswiseSearchbyname(name, businessId) {
        businessId = this.dataStore.selectedBusiness.businessId;
        return this.http.get(Constants.BASE_URL + URLStore.ALL_BUSINESS_SEARCH_NAME + name + "/" + businessId + this.requestOptions);
    }
    // sales
    addPos(formdata) {
        return this.http.post(Constants.BASE_URL + URLStore.ADD_POS, formdata, this.requestOptions);
    }
    getBillNo() {
        return this.http.get(Constants.BASE_URL + URLStore.GET_BILLNO, this.requestOptions);
    }
    getInvoiceList(branchId) {
        branchId = this.dataStore.selectedBranch.branchId;
        return this.http.get(Constants.BASE_URL + URLStore.GET_INVOICE + branchId, this.requestOptions);
    }

}