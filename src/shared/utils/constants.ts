/**
 * Author: Easwar K
 *
 * About the file:
 *
 * This file should contain all the strings used in the application.
 * There should not be any hardcoded strings in the application.
 * The variable scope should always public static.
 * The variable name should be capital letters and can be seperated by _.
 * Access the variable from other classes like TTBConstants.APP_NAME
 *
 */

export class Constants {
    public static APP_NAME = 'Track the business';
    // public static BASE_URL = "http://trackthebusiness.com:8080/TrackTheBusiness/";
    // public static BASE_URL = "https://www.trackthebusiness.com/TrackTheBusiness/";
    public static BASE_URL = "http://192.168.1.2:8080/Ttb/";
    //  public static BASE_URL = "http://166.62.44.165:8080/TrackTheBusiness/";
}
