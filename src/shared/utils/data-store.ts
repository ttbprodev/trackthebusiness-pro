/**
 * Author: Easwar K
 * 
 * About the file:
 * 
 * This file is to store volatile data across the application.  
 *
 */

import { Injectable } from "@angular/core";

@Injectable()
export class DataStore {
    userRole: any = null;
    userPlan: any = null;
    businessList: any = [];
    businessId: any;
    selectedBusiness: any = null;
    loginMetaData: any = null;
    csrf_token: any = null;
    accessCookie: any = null;
    refreshCookie: any = null;
    filteredContact: any = [];
    currentHeaderTitle: any = "Dashboard";
    currentHeaderTitle2: any;
    branchesList: any = [];
    EmployeeList: any = [];
    selectedBranch: any = null;
    branchId: any;
    productList: any = [];
    constructor() {
        console.log(this.currentHeaderTitle2);


    }
}