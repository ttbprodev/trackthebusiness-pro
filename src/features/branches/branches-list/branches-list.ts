import { Service } from "../../../shared/utils/service";
import { Component, Input } from "@angular/core";
import { NavController, AlertController, ToastController } from "ionic-angular";
import { AddBranch } from "../add-branch/add-branch";
import { DataStore } from "../../../shared/utils/data-store";
@Component({
    selector: 'branches-list',
    templateUrl: './branches-list.html'
})
export class BranchesList {
    rows = [];
    columns = [
        { prop: 'businessName' },
        { name: 'Gender' },
        { name: 'Company' }
    ];
    // headerTitle = "Branches List";
    branchesList = [];
    IsAllChecked: any;
    IsItemChecked = false;
    showButton = false;
    selectedBranchesCount = 0;
    selectedBranchId: any;
    currentBranchId: any;
    selectedBranchesIds = [];
    tablestyle = 'bootstrap';

    constructor(
        private service: Service,
        public navCtrl: NavController,
        public alertCtrl: AlertController,
        public dataStore: DataStore,
        private toastCtrl: ToastController,
    ) {
        this.getBranchesList();
    }

    presentToast(toastMessage: string) {
        let toast = this.toastCtrl.create({
            message: toastMessage,
            duration: 3000,
            position: 'bottom'
        });
        toast.present();
    }

    getBranchesList() {
        this.service.getBranchesList(this.dataStore.selectedBusiness.businessId).subscribe(
            response => {
                console.log(response);
                this.branchesList = response.json();
                this.rows = this.branchesList;
                if (this.branchesList.length == 0) {
                    this.navCtrl.setRoot(AddBranch);
                }
            },
            error => {
                console.log(error);
            }
        );
    }

    CheckUncheckAll(IsAllChecked) {
        for (var i = 0; i < this.branchesList.length; i++) {
            this.branchesList[i].branchSelected = this.IsAllChecked;
            if (IsAllChecked == true) {
                this.selectedBranchesCount = this.selectedBranchesCount + 1;
                console.log("selectedBranchesCount: " + this.selectedBranchesCount);

            } else if (IsAllChecked == false) {
                this.selectedBranchesCount = 0;
            }
        }
    }

    CheckUncheckHeader(selectedBranch, branchId) {
        console.log("change branch id: " + branchId);
        if (selectedBranch == true) {
            this.selectedBranchId = branchId;
            this.selectedBranchesCount = this.selectedBranchesCount + 1;
            console.log("selectedBranchesCount: " + this.selectedBranchesCount);
            console.log(this.selectedBranchesCount > 1 || this.selectedBranchesCount < 1);
            console.log(!(this.selectedBranchesCount > 1 || this.selectedBranchesCount < 1));
        } else if (selectedBranch == false) {
            // this.selectedBranchId = branchId;
            this.selectedBranchesCount = this.selectedBranchesCount - 1;
            console.log("selectedBranchesCount: " + this.selectedBranchesCount);
        }
        for (var i = 0; i < this.branchesList.length; i++) {
            if (this.branchesList[i].branchSelected == false) {
                this.IsAllChecked = false;
                this.IsItemChecked = true;
                break;
            }
            else if (this.branchesList[i].selected == true) {
                this.selectedBranchId = this.branchesList[i].branchId;
            }
        }
        if (this.selectedBranchesCount == this.branchesList.length) {
            this.IsAllChecked = true;
        }
    }

    addNewBranch() {
        this.navCtrl.setRoot(AddBranch);
    }
    editSingleBranch() {
        this.editBranch(this.selectedBranchId);
    }

    editBranch(branchId) {
        console.log(branchId + " edit clicked..");
        this.service.getBranchDetails(branchId).subscribe(
            response => {
                var result = response.json();
                console.log("Result data of branch");
                this.navCtrl.setRoot(AddBranch, result);
            },
            error => {
                console.log(error);
            });
    }

    deleteBranch(branchId) {
        let alert = this.alertCtrl.create({
            title: 'Confirm deletion',
            message: 'Do you want to Delete this Branch?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: () => {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Delete',
                    handler: () => {
                        console.log(branchId + ", " + " delete clicked.. ");
                        this.service.deleteBranch(branchId).subscribe(
                            response => {
                                var result = response.json();
                                this.presentToast(response.json().message);
                                this.navCtrl.setRoot(BranchesList);
                            },
                            error => {
                                console.log(error);
                            });
                    }
                }
            ]
        });
        alert.present();
    }

    deleteMultipleBranches() {
        let alert = this.alertCtrl.create({
            title: 'Confirm deletion',
            message: 'Do you want to Delete Branch(es) ?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: () => {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Delete',
                    handler: () => {
                        for (var i = 0; i < this.branchesList.length; i++) {
                            if (this.branchesList[i].branchSelected) {
                                this.selectedBranchesIds.push(this.branchesList[i].branchId);
                            }
                        }
                        this.service.deleteMultipleBranches(this.selectedBranchesIds).subscribe(
                            res => {
                                if (res.status == 200) {
                                    // this.presentToast(res.json().message);
                                    this.presentToast('Branches deleted successfully..');
                                    this.navCtrl.setRoot(BranchesList);
                                }
                            },
                            error => {
                                console.log(error);
                            }
                        );
                    }
                }
            ]
        });
        alert.present();
    }

    switchStyle() {
        if (this.tablestyle == 'dark') {
            this.tablestyle = 'bootstarp';
        } else {
            this.tablestyle = 'dark';
        }
    }
    openDataTable() {
        this.navCtrl.setRoot('TablePage');
    }
    getRowClass(branch) {
        return this.branchesList;
    }
}