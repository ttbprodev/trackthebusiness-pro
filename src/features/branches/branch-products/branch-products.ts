import { Component } from "@angular/core";
import { NavParams, ViewController, NavController } from "ionic-angular";
import { Service } from "../../../shared/utils/service";

@Component({
    selector: 'branch-products',
    templateUrl: './branch-products.html'
})
export class BranchProducts {
    productsList: any;
    selectedProducts = [];
    branchDetails: any;
    productsList2: any;
    IsAllChecked: any;
    IsItemChecked = false;
    formData: any;
    categoriesList: any;

    categoryChecked: any;
    subCategoryChecked: any;
    productChecked: any;

    showLevel1 = null;
    showLevel2 = null;
    cid: any;

    constructor(
        public navParams: NavParams,
        public viewCtrl: ViewController,
        public service: Service,
        public navCtrl: NavController
    ) {
        this.getProductCategories();
        this.getProducts();
        // this.getProductSubCategories(this.cid);
        // this.productsList = this.navParams.get('productsList');
        this.branchDetails = this.navParams.get('branchform');
        // this.categoriesList = [
        //     { categoryId: "1", name: "Mens" },
        //     { categoryId: "2", name: "Womens" },
        //     { categoryId: "3", name: "Kids" }];
        // this.productsList = [
        //     { productId: "1", productName: "Wallet", category: [{ categoryId: "1" }] },
        //     { productId: "2", productName: "Mobile", category: [{ categoryId: "2" }] },
        //     { productId: "3", productName: "Bottle" }];
    }
    isLevel1Shown(idx) {
        return this.showLevel1 === idx;
    };

    isLevel2Shown(idx) {
        return this.showLevel2 === idx;
    };
    toggleLevel1(idx) {
        if (this.isLevel1Shown(idx)) {
            this.showLevel1 = null;
        } else {
            this.showLevel1 = idx;
        }
    };

    toggleLevel2(idx) {
        if (this.isLevel2Shown(idx)) {
            this.showLevel1 = null;
            this.showLevel2 = null;
        } else {
            this.showLevel1 = idx;
            this.showLevel2 = idx;
        }
    };

    CheckUncheckHeader() {
        this.IsAllChecked = true;
        for (var i = 0; i < this.productsList.length; i++) {
            if (!this.productsList[i].selected) {
                this.IsAllChecked = false;
                this.IsItemChecked = true;
                break;
            }
        }
    }
    CheckUncheckAll() {
        for (var i = 0; i < this.productsList.length; i++) {
            this.productsList[i].selected = this.IsAllChecked;
            this.selectedProducts.push({ productId: this.productsList[i].productId });
        }
    }
    saveAndAssignProducts() {
        this.selectedProducts = [];
        for (var i = 0; i < this.productsList.length; i++) {
            if (this.productsList[i].selected) {
                this.selectedProducts.push({ productId: this.productsList[i].productId });
                //console.log( JSON.stringify(this.selectedSchoolIds));
            }
        }
        this.formData = new FormData();
        this.formData.append('buId', this.branchDetails.businessId);
        this.formData.append('branchName', this.branchDetails.branchName);
        this.formData.append('email', this.branchDetails.email);
        this.formData.append('mobileNumber', this.branchDetails.mobileNumber);
        this.formData.append('address', this.branchDetails.address);
        this.formData.append('country', this.branchDetails.country);
        this.formData.append('state', this.branchDetails.state);
        this.formData.append('city', this.branchDetails.city);
        this.formData.append('pincode', this.branchDetails.pincode);
        this.formData.append('landmark', this.branchDetails.landmark);
        var barnch = [];
        barnch.push({ "products": [{ "productId": 1 }, { "productId": 2 }] });
        console.log(barnch);

        var data = { "productList": [{ "productId": 1 }, { "productId": 2 }] };
        console.log(data);
        // this.formData.append('productList',  this.selectedProducts[0].productId);
        // this.formData.append('products', data);
        // console.log(data);
        for (var i = 0; i < this.selectedProducts.length; i++) {
            console.log('productList[' + i + '].productId', this.selectedProducts[i].productId);
            this.formData.append('productList[' + i + '].productId', this.selectedProducts[i].productId);
        }
        // let arr = [{ branchDetails: this.branchDetails }, { products: this.selectedProducts }];
        this.service.addNewBranch(this.formData).subscribe(
            res => {
                console.log(res);
            },
            err => {
                console.log(err);
            }
        )
        console.log(this.productsList);
    }
    dismiss() {
        this.viewCtrl.dismiss();
    }
     getProducts() {
        this.service.getProductAndCategories().subscribe(
            res => {
                let result = res.json();
                this.productsList = result;
            },
            err => {

            }
        )
    }
    getProductCategories() {
        this.service.getCategory().subscribe(
            res => {
                this.categoriesList = res.json();
                console.log(this.categoriesList);
            },
            err => {
                console.log(err);
            }
        )
    }
    getProductSubCategories(cId) {
        this.service.getSubCategory(cId).subscribe(
            res => {
                this.categoriesList = res.json();
                console.log(this.categoriesList);
            },
            err => {
                console.log(err);
            }
        )
    }
    CheckUncheckCategory(categoryChecked, categoryId) {
        let selectedCategories = [];
        console.log("categoryChecked:" + categoryChecked + " & categoryId:" + categoryId);
        for (var i = 0; i < this.categoriesList.length; i++) {
            if (this.categoriesList[i].categoryId == categoryId) {
                this.categoriesList[i].categoryChecked = categoryChecked;
            }
        }
        // if (categoryChecked == true) {
        //     for (var i = 0; i < this.categoriesList.length; i++) {
        //         if (this.categoriesList[i].categoryId == categoryId) {
        //             for(var i = 0; i < this.productsList.length; i++){
        //                 this.selectedProducts.push(this.categoriesList[i].categoryId);
        //             }
        //         }
        //     }
        //     this.CheckUncheckSubCategory("", "");
        // } else if (categoryChecked == false) {
        // }

        // for (var i = 0; i < this.productsList.length; i++) {
        //     if (this.productsList[i].categoriesList[i].categoryId == categoryId) {
        //         this.productsList[i].selected = this.categoryChecked;
        //     }
        // }
    }
    CheckUncheckSubCategory(subCategoryChecked, subCategoryId) {
        console.log("subCategoryChecked:" + subCategoryChecked + " & subCategoryId:" + subCategoryId);

    }
    CheckUncheckProduct(productChecked, productId) {
        console.log("productChecked:" + productChecked + " & productChecked:" + productId);
        for (var i = 0; i < this.productsList.length; i++) {
            if (this.productsList[i].productId == productId) {
                this.productsList[i].productChecked = productChecked;
            }
        }
        if (productChecked) {
            this.selectedProducts.push(productId);
        } else if (productChecked == false) {
            this.selectedProducts.splice(this.selectedProducts.indexOf(productId), 1);
        }
    }
}
