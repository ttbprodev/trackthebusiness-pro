import { Component } from "@angular/core";
import { FormBuilder, FormGroup, Validators, FormControl } from "@angular/forms";
import { DataStore } from "../../../shared/utils/data-store";
import { Service } from "../../../shared/utils/service";
import { NavController, ModalController, NavParams, Platform, ToastController, AlertController } from "ionic-angular";
import { BranchesList } from "../branches-list/branches-list";
import { BranchProducts } from "../branch-products/branch-products";
import * as XLSX from 'xlsx';


@Component({
    selector: 'add-branch',
    templateUrl: './add-branch.html'
})
export class AddBranch {
    isWeb: any;
    headerTitle = "Adding Branch";
    public branchForm: FormGroup;
    formData: any;
    branchName: any;
    email: any;
    mobileNumber: any;
    // gst: any;
    address: any;
    country: any;
    state: any;
    city: any;
    pincode: any;
    landmark: any;
    countriesList = [];
    statesList = [];
    citiesList = [];

    emailKeys = [];
    emailslist: string = "";
    addByDocument = false;
    arrayBuffer: any;
    docName = "";
    branchesFromDoc = [];
    fileUrl: any;
    fileType: any;
    file: File;
    usersList: any;

    branchId: any;
    productsList: any;
    branchesCount: any;
    productsCount: any;

    branchNameCtrl: FormControl;
    emailCtrl: FormControl;
    mobileNumberCtrl: FormControl;
    // gstCtrl: FormControl;
    addressCtrl: FormControl;
    countryCtrl: FormControl;
    stateCtrl: FormControl;
    cityCtrl: FormControl;
    pincodeCtrl: FormControl;
    landmarkCtrl: FormControl;

    constructor(
        private formBuilder: FormBuilder,
        public dataStore: DataStore,
        private service: Service,
        public navCtrl: NavController,
        public modalCtlr: ModalController,
        public navParams: NavParams,
        private toastCtrl: ToastController,
        public plt: Platform,
        public alertCtrl: AlertController,
    ) {
        this.getCountriesList();
        // if (this.state != undefined) {
        //     this.getStatesList(this.country);
        // }
        // if (this.city != undefined) {
        //     this.getCitiesList(this.state);
        // }        
        this.getProducts();
        this.getBranchesList();
        let branchDetails = this.navParams.data;
        this.branchId = branchDetails.branchId;
        if (branchDetails.country != undefined) {
            this.getStatesList(branchDetails.country);
        }
        if (branchDetails.state != undefined) {
            this.getCitiesList(branchDetails.state);
        }
        if (this.branchId != undefined) {
            this.showBranchInfo(this.branchId);
        }
        this.branchNameCtrl = this.formBuilder.control('', [Validators.required, Validators.minLength(2), Validators.maxLength(25), Validators.pattern("^[a-zA-Z0-9@#$&()\\_\,\ \- \\-`.+,/\]*$")]);
        this.emailCtrl = this.formBuilder.control('', [Validators.required, Validators.email]);
        this.mobileNumberCtrl = this.formBuilder.control('', [Validators.required, Validators.minLength(10), Validators.maxLength(12), Validators.pattern("^[0-9]*[0-9]$")]);
        // this.gstCtrl = this.formBuilder.control('', [Validators.required, Validators.minLength(8), Validators.maxLength(25), Validators.pattern("^[a-zA-Z0-9]*$")]);
        this.addressCtrl = this.formBuilder.control('', [Validators.required, Validators.minLength(8), Validators.maxLength(50), Validators.pattern("^[a-zA-Z0-9@#$&()\\_\,\ \- \\-`.+,/\]*$")]);
        this.countryCtrl = this.formBuilder.control(''),// Validators.required);
            this.stateCtrl = this.formBuilder.control(''),// Validators.required);
            this.cityCtrl = this.formBuilder.control(''),// Validators.required);
            this.pincodeCtrl = this.formBuilder.control('', [Validators.required, Validators.minLength(5), Validators.maxLength(7), Validators.pattern("^[0-9]*[0-9]$")]);
        this.landmarkCtrl = this.formBuilder.control('', [Validators.required, Validators.minLength(8), Validators.maxLength(25), Validators.pattern("^[a-zA-Z0-9\_\,\ ]*$")]);

        this.branchForm = this.formBuilder.group({
            branchName: this.branchNameCtrl,
            email: this.emailCtrl,
            mobileNumber: this.mobileNumberCtrl,
            // gst: this.gstCtrl,
            address: this.addressCtrl,
            country: this.countryCtrl,
            state: this.stateCtrl,
            city: this.cityCtrl,
            pincode: this.pincodeCtrl,
            landmark: this.landmarkCtrl
        })
    }
    ngOnInit() {
        this.findPlatform();
    }
    findPlatform() {
        if (this.plt.is('core')) {
            this.isWeb = true;
            console.log("web platform");
        }
        else {
            this.isWeb = false;
        }
    }
    presentToast(toastMessage: string) {
        let toast = this.toastCtrl.create({
            message: toastMessage,
            duration: 3000,
            position: 'bottom'
        });
        toast.present();
    }
    getCountriesList() {
        this.service.getCountries().subscribe(
            res => {
                console.log(res.json());
                this.countriesList = res.json();
            },
            err => {
                console.log(err);
            }
        );
    }
    getStatesList(event) {
        this.statesList = [];
        this.citiesList = [];
        this.service.getStates(event).subscribe(
            res => {
                console.log(res.json());
                this.statesList = res.json();
            },
            err => {
                console.log(err);
            }
        )
    }
    getCitiesList(event) {
        this.service.getCities(event).subscribe(
            res => {
                console.log(res.json());
                this.citiesList = res.json();
            },
            err => {
                console.log(err);
            }
        )
    }
    getBranchesList() {
        this.service.getBranchesList(this.dataStore.selectedBusiness.businessId).subscribe(
            response => {
                console.log(response.status == 200);
                // this.branchesList = response.json();
                if (response.json().length == 0) {
                    this.branchesCount = response.json().length;
                    console.log(response.json().length);
                }
                else {
                    this.branchesCount = response.json().length;
                    console.log(this.branchesCount);
                }
            },
            error => {
                console.log(error);
            }
        );
    }
    addBranch(bform) {
        console.log(bform);
        console.log(this.branchForm.value);
        let branchFormData = this.branchForm.value;
        branchFormData.buId = this.dataStore.selectedBusiness.businessId;
        console.log(this.branchForm.value);
        this.formData = new FormData();
        this.formData.append('buId', this.dataStore.selectedBusiness.businessId);
        this.formData.append('branchName', this.branchForm.value.branchName);
        this.formData.append('email', this.branchForm.value.email);
        this.formData.append('mobileNumber', this.branchForm.value.mobileNumber);
        // this.formData.append('gst', this.branchForm.value.gst);
        this.formData.append('address', this.branchForm.value.address);
        this.formData.append('country', this.branchForm.value.country);
        this.formData.append('state', this.branchForm.value.state);
        this.formData.append('city', this.branchForm.value.city);
        this.formData.append('pincode', this.branchForm.value.pincode);
        this.formData.append('landmark', this.branchForm.value.landmark);
        if (this.branchId != undefined) {
            this.formData.append('branchId', this.branchId);
        }

        this.service.addNewBranch(this.formData).subscribe(
            response => {
                console.log(response);
                if (response.status == 200) {
                    this.navCtrl.setRoot(BranchesList);
                }
            },
            error => {
                console.log(error);
            }
        )
    }
    deleteBranch() {
        let alert = this.alertCtrl.create({
            title: 'Confirm deletion',
            message: 'Do you want to Delete this Branch?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: () => {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Delete',
                    handler: () => {
                        console.log(this.branchId + ", " + " delete clicked.. ");
                        this.service.deleteBranch(this.branchId).subscribe(
                            response => {
                                var result = response.json();
                                this.presentToast(response.json().message);
                                this.navCtrl.setRoot(BranchesList);
                            },
                            error => {
                                console.log(error);
                            });
                    }
                }
            ]
        });
        alert.present();
    }

    showBranchesList() {
        this.navCtrl.setRoot(BranchesList);
    }
    assignProducts(bForm) {
        let productsModal = this.modalCtlr.create(BranchProducts, { branchform: this.branchForm.value });
        productsModal.present();
    }
    getProducts() {
        this.service.getProduct().subscribe(
            res => {
                let result = res.json();
                this.productsList = result;
                this.productsCount = this.productsList.length;
            },
            err => {

            }
        )
    }
    showBranchInfo(branchId) {
        this.service.getBranchDetails(branchId).subscribe(
            response => {
                var result = response.json();
                console.log("Result data for business view");
                this.populateBranchData(result);
            });
    }
    populateBranchData(result) {
        this.branchName = result.branchName;
        this.email = result.email;
        this.mobileNumber = result.mobileNumber;
        // this.gst = result.gst;
        this.address = result.address;
        this.country = result.country;
        this.state = result.state;
        this.city = result.city;
        this.pincode = result.pincode;
        this.landmark = result.landmark;
    }
    addMultipleBranches() {

    }
    uploadByDocument() {
        this.addByDocument = true;
    }
    onDocUpload(file) {
        // this.fileUrl = "";
        // this.fileUrl = file[0];
        this.file = null;
        this.file = file[0];
        try {
            this.docName = this.file.name;
            let fileReader = new FileReader();
            fileReader.onload = (e) => {
                this.arrayBuffer = fileReader.result;
                var data = new Uint8Array(this.arrayBuffer);
                var arr = new Array();
                for (var i = 0; i != data.length; ++i) {
                    arr[i] = String.fromCharCode(data[i]);
                }
                var bstr = arr.join("");
                var workbook = XLSX.read(bstr, { type: "binary" });
                var first_sheet_name = workbook.SheetNames[0];
                var worksheet = workbook.Sheets[first_sheet_name];
                let docObjArray = XLSX.utils.sheet_to_json(worksheet, { raw: true });
                this.branchesFromDoc = docObjArray;
            }
            fileReader.readAsArrayBuffer(this.file);
        }
        catch (error) {
            console.log(error);
        }
    }
    uploadDocument() {
        this.branchesFromDoc.map(v => v.buId = this.dataStore.selectedBusiness.businessId);
        for (let i = 0; i < this.branchesFromDoc.length; i++) {
            delete this.branchesFromDoc[i].__rowNum__;
        }
        this.service.addMultipleBranches(this.branchesFromDoc).subscribe(
            res => {
                console.log(res);
                if (res.status == 200) {
                    this.presentToast(res.json().message);
                } else {

                    let existedBranches = [];
                    for (var k in JSON.parse(res.json().message)) existedBranches.push(k);
                    // alert('These brances exists already: ' + existedBranches);
                    this.presentToast(res.json().message);
                }
                this.navCtrl.setRoot(BranchesList);
            },
            err => {
                alert(err);
            })
    }
    cancelUploadByDocument() {
        this.addByDocument = false;
    }
}