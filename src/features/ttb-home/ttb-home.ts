import { Component, OnInit } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import { Constants } from '../../shared/utils/constants';
import { Service } from '../../shared/utils/service';
import { DataStore } from '../../shared/utils/data-store';
import { BusinessView } from '../business/business-view/business-view';
@Component({
  selector: 'ttb-home',
  templateUrl: 'ttb-home.html'
})
export class TTBHome implements OnInit {
  appName = Constants.APP_NAME;
  constructor(public navCtrl: NavController, 
    public service: Service,
    public dataStore: DataStore,
    private toastCtrl: ToastController) {
  }

  ngOnInit() {
    this.getBusinessList();
  }

  getBusinessList() {
    this.service.getBusinessList().subscribe(
      response => {
        this.dataStore.businessList = response.json();
        if (this.dataStore.businessList.length == 0) {
          this.navCtrl.setRoot(BusinessView);
        }
      },error => {
        this.presentToast("Service Error");
        console.log(error);
    });
  }
  presentToast(toastMessage: string) {
    let toast = this.toastCtrl.create({
        message: toastMessage,
        duration: 3000,
        position: 'bottom'
    });
    toast.present();
}
}
