import { Component } from "@angular/core";
import { ViewController, NavParams, LoadingController, NavController, ToastController, AlertController, Platform } from "ionic-angular";
import { Service } from '../../../shared/utils/service';
import { Http } from '@angular/http';
import { FormGroup, FormBuilder, Validators, } from "@angular/forms";
import { DataStore } from "../../../shared/utils/data-store";
import { OrderSummary } from "../order-summary/order-summary";
import { OrderHistory } from "../order-history/order-history";

@Component({
    selector: 'price-tag',
    templateUrl: 'price-tag.html',
})
export class PriceTag {
    isWeb: any;
    formData: any;
    editCustomer = false;
    BusinessId: any;
    customerId: any;
    CustomerId: any;
    BuId:any;
    BId:any;
    priceUnitValue = 0;
    recivedOrders: any;
    priceperunit:any;
    percantagevalue: any;
    AditionalPrice: any;
    UnitPrice: any;
    private priceTagForm: FormGroup;
    constructor(
        private view: ViewController,
        public http: Http,
        public service: Service,
        private navParams: NavParams,
        private loadingCtrl: LoadingController,
        private navCtrl: NavController,
        private toastCtrl: ToastController,
        private alertCtrl: AlertController,
        private formBuilder: FormBuilder,
        public dataStore: DataStore,
        public plt: Platform,
        public dataSTore: DataStore, ) {
        this.UnitPrice = this.navParams.data.PriceUnitValue;
        this.recivedOrders = this.navParams.data.recivedOrders;
    }

    ngOnInit() {
        this.findPlatform();
    }
    findPlatform() {
        if (this.plt.is('core')) {
            this.isWeb = true;
            console.log("web platform");
        }
        else {
            this.isWeb = false;
        }
    }
    presentToast(toastMessage: string) {
        let toast = this.toastCtrl.create({
            message: toastMessage,
            duration: 2000,
            position: 'bottom'
        });
        toast.present();
    }
    addPriceTag(priceForm) {
        this.recivedOrders.value.aditionalPrice = priceForm.value.aditionalPrice;
        // this.recivedOrders.value.businessId = this.dataSTore.selectedBusiness.businessId;
        this.recivedOrders.value.buId = this.dataSTore.selectedBusiness.businessId;
        // this.recivedOrders.value.bId  = this.dataSTore.branchesList[0].branchId;
        this.service.postOrderDetails(this.recivedOrders.value).subscribe(
            response => {
                let serverResponse = response.json();
                this.presentToast(serverResponse.message);
                if (serverResponse.status == 200) {
                    this.presentToast("Order added Successfully.");
                    this.close();
                }
            },
            error => {
                this.presentToast("Service Error");
                console.log(error);
            }
            );
    }
    close() {
        this.view.dismiss("Close");
    }
}


