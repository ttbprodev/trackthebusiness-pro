import { Component } from "@angular/core";
import { ViewController, NavParams } from "ionic-angular";
import { Service } from '../../../shared/utils/service';
import { Http } from '@angular/http';

@Component({
    selector: 'filter-model',
    templateUrl: 'filter-model.html',
})
export class FilterModel {
    startDate: any;
    endDate: any;
    minPrice: any;
    maxPrice: any;
    maxQty: any;
    minQty: any;
    OrderId: any;
    BusinessId: any;
    filterList : any ;
    quantityrange: any = {};
    amountrange: any = {};
    originScreenName: string = "";
    orderStatus: string = "";
    constructor(private view: ViewController,
        public http: Http,
        public service: Service,
        private navParams: NavParams ) {
        this.originScreenName = this.navParams.data.originScreenName;
        this.setOrderStatus();
        this.getMinMaxQtyPrice();
    }

    setOrderStatus() {
        if(this.originScreenName == "OrderSummary") 
        {
            this.orderStatus = "pending";

        } else if(this.originScreenName == "OrderHistory") {
            this.orderStatus = "received";
        }
    }
    getMinMaxQtyPrice() 
    {
        this.service.minAndMaxPriceAndQuantity(this.orderStatus).subscribe(
            response => {
                this.filterList = response.json();
                console.log(response);
                console.log(this.filterList);
                this.maxPrice =this.filterList.maxPrice;
                this.maxQty = this.filterList.maxQty;
                this.quantityrange = {
                    upper: this.maxQty,
                    lower: 1
                };
                this.amountrange = {
                    upper: this.maxPrice,
                    lower: 1
                };
            });
    }
    applyFilter() 
    {
        console.log(this.amountrange);
        console.log(this.quantityrange);
        
            this.maxPrice = this.amountrange.upper;
       
            this.minPrice = this.amountrange.lower;
        
            this.maxQty = this.quantityrange.upper;
        
            this.minQty = this.quantityrange.lower;
        
        if (this.startDate == undefined) {
            this.startDate = "";
        }
        else {
            this.startDate = this.startDate;
        }

        if (this.endDate == undefined) {
            this.endDate = "";
        }
        else {
            this.endDate = this.endDate;
        }
        this.service.getfilterByRangeOrderHistory(this.startDate, this.endDate, this.minPrice, this.maxPrice, this.minQty, this.maxQty, this.orderStatus).subscribe(
            response => {
                this.filterList = response.json();
                console.log(response);
                console.log(this.filterList);
                this.closeFilter(this.filterList);
            });
    }
    closeFilter(filterList: any) {
        if (filterList === undefined || filterList === null) {
            this.view.dismiss("Close");
        } else {
            this.view.dismiss(filterList);
        }
    }
}