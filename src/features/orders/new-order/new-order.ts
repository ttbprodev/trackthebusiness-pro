import { Component, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators, FormControl, NgForm } from "@angular/forms";
import 'rxjs/add/operator/debounceTime';
import { Service } from "../../../shared/utils/service";
import { DataStore } from "../../../shared/utils/data-store";
import { NavParams, LoadingController, NavController, Platform, ToastController, ModalController, ViewController } from "ionic-angular";
import { OrderHistory } from "../order-history/order-history";
import { OrderSummary } from "../order-summary/order-summary";
import { PriceTag } from "../price-tag/price-tag";

@Component({
    selector: 'new-order',
    templateUrl: './new-order.html',
})
export class NewOrder implements OnInit {
    @ViewChild('f') orderForm: NgForm;
    today: any;
    isWeb: any;
    OrderId: any;
    productId: any;
    BuId: any;
    BId: any;
    CustomerId: any;
    ProductName: any;
    ProductDesc: any;
    OrderQuantity: any;
    CustomerName: any;
    CustomerMobile: any;
    TotalAmount: any;
    AmountPaid: any;
    AmountPending: number;
    OrderStatus: any;
    OrderStatus1: string = "pending";
    OrderDate: any;
    ExpectedDelivery: any;
    aditionalPrice: any;
    unitPrice: any;
    AditionalPrice: any;
    UnitPrice: any;
    ordersummaryCount: any;
    orderSummary = [];
    amountPaidExceed = false;
    productNameCtrl: FormControl;
    productDescCtrl: FormControl;
    orderQuantityCtrl: FormControl;
    customerNameCtrl: FormControl;
    customerMobileCtrl: FormControl;
    totalAmountCtrl: FormControl;
    amountPaidCtrl: FormControl;
    amountPendingCtrl: FormControl;
    orderStatusCtrl: FormControl;
    orderDateCtrl: FormControl;
    expectedDeliveryCtrl: FormControl;
    orderIdCtrl: FormControl;
    productIdCtrl: FormControl;
    bIdCtrl: FormControl;
    buIdCtrl: FormControl;
    customerIdCtrl: FormControl;
    private ordersForm: FormGroup;
    constructor(
        private formBuilder: FormBuilder,
        public service: Service,
        public dataSTore: DataStore,
        public loadingCtrl: LoadingController,
        public navParams: NavParams,
        public navctr: NavController,
        public plt: Platform,
        private toastCtrl: ToastController,
        public modalCtrl: ModalController,
        public viewCtrl: ViewController,

    ) {
        this.today = new Date().toISOString();
        console.log(this.OrderStatus1);
        this.ordersForm = this.formBuilder.group({
            productName: ['', Validators.required, Validators.minLength(3), Validators.maxLength(50)],
            productDesc: ['', Validators.required, Validators.minLength(3), Validators.maxLength(50)],
            orderQuantity: ['', Validators.required, Validators.minLength(1), Validators.maxLength(50)],
            customerName: ['', Validators.required, Validators.minLength(3), Validators.maxLength(50)],
            customerMobile: ['', Validators.required, Validators.minLength(10), Validators.maxLength(10)],
            totalAmount: ['', Validators.required],
            amountPaid: ['', Validators.required],
            amountPending: ['', Validators.required, Validators.minLength(0), Validators.maxLength(50)],
            orderStatus: ['', Validators.required],
            orderDate: ['', Validators.required],
            expectedDelivery: ['', Validators.required],
            productId: ['', Validators.required],
            orderId: ['', Validators.required],
            customerId: ['', Validators.required],
            buId: ['', Validators.required],
            bId: ['', Validators.required],

        });
        this.OrderId = navParams.get('orderId');
        this.orderSummary = navParams.get('orderSummary');
        if (this.OrderId != undefined) {

            this.showOrderInfo(this.OrderId);
        }
        this.productNameCtrl = new FormControl();
        this.productNameCtrl = this.formBuilder.control('', Validators.compose([Validators.required, Validators.minLength(12), Validators.pattern("^[a-zA-Z\.]*$\ ")]));
    }
    setDefaultStatus(): string[] {
        return [
            "pending",
            "received",
        ];
    }
    logChosen(): void {
        console.log(this.OrderStatus1);
    }
    showOrderInfo(orderId) {
        // console.log(orderSummary);
        // let result;
        // for (var i = 0; i < orderSummary.length; i++) {
        //     if (orderSummary[i].orderId == orderId) {
        //         result = orderSummary[i];
        //     }
        // }

        this.service.getOrdersById(orderId).subscribe(
    
            response => {
                let result = response.json();
                console.log(response);
                this.ProductName = result.orderProducts[0].productName;
                this.ProductDesc = result.orderProducts[0].productDesc;
                this.OrderQuantity = result.orderProducts[0].availableQuantity;
                this.CustomerName = result.customer.customerName;
                this.CustomerMobile = result.customer.customerMobile;
                this.TotalAmount = result.totalAmount;
                this.AmountPaid = result.amountPaid;
                this.AmountPending = result.amountPending;
                this.OrderStatus = result.orderStatus;
                this.OrderDate = result.orderDate;
                this.ExpectedDelivery = result.expectedDelivery;
                this.CustomerId = result.customer.custId;
                this.AditionalPrice = result.aditionalPrice;
                this.UnitPrice = result.unitPrice;
            },
            error => {
                console.log(error);
            }
        )
    }
    presentToast(toastMessage: string) {
        let toast = this.toastCtrl.create
            ({
                message: toastMessage,
                duration: 2000,
                position: 'bottom'
            });
        toast.present();
    }
    newOrder(ordersForm) {
        console.log(ordersForm.value);
        ordersForm.value.buId = this.dataSTore.selectedBusiness.businessId;
        // ordersForm.value.bId = this.dataSTore.branchesList[0].branchId;
        if (ordersForm.value.orderStatus == 'received') {
            let priceperunit = this.UnitPrice = (parseFloat(this.TotalAmount) / parseFloat(this.OrderQuantity));
            let profileModal = this.modalCtrl.create(PriceTag, { recivedOrders: ordersForm, PriceUnitValue: priceperunit.toFixed(2) });
            profileModal.present();
            profileModal.onDidDismiss(data => {
                this.navctr.setRoot(OrderSummary);
                this.orderForm.reset({ OrderStatus: "pending" });
                console.log(data);
            });
        } else {
            this.service.postOrderDetails(ordersForm.value).subscribe(
                response => {
                    var result = response.json();
                    this.presentToast(result.message);
                    this.orderForm.reset({ OrderStatus: "pending" });
                },
                error => {
                    alert(error);
                }
            );

        }
    }
    ngOnInit() {
        this.findPlatform();
    }
    ionViewWillEnter() {
        this.orderForm.controls['orderStatus'].setValue("pending");
    }
    findPlatform() {
        if (this.plt.is('core')) {
            this.isWeb = true;
            console.log("web platform");
        }
        else {
            this.isWeb = false;
        }
    }
    calculatePrice(event) {
        try {
            this.TotalAmount = this.TotalAmount ? this.TotalAmount : 0;
            this.AmountPaid = this.AmountPaid ? this.AmountPaid : 0;
            if (parseInt(this.TotalAmount) > parseInt(this.AmountPaid) || (parseInt(this.TotalAmount) == parseInt(this.AmountPaid))) {
                console.log("value is not valid");
                this.AmountPending = (parseInt(this.TotalAmount) - parseInt(this.AmountPaid));
                this.amountPaidExceed = false;
            }
            else if (parseInt(this.TotalAmount) < parseInt(this.AmountPaid)) {
                this.AmountPending = (parseInt(this.TotalAmount) - parseInt(this.AmountPaid));
                this.amountPaidExceed = true;
            }
        }
        catch (error) {
            this.AmountPending = 0;
        }
    }

    isReadonly() { return false };

    showOrdersList() {
        this.navctr.setRoot(OrderSummary);
    }


    getOrderLists() {
        this.service.getOrders().subscribe(
            response => {
                console.log(response.status == 200);
                if (response.json().length == 0) {
                    this.ordersummaryCount = response.json().length;
                    console.log(response.json().length);
                }
                else {
                    this.ordersummaryCount = response.json().length;
                    console.log(this.ordersummaryCount);
                }
            },
            error => {
                console.log(error);
            }
        )
    }
}
