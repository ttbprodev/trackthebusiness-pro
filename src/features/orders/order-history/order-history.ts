import { Component, OnInit } from "@angular/core";
import { Platform, NavController, ToastController, ModalController, AlertController, LoadingController, NavParams } from 'ionic-angular';
import { Service } from '../../../shared/utils/service';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { NewOrder } from '../new-order/new-order';
import { FilterModel } from '../filter-model/filter-model';

@Component({
  selector: 'order-history',
  templateUrl: './order-history.html'
})
export class OrderHistory implements OnInit {
  itemShow;
  orderHistory = [];
  isWeb: any = false;
  information: any[];
  isSearchbarisOpened: any;
  public operation: string;
  businessId = 1;
  flteredOrders: any;
  isfilter: any;
  selectedIndex = 0;

  constructor(public navCtrl: NavController,
    public platform: Platform,
    public http: Http,
    public service: Service,
    private toastCtrl: ToastController,
    public modalCtrl: ModalController,
    private alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public navParams: NavParams) {
    this.getOrderHistory();
  }
  ngOnInit() {
    this.findPlatform();
  }

  ionViewWillEnter() {
    console.log("ionViewWillEnter - Order History");

  }

  findPlatform() {
    if (this.platform.is('core')) {
      this.isWeb = true;
    }
    else {
      this.isWeb = false;
    }
  }
  getOrderHistory() {
    this.service.getOrderHistory().subscribe(
      response => {
        this.orderHistory = response.json();
        console.log(response);
        if (this.orderHistory.length == 0) {
          this.navCtrl.setRoot(NewOrder);
        }
        else {
          this.formatOrderHistory();
        }
        console.log(this.orderHistory);
      });
  }
  itemSelected(order: any) {
    if (!this.isWeb)
      order.showDetails = !order.showDetails;
  }
  ///toast message
  presentToast(toastMessage: string) {
    let toast = this.toastCtrl.create({
      message: toastMessage,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }

  getorderItems(event) {
    var search = event.target.value;
    this.service.getFilterHistory(search).subscribe(response => {
      this.orderHistory = response.json()
      .filter(item => item.productName.toLowerCase().startsWith(search.toLowerCase()) )
      this.formatOrderHistory();
      console.log(this.orderHistory);
    })
  }
  formatOrderHistory() {
    for (let i = 0; i < this.orderHistory.length; i++) {
      if (this.isWeb)
        this.orderHistory[i]["showDetails"] = true;
      else
        this.orderHistory[i]["showDetails"] = false;
    }
  }
  openfilter() {
    let myModel = this.modalCtrl.create(FilterModel, { originScreenName: "OrderHistory" }, { enableBackdropDismiss: false });
    myModel.onDidDismiss(data => {
      console.log(data);
      if (data != "Close") {
        this.orderHistory = data;
        this.formatOrderHistory();
      }
    });
    myModel.present();
  }
  AddNewOrder() {
    this.navCtrl.parent.select(0);
  }
}