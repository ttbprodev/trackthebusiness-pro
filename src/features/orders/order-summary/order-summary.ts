import { Component, OnInit } from "@angular/core";
import { Platform, NavController, ToastController, ModalController, AlertController, LoadingController, NavParams } from 'ionic-angular';
import { Service } from '../../../shared/utils/service';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { NewOrder } from '../new-order/new-order';
import { FilterModel } from '../filter-model/filter-model';
import { DataStore } from "../../../shared/utils/data-store";
@Component({
  selector: 'order-summary',
  templateUrl: './order-summary.html'
})
export class OrderSummary implements OnInit {
  isWeb: any = false;
  orderSummary = [];
  information: any[];
  isSearchbarisOpened: any;
  businessId = 1;
  flteredOrders: any;
  isfilter: any;
  selectedIndex = 0;
  selectedOrdersCount = 0;
  IsAllChecked: any;
  IsItemChecked = false;
  currentOrderId: any;
  selectedOrderIds = [];
  selectedBranchId: any;
  constructor(public navCtrl: NavController,
    public platform: Platform,
    public http: Http,
    public service: Service,
    private toastCtrl: ToastController,
    public dataStore: DataStore,
    public modalCtrl: ModalController,
    private alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public navParams: NavParams) {

    this.getOrderLists();
  }

  ngOnInit() {
    this.findPlatform();
  }

  ionViewWillEnter() {
    console.log("ionViewWillEnter - Order Summary");

  }
  findPlatform() {
    if (this.platform.is('core')) {
      this.isWeb = true;
    }
    else {
      this.isWeb = false;
    }
  }
  getOrderLists() {
    this.service.getOrders().subscribe(
      response => {
        console.log(response);
        this.orderSummary = response.json();
        if (this.orderSummary.length == 0) {
          this.navCtrl.setRoot(NewOrder);
        }
      },
      error => {
        console.log(error);
      }
    )
  }
  CheckUncheckAll(IsAllChecked) {
    this.selectedOrdersCount = 0;
    for (let i = 0; i < this.orderSummary.length; i++) {
      this.orderSummary[i].selected = this.IsAllChecked;

      if (IsAllChecked == true) {
        this.selectedOrdersCount = this.selectedOrdersCount + 1;
        console.log("all selected: " + this.selectedOrdersCount);

      } else if (IsAllChecked == false) {
        if (this.selectedOrdersCount >= 0) {
          console.log(" none if selected: " + this.selectedOrdersCount);
        }
        else {
          this.selectedOrdersCount = this.selectedOrdersCount - 1;
          console.log(" none else selected: " + this.selectedOrdersCount);
        }
      }
    }
  }


  CheckUncheckHeader(orderSelected, orderId) {
    this.IsAllChecked = true;
    if (this.IsAllChecked == true) {
      this.currentOrderId = orderId;
    }
    else {

    }
    if (orderSelected == true) {
      this.selectedOrdersCount = this.selectedOrdersCount + 1;
      console.log("selectedOrdersCount if one selected: " + this.selectedOrdersCount);

    } else if (orderSelected == false) {
      this.selectedOrdersCount = this.selectedOrdersCount - 1;
      console.log("selectedOrdersCount else selected: " + this.selectedOrdersCount);
    }
    for (var i = 0; i < this.orderSummary.length; i++) {
      if (!this.orderSummary[i].selected) {
        this.IsAllChecked = false;
        this.IsItemChecked = true;
        break;
      }
      else if (this.orderSummary[i].selected == orderId) {
        this.currentOrderId = this.orderSummary[i].orderId;
        // this.currentBranchId = branchId;
      }
    }
  }




  itemSelected(order: any) {
    if (!this.isWeb) {
      //not web do this
      order.showDetails = !order.showDetails;
    }
  }
  ///toast message
  presentToast(toastMessage: string) {
    let toast = this.toastCtrl.create({
      message: toastMessage,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }
  openfilter() {
    let myModel = this.modalCtrl.create(FilterModel, { originScreenName: "OrderSummary" },
      { enableBackdropDismiss: false });
    myModel.onDidDismiss(data => {
      console.log(data);
      if (data != "Close") {
        this.orderSummary = data;
        this.formatOrderSummary();
      }
    });
    myModel.present();
  }
  AddNewOrder() {
    this.navCtrl.setRoot(NewOrder);
  }
  editOrder(orderId) {
    this.navCtrl.setRoot(NewOrder,{ orderId});
  }
  
  deleteOrder(orderId) {
    let alert = this.alertCtrl.create({
      title: 'Confirm delete',
      message: 'Do You Want To Delete this Order?',
      buttons: [
        
          {
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
                console.log('Cancel clicked');
            }
        },
        {
            text: 'Delete',
            handler: () => {
                console.log(orderId + ", " + " delete clicked.. ");
            this.service.deleteOrder(orderId).subscribe(
              response => {
                  var result = response.json();
                  this.getOrderLists();
                  this.presentToast(result.message);
                  console.log("Order deleted Successfully..");
              },
              error => {
                  console.log(error);
              });
            }
          }
      ]
  });
  alert.present();
}
  //   this.navCtrl.setRoot(OrderSummary);
  // }

  deleteMultipleOrders() {
    this.selectedOrderIds = [];
    for (var i = 0; i < this.orderSummary.length; i++) {
      if (this.orderSummary[i].selected) {
        this.selectedOrderIds.push(this.orderSummary[i].orderId);
      }
    }
    this.service.deleteMultipleOrders(this.selectedOrderIds).subscribe(
      res => {
        var result = res.json();
        console.log("Order Deleted... ", result);
        this.getOrderLists();
        this.presentToast(result.message);
      },
      error => {
        console.log(error);
      }
    );
  }

  getorderItems(event) {
    var Serval = event.target.value;
    this.service.getOrderByName(Serval).subscribe(response => {
      this.orderSummary = response.json()
        .filter(item => item.productName.toLowerCase().startsWith(Serval.toLowerCase()))
      this.formatOrderSummary();
      console.log(this.orderSummary);
    });
  }
  formatOrderSummary() {
    for (let i = 0; i < this.orderSummary.length; i++) {
      if (this.isWeb)
        this.orderSummary[i]["showDetails"] = true;
      else
        this.orderSummary[i]["showDetails"] = false;
    }
  }
}