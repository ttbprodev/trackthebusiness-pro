import { NgModule } from '@angular/core';
import { IonicPageModule, IonicPage } from 'ionic-angular';
import { PosModal } from './pos-modal';

@IonicPage()
@NgModule({
  declarations: [
    PosModal,
  ],
  imports: [
    IonicPageModule.forChild(PosModal),
  ],
})
export class PosModalModule {}
