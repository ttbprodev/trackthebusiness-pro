import { Component, ViewChild, ChangeDetectionStrategy, OnInit } from "@angular/core";
import { NavController, NavParams, ViewController, ModalController, Platform, ToastController } from "ionic-angular";
import { FormBuilder, FormGroup, FormControl } from "@angular/forms";
import { DataStore } from "../../../shared/utils/data-store";
import { Service } from "../../../shared/utils/service";

// import { ChangeDetectionStrategy } from "@angular/compiler/src/core";

@Component({
    changeDetection: ChangeDetectionStrategy.OnPush,
    selector: 'pos-modal',
    templateUrl: './pos-modal.html'
})
export class PosModal implements OnInit {
    @ViewChild('dateTime') DateTime;
    public PosForm: FormGroup;
    showList: boolean = false;
    formData: any;
    showProductList: boolean = false;
    searchQuery: string = '';
    items: string[];
    isWeb: any;
    productList: any = [];
    currentDate: any;
    formattedDate: any;
    formattedDateObj: any;
    time: any;
    imageUrl: any;
    search = [];
    unitPrice: any;
    availableQuantity: any;
    totalPrice: any;
    aditionalPrice: any;
    customerData = [];
    listCustomer = [];
    buyProducts = [];
    selectedProducts = [];
    grandTotal = 0;
    selectedProductsCount = 2;
    cartProducts = [];
    productSelected = false;
    totalAmount: any;
    selectedProductQuantity = 1;
    totalcGst = 0;
    totalsGst = 0;
    selectedCustomerId: any;
    selectedCustomerName: any;
    // selectedCustomerNameCtrl: FormControl;
    selectedCustomerMobileNumber: any;
    // selectedCustomerMobileNumberCtrl: FormControl;
    amountPaid: any;
    // amountPaidCtrl: FormControl;
    balanceAmount: any;
    // balanceAmountCtrl: FormControl;
    billNo: any;
    // billNoCtrl: FormControl;
    productName: any;
    // productNameCtrl: FormControl;
    branchId: any;
    totalsgst: any;
    // totalsgstCtrl: FormControl;
    totalcgst: any;
    // totalcgstCtrl: FormControl;
    singleProduct: any;
    showProductDetails = false;
    // billtotal: any;
    // billTotalCtrl: FormControl;
    price: any;
    // priceCtrl: FormControl;
    productquantity: any;
    // productquantityCtrl: FormControl;
    singleProductTotalAmount: any = 0;
    // singleProductTotalAmountCtrl: FormControl;
    totalsgstAllproducts: any;
    // totalsgstAllproductsCtrl: FormControl;
    totalcgstAllproducts: any;
    // totalcgstAllproductsCtrl: FormControl;
    cgst: any;
    // cgstCtrl: FormControl;
    sgst: any;
    // sgstCtrl: FormControl;
    sellQuantity = 0;
    // sellQuantityCtrl: FormControl;
    constructor(
        private formBuilder: FormBuilder,
        public dataStore: DataStore,
        private service: Service,
        public navCtrl: NavController,
        public modalCtlr: ModalController,
        public navParams: NavParams,
        public plt: Platform,
        public modalCtrl: ModalController,
        public viewCtrl: ViewController,
        private toastCtrl: ToastController,
        // private cd: ChangeDetectorRef
    ) {
        this.productList = this.navParams.get("data");
        this.getFormattedDate();
        this.getCustomers();
        this.billNo = (Math.random() * 100000).toFixed(0);

        // for (let i = 0; i < this.selectedProducts.length; i++) {
        //     this.grandTotal = this.grandTotal + parseInt(this.selectedProducts[i].singleProductTotalAmount);
        // }
        // this.amountPaidCtrl = this.formBuilder.control('');
        // this.selectedCustomerNameCtrl = this.formBuilder.control('');
        // this.selectedCustomerMobileNumberCtrl = this.formBuilder.control('');
        // this.balanceAmountCtrl = this.formBuilder.control('');
        // this.billNoCtrl = this.formBuilder.control('');
        // this.grandTotalCtrl = this.formBuilder.control('');
        // this.productNameCtrl = this.formBuilder.control('');
        // this.price = this.formBuilder.control('');
        // this.productquantityCtrl = this.formBuilder.control('');
        // this.singleProductTotalAmountCtrl = this.formBuilder.control('');
        // this.totalsgstAllproductsCtrl = this.formBuilder.control('');
        // this.totalcgstAllproductsCtrl = this.formBuilder.control('');
        // this.sgstCtrl = this.formBuilder.control('');
        // this.cgstCtrl = this.formBuilder.control('');
        // this.sellQuantityCtrl = this.formBuilder.control('');

        // this.PosForm = this.formBuilder.group({
        //     selectedCustomerName: this.selectedCustomerNameCtrl,
        //     selectedCustomerMobileNumber: this.selectedCustomerMobileNumberCtrl,
        //     amountPaid: this.amountPaidCtrl,
        //     balanceAmount: this.balanceAmountCtrl,
        //     billNo: this.billNoCtrl,
        //     grandTotal: this.grandTotalCtrl,
        //     productName: this.productNameCtrl,
        //     price: this.priceCtrl,
        //     productquantity: this.productquantityCtrl,
        //     singleProductTotalAmount: this.singleProductTotalAmountCtrl,
        //     totalsgstAllproducts: this.totalsgstAllproductsCtrl,
        //     totalcgstAllproducts: this.totalcgstAllproductsCtrl,
        //     sgst: this.sgstCtrl,
        //     cgst: this.cgstCtrl,
        //     sellQuantity: this.sellQuantityCtrl,
        // });
    }
    ngOnInit() {
        // this.getProductsList();
    }

    decrementQty(i) {
        if (this.selectedProducts[i].selectedProductQuantity <= 1) {
            alert("Minimum quantity should be atleast 1..");
        } else {
            this.selectedProducts[i].selectedProductQuantity = this.selectedProducts[i].selectedProductQuantity - 1;
            this.selectedProducts[i].singleProductTotalAmount = this.singleProductTotalAmount * this.selectedProducts[i].selectedProductQuantity;
        }
        this.calculateTotalProductsQuantity();
    }
    incrementQty(i) {
        this.selectedProducts[i].selectedProductQuantity = this.selectedProducts[i].selectedProductQuantity + 1;
        this.selectedProducts[i].singleProductTotalAmount = this.singleProductTotalAmount * this.selectedProducts[i].selectedProductQuantity;
        this.calculateTotalProductsQuantity();
    }
    addProductsToSell(event) {
        this.singleProductTotalAmount = 0;
        this.singleProductTotalAmount = (event.totalPrice + event.sGst + event.cGst) * this.selectedProductQuantity;

        if (this.selectedProducts.length == 0) {
            this.selectedProducts.push({ productName: event.productName, productId: event.productId, singleProductPrice: event.totalPrice, selectedProductQuantity: this.selectedProductQuantity, singleProductTotalAmount: this.singleProductTotalAmount, grandTotal: this.grandTotal, sGst: event.sGst, cGst: event.cGst });
        } else {
            var repeat = false;
            for (let i = 0; i < this.selectedProducts.length; i++) {
                if (this.selectedProducts[i].productId == event.productId) {
                    repeat = true;
                    this.selectedProducts[i].selectedProductQuantity = this.selectedProducts[i].selectedProductQuantity + 1;
                    this.selectedProducts[i].totalAmount = this.selectedProducts[i].totalAmount + this.selectedProducts[i].totalAmount;
                }
            }
            if (!repeat) {
                this.selectedProducts.push({ productName: event.productName, productId: event.productId, singleProductPrice: event.totalPrice, selectedProductQuantity: this.selectedProductQuantity, singleProductTotalAmount: this.singleProductTotalAmount, grandTotal: this.grandTotal, sGst: event.sGst, cGst: event.cGst });
                // this.selectedProducts.push({ productName: event.productName, productId: event.productId, totalPrice: event.totalPrice, selectedProductQuantity: this.selectedProductQuantity, totalAmount: event.totalPrice * this.selectedProductQuantity, grandTotal: this.grandTotal, sGst: event.sGst, cGst: event.cGst });
            }
        }
        console.log(this.selectedProducts);
        this.calculateTotalProductsQuantity();
    }

    // calculateGrandTotal(grandTotal,singleProductTotalAmount) {
    //     this.grandTotal = 0;
    //     this.grandTotal = parseInt (grandTotal ) + parseInt (singleProductTotalAmount);
    // }

    calculateProductDetails(i, pQty) {
        if (parseInt(pQty) < 0 || pQty == "") {

        } else {
            this.selectedProducts[i].selectedProductQuantity = parseInt(pQty);
            this.selectedProducts[i].selectedProductQuantity = parseInt(pQty);
            this.selectedProducts[i].singleProductTotalAmount = this.singleProductTotalAmount * this.selectedProducts[i].selectedProductQuantity;
        }
    }
    calculateGrandTotal() {
        this.calculateTotalProductsQuantity();
    }
    calculateTotalProductsQuantity() {
        this.sellQuantity = 0;
        this.grandTotal = 0;
        this.totalsGst = 0;
        this.totalcGst = 0;

        for (let i = 0; i < this.selectedProducts.length; i++) {
            this.totalsGst = 0;
            this.totalcGst = 0;
            this.grandTotal = this.grandTotal + this.selectedProducts[i].singleProductTotalAmount;
            this.sellQuantity = this.sellQuantity + this.selectedProducts[i].selectedProductQuantity;
            this.totalsGst = (this.totalsGst + this.selectedProducts[i].sGst) * this.sellQuantity;
            this.totalcGst = (this.totalcGst + this.selectedProducts[i].cGst) * this.sellQuantity;
        }
    }
    removeProductsToSell(productIdToRemove) {
        console.log(this.selectedProducts);
        this.selectedProducts.splice(this.selectedProducts.indexOf(productIdToRemove), 1);
        // for (let i = 0; i < this.selectedProducts.length; i++) {
        //     this.selectedProducts.splice(this.selectedProducts.indexOf(this.selectedProducts[i].productIdToRemove), 1);
        // }
        console.log(this.selectedProducts);
        this.calculateTotalProductsQuantity();
    }
    selectProduct() {
        this.productSelected = !this.productSelected;
    }

    // itemDetailPosModal(i) {
    //     let posModal = this.modalCtrl.create(PosModal, { data: this.selectedProducts },{ cssClass:"pos-modal"});
    //     posModal.onDidDismiss(data => {
    //         console.log(data);
    //     });
    //     posModal.present();
    // }

    deleteSelectedItem(productId) {
        alert("deleted");
        this.selectedProducts.splice(productId, 1);
    }

    getFormattedDate() {
        var date = new Date();
        var year = date.getFullYear().toString();
        var month = date.getMonth().toString();
        var todaydDate = date.getDate().toString();
        var Hours = date.getHours().toLocaleString();
        var Minutes = date.getMinutes().toLocaleString();
        var monthArray = ['jan', 'feb', 'mar', 'apr', 'may', 'june', 'july', 'aug', 'sep', 'oct', 'nov', 'dec'];
        this.formattedDate = todaydDate + '-' + monthArray[month] + '-' + year;
        this.formattedDateObj = new Date(this.formattedDate);
        this.time = Hours + ':' + Minutes;
    }
    getCustomers() {
        this.service.getCustomer().subscribe(
            response => {
                this.customerData = response.json();
                this.listCustomer = this.customerData;
            });
    }
    getItems(ev: any) {
        // Reset items back to all of the items
        this.getCustomers();
        // set val to the value of the searchbar
        let val = ev.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            // Filter the items
            this.listCustomer = this.customerData.filter((customer) => {
                return (customer.customerMobile.toLowerCase().startsWith(val.toLowerCase()));
            });
            // Show the results
            this.showList = true;
        } else {
            // hide the results when the query is empty
            this.showList = false;
        }
    }
    getProductsList() {
        this.service.getProductlist(this.dataStore.selectedBusiness.businessId).subscribe(
            response => {
                this.productList = response.json();
                this.formatProductList();
                console.log(this.productList);
            });
    }
    itemSelected(singleProduct) {
        this.singleProduct = singleProduct;
        this.showProductDetails = true;
        // if (!this.isWeb) // not web do this
        // singleProduct.showDetails = !singleProduct.showDetails;
    }
    formatProductList() {
        for (let i = 0; i < this.productList.length; i++) {
            if (this.isWeb)
                this.productList[i]["showDetails"] = true;
            else
                this.productList[i]["showDetails"] = false;
        }
    }
    dismiss() {
        this.viewCtrl.dismiss();
    }

    getProductsItems(ev: any) {
        // Reset items back to all of the items
        this.getProductsList();
        // set val to the value of the searchbar
        let val = ev.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            // Filter the items
            this.productList = this.productList.filter((product) => {
                return (product.productName.toLowerCase().startsWith(val.toLowerCase()));
            });
            // Show the results
            this.showProductList = true;
        } else {
            // hide the results when the query is empty
            this.showProductList = false;
        }
    }
    selectCustomer(customer) {
        this.selectedCustomerName = customer.customerName;
        this.selectedCustomerMobileNumber = customer.customerMobile;
        this.selectedCustomerId = customer.custId;
        this.listCustomer = [];
    }

    AddPosData(PosForm) {
        console.log(PosForm);
        PosForm.bId = this.dataStore.selectedBranch.branchId;
        let PosFormData = this.PosForm.value;
        PosFormData.bId = this.dataStore.selectedBranch.branchId;
        console.log(PosForm.value);
        this.formData = new FormData();
        this.formData.append('buId', this.dataStore.selectedBusiness.businessId);
        this.formData.append('productId', PosForm.selectedProducts.value.productId);
        this.formData.append('bId', this.dataStore.selectedBranch.branchId);
        this.formData.append('customerId', this.PosForm.value.selectedCustomerId);
        this.formData.append('billNo', this.PosForm.value.billNo);
        this.formData.append('customerName', this.PosForm.value.selectedCustomerName);
        this.formData.append('customerMobile', this.PosForm.value.selectedCustomerMobileNumber);
        this.formData.append('cgst', PosForm.value.cgst);
        this.formData.append('sgst', PosForm.value.sgst);
        this.formData.append('productName', PosForm.value.productName);
        this.formData.append('quantity', PosForm.value.quantity);
        this.formData.append('price', PosForm.value.price);
        this.formData.append('totalPrice', PosForm.value.totalPrice);
        this.formData.append('grandTotal', PosForm.value.grandTotal);
        this.formData.append('sellQuantity', PosForm.value.sellQuantity);

        // this.formData.append('paymentType', PosForm.selectProduct.value.paymentType);
        if (this.branchId != undefined) {
            this.formData.append('branchId', this.branchId);
        }
        this.service.addPos(this.formData).subscribe(
            response => {
                console.log(response);
                if (response.status == 200) {

                    var result = response.json();
                    this.presentToast(result.message);

                    this.navCtrl.setRoot(PosModal);
                }
            },
            error => {
                console.log(error);
            }
        )
    }


    presentToast(toastMessage: string) {
        let toast = this.toastCtrl.create
            ({
                message: toastMessage,
                duration: 2000,
                position: 'bottom'
            });
        toast.present();
    }

    // proceedToPay(selectedProducts) {
    //     if (this.dataStore.selectedBranch == null || this.dataStore.selectedBranch == undefined) {
    //         alert("Please select any branch.");
    //     } else {
    //         //    this.branchId = this.dataStore.branchesList[0].branchId;
    //         this.service.addpos(this.dataStore.selectedBranch.branchId, this.selectedCustomerMobileNumber, this.selectedCustomerName, selectedProducts).subscribe(
    //             res => {
    //                 console.log(res);
    //             },
    //             err => {
    //                 console.log(err);
    //             }
    //         )
    //     }
    // }


    calculateAmountPaid(grandTotal, amountPaid) {
        this.balanceAmount = parseInt(grandTotal) - parseInt(amountPaid);

    }
    SaveCart() {
        // let customer = {customerName: this.selectedCustomerName, customerMobileNumber: this.selectedCustomerMobileNumber};
        let posData = {
            bId: this.dataStore.selectedBranch.branchId,
            buId: this.dataStore.selectedBusiness.businessId,
            salesProducts: this.selectedProducts,
            customerName: this.selectedCustomerName,
            customerMobileNumber: this.selectedCustomerMobileNumber,
            customerId: this.selectedCustomerId,
            billNo: this.billNo,
            grandTotal: this.grandTotal,
            amountPaid: this.amountPaid,
            balanceAmount: this.balanceAmount,
            quantity: this.sellQuantity,
            totalSgst: this.totalsGst,
            totalCgst: this.totalcGst
        };
        console.log(posData);
        this.service.addPos(posData).subscribe(
            res => {
                console.log("Respose pos: " + res);
                var result = res.json();
                this.presentToast(result.message);
            },
            err => {
                console.log("Error for pos: " + err);
            }
        )
    }

}