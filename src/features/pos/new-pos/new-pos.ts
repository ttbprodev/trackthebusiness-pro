import { Component, ViewChild } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { DataStore } from "../../../shared/utils/data-store";
import { Service } from "../../../shared/utils/service";
import { NavController, ModalController, NavParams, Platform, ToastController, ViewController } from "ionic-angular";
import { PosModal } from "../pos-modal/pos-modal";
import { CalculatorModal } from "../calculator-modal/calculator-modal";
import { InvoiceLIst } from "../../invoice/invoice-list/invoice-list";
import { Invoice } from "../../invoice/new-invoice/new-invoice";


@Component({
    selector: 'new-pos',
    templateUrl: './new-pos.html'
})

export class NewSales {
    @ViewChild('dateTime') DateTime;
    public PosForm: FormGroup;
    showList: boolean = false;
    formData: any;
    showProductList: boolean = false;
    searchQuery: string = '';
    items: string[];
    isWeb: any;
    productList: any = [];
    currentDate: any;
    formattedDate: any;
    formattedDateObj: any;
    time: any;
    imageUrl: any;
    search = [];
    unitPrice: any;
    availableQuantity: any;
    totalPrice: any;
    aditionalPrice: any;
    customerData = [];
    listCustomer = [];
    buyProducts = [];
    selectedProducts = [];
    grandTotal = 0;
    selectedProductsCount = 2;
    cartProducts = [];
    productSelected = false;
    totalAmount: any;
    selectedProductQuantity = 1;
    totalcGst = 0;
    totalsGst = 0;
    selectedCustomerId: any;
    selectedCustomerName: any;
    userName: any;
    selectedCustomerMobileNumber: any;
    amountPaid: any;
    balanceAmount: any;
    billNo: any;
    productName: any;
    branchId: any;
    totalsgst: any;
    totalcgst: any;
    singleProduct: any;
    showProductDetails = false;
    price: any;
    productquantity: any;
    singleProductTotalAmount: any = 0;
    totalsgstAllproducts: any;
    totalcgstAllproducts: any;
    cgst: any;
    sgst: any;
    barcode: any;
    sellQuantity = 0;
    salesDateTime: any;
    constructor(
        public dataStore: DataStore,
        private service: Service,
        public navCtrl: NavController,
        public modalCtlr: ModalController,
        public navParams: NavParams,
        public plt: Platform,
        public modalCtrl: ModalController,
        public viewCtrl: ViewController,
        private toastCtrl: ToastController) {
        this.getProductsList();
        this.getFormattedDate();
        this.getCustomers();
        this.productList = this.navParams.get("data");
        this.getFormattedDate();
        this.getCustomers();
        // this.billNo = (Math.random() * 100000).toFixed(0);
        this.getbillNo();

    }
    decrementQty(i) {
        if (this.selectedProducts[i].selectedProductQuantity <= 1) {
            alert("Minimum quantity should be atleast 1..");
        } else {
            this.selectedProducts[i].selectedProductQuantity = this.selectedProducts[i].selectedProductQuantity - 1;
            this.selectedProducts[i].singleProductTotalAmount = this.singleProductTotalAmount * this.selectedProducts[i].selectedProductQuantity;
        }
        this.calculateTotalProductsQuantity();
    }
    incrementQty(i) {
        this.selectedProducts[i].selectedProductQuantity = this.selectedProducts[i].selectedProductQuantity + 1;
        this.selectedProducts[i].singleProductTotalAmount = this.singleProductTotalAmount * this.selectedProducts[i].selectedProductQuantity;
        this.calculateTotalProductsQuantity();
    }


    addProductsToSell(event) {
        this.singleProductTotalAmount = 0;
        // this.singleProductTotalAmount = (event.totalPrice + event.sGst + event.cGst) * this.selectedProductQuantity;
        this.singleProductTotalAmount = (event.totalPrice) * this.selectedProductQuantity;
        if (this.selectedProducts.length == 0) {
            this.selectedProducts.push({ productName: event.productName, productId: event.productId, singleProductPrice: event.totalPrice, selectedProductQuantity: this.selectedProductQuantity, singleProductTotalAmount: this.singleProductTotalAmount, grandTotal: this.grandTotal, sGst: event.sGst, cGst: event.cGst, barcode: event.barcode });
            // this.selectedProducts.push({ productName: event.productName, productId: event.productId, totalPrice: event.totalPrice, productDesc: event.productDesc, quantity: this.selectedProductQuantity, singleProductTotalAmount: this.singleProductTotalAmount, cgst: event.cGst, sgst: event.sGst, barcode: event.barcode });
        } else {
            var repeat = false;
            for (let i = 0; i < this.selectedProducts.length; i++) {
                if (this.selectedProducts[i].productId == event.productId) {
                    repeat = true;
                    this.selectedProducts[i].selectedProductQuantity = this.selectedProducts[i].selectedProductQuantity + 1;
                    this.selectedProducts[i].totalAmount = this.selectedProducts[i].totalAmount + this.selectedProducts[i].totalAmount;
                }
            }
            if (!repeat) {
                this.selectedProducts.push({ productName: event.productName, productId: event.productId, singleProductPrice: event.totalPrice, selectedProductQuantity: this.selectedProductQuantity, singleProductTotalAmount: this.singleProductTotalAmount, grandTotal: this.grandTotal, sGst: event.sGst, cGst: event.cGst, barcode: event.barcode });
                // this.selectedProducts.push({ productName: event.productName, productId: event.productId, totalPrice: event.totalPrice, productDesc: event.productDesc, quantity: this.selectedProductQuantity, singleProductTotalAmount: this.singleProductTotalAmount, cgst: event.cGst, sgst: event.sGst, barcode: event.barcode });
                // this.selectedProducts.push({ productName: event.productName, productId: event.productId, totalPrice: event.totalPrice, selectedProductQuantity: this.selectedProductQuantity, totalAmount: event.totalPrice * this.selectedProductQuantity, grandTotal: this.grandTotal, sGst: event.sGst, cGst: event.cGst });
            }
        }
        console.log(this.selectedProducts);
        this.calculateTotalProductsQuantity();
    }

    // calculateGrandTotal(grandTotal,singleProductTotalAmount) {
    //     this.grandTotal = 0;
    //     this.grandTotal = parseInt (grandTotal ) + parseInt (singleProductTotalAmount);
    // }

    calculateProductDetails(i, pQty) {
        if (parseInt(pQty) < 0 || pQty == "") {

        } else {
            this.selectedProducts[i].selectedProductQuantity = parseInt(pQty);
            this.selectedProducts[i].selectedProductQuantity = parseInt(pQty);
            this.selectedProducts[i].singleProductTotalAmount = this.singleProductTotalAmount * this.selectedProducts[i].selectedProductQuantity;
        }
    }
    calculateGrandTotal() {
        this.calculateTotalProductsQuantity();
    }
    calculateTotalProductsQuantity() {
        this.sellQuantity = 0;
        this.grandTotal = 0;
        this.totalsGst = 0;
        this.totalcGst = 0;

        for (let i = 0; i < this.selectedProducts.length; i++) {
            this.totalsGst = 0;
            this.totalcGst = 0;
            this.grandTotal = this.grandTotal + this.selectedProducts[i].singleProductTotalAmount;
            this.sellQuantity = this.sellQuantity + this.selectedProducts[i].selectedProductQuantity;
            this.totalsGst = (this.totalsGst + this.selectedProducts[i].sGst) * this.sellQuantity;
            this.totalcGst = (this.totalcGst + this.selectedProducts[i].cGst) * this.sellQuantity;
        }
    }

    removeProductsToSell(productIdToRemove) {
        console.log(this.selectedProducts);
        this.selectedProducts.splice(this.selectedProducts.indexOf(productIdToRemove), 1);
        // for (let i = 0; i < this.selectedProducts.length; i++) {
        //     this.selectedProducts.splice(this.selectedProducts.indexOf(this.selectedProducts[i].productIdToRemove), 1);
        // }
        console.log(this.selectedProducts);
        this.calculateTotalProductsQuantity();
    }


    selectProduct() {
        this.productSelected = !this.productSelected;
    }


    deleteSelectedItem(productId) {
        alert("deleted");
        this.selectedProducts.splice(productId, 1);
    }

    itemDetailPosModal(i) {
        let posModal = this.modalCtrl.create(PosModal, { data: this.productList }, { cssClass: "pos-modal" });
        posModal.onDidDismiss(data => {
            console.log(data);
        });
        posModal.present();
    }
    getFormattedDate() {
        var date = new Date();
        var year = date.getFullYear().toString();
        var month = date.getMonth().toString();
        var todaydDate = date.getDate().toString();
        var Hours = date.getHours().toLocaleString();
        var Minutes = date.getMinutes().toLocaleString();
        var monthArray = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
        // this.formattedDate = todaydDate + '-' + monthArray[month] + '-' + year;
        this.formattedDate = year + '-' + monthArray[month] + '-' + todaydDate;
        this.formattedDateObj = new Date(this.formattedDate);
        this.time = Hours + ':' + Minutes;

    }
    getCustomers() {
        this.service.getCustomer().subscribe(
            response => {
                this.customerData = response.json();
                this.listCustomer = this.customerData;
            });
    }
    // getItems(ev: any) {
    //     // Reset items back to all of the items
    //     this.getCustomers();
    //     // set val to the value of the searchbar
    //     let val = ev.target.value;
    //     // if the value is an empty string don't filter the items
    //     if (val && val.trim() != '') {
    //         // Filter the items
    //         this.listCustomer = this.customerData.filter((customer) => {
    //             return (customer.customerName.toLowerCase().startsWith(val.toLowerCase()));
    //         });
    //         // Show the results
    //         this.showList = true;
    //     } else {
    //         // hide the results when the query is empty
    //         this.showList = false;
    //     }
    // }

    getItems(event) {
        var searchString = event.target.value;
        console.log(searchString);
        if (searchString && searchString.trim() != "") {
            this.listCustomer = this.customerData.filter(customer => {
                return (customer.customerMobile.toLowerCase().startsWith(searchString.toLowerCase()))
            });
            this.showList = true;
        }
        else {
            this.showList = false;
        }
    }

    getProductsList() {
        this.service.getProductlist(this.dataStore.selectedBusiness.businessId).subscribe(
            response => {
                this.productList = response.json();
                this.formatProductList();
                console.log(this.productList);
            });
    }
    itemSelected(singleProduct) {
        this.singleProduct = singleProduct;
        this.showProductDetails = true;
        // if (!this.isWeb) // not web do this
        // singleProduct.showDetails = !singleProduct.showDetails;
    }
    formatProductList() {
        for (let i = 0; i < this.productList.length; i++) {
            if (this.isWeb)
                this.productList[i]["showDetails"] = true;
            else
                this.productList[i]["showDetails"] = false;
        }
    }
    presentToast(toastMessage: string) {
        let toast = this.toastCtrl.create
            ({
                message: toastMessage,
                duration: 2000,
                position: 'bottom'
            });
        toast.present();
    }
    calculateAmountPaid(grandTotal, amountPaid) {
        this.balanceAmount = parseInt(grandTotal) - parseInt(amountPaid);

    }
    getbillNo() {
        this.service.getBillNo().subscribe(
            res => {
                console.log("Respose pos: " + res);
                var result = res.json();
                this.billNo = result;
            },
            err => {
                console.log("Error for pos: " + err);
            }
        )

    }




    SaveCart() {
        // let customer = {customerName: this.selectedCustomerName, customerMobileNumber: this.selectedCustomerMobileNumber};
        this.userName = this.dataStore.loginMetaData.firstName + " " + this.dataStore.loginMetaData.lastName;
        // this.salesDateTime =this.formattedDate+" "+this.time;
        let posData = {
            bId: this.dataStore.selectedBranch.branchId,
            buId: this.dataStore.selectedBusiness.businessId,
            salesProducts: this.selectedProducts,
            customerName: this.selectedCustomerName,
            customerMobileNumber: this.selectedCustomerMobileNumber,
            customerId: this.selectedCustomerId,
            billNo: this.billNo,
            grandTotal: this.grandTotal,
            amountPaid: this.amountPaid,
            balanceAmount: this.balanceAmount,
            quantity: this.sellQuantity,
            totalSgst: this.totalsGst,
            totalCgst: this.totalcGst,
            barcode: this.barcode,
            userName: this.userName,
            salesDate: this.formattedDate,
            salesTime: this.time,


        };
        console.log(posData);
        this.service.addPos(posData).subscribe(
            res => {
                console.log("Respose pos: " + res);
                var result = res.json();
                this.presentToast(result.message);
                this.navCtrl.setRoot(Invoice, { 'posData': posData });

            },
            err => {
                console.log("Error for pos: " + err);
            }
        )
    }
    selectCustomer(customer) {
        this.selectedCustomerName = customer.customerName;
        this.selectedCustomerMobileNumber = customer.customerMobile;
        this.selectedCustomerId = customer.custId;
        this.listCustomer = [];
    }
    getProductsItems(ev: any) {
        // Reset items back to all of the items
        this.getProductsList();
        // set val to the value of the searchbar
        let val = ev.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            // Filter the items
            this.productList = this.productList.filter((product) => {
                return (product.productName.toLowerCase().startsWith(val.toLowerCase()));
            });
            // Show the results
            this.showProductList = true;
        } else {
            // hide the results when the query is empty
            this.showProductList = false;
        }
    }

    printInvoice() {
        this.navCtrl.setRoot(InvoiceLIst);
    }


    openCalculator() {


        let calculatorModal = this.modalCtrl.create(CalculatorModal, { data: this.productList }, { cssClass: "calculator-modal" });

        // calculatorModal.onDidDismiss().then((data) => {

        //     const data1 = data['data'];

        calculatorModal.onDidDismiss(data => {
            console.log(data);
        });


        calculatorModal.present().then(res => {
            let denom = res;
        });


        // let calculatorModal = this.modalCtrl.create(CalculatorModal, { data: this.productList }, { cssClass: "calculator-modal" });
        // calculatorModal.onDidDismiss(data => {
        //     console.log(data);
        // });
        // calculatorModal.present().then(res => {
        //     let denom = res;
        // });
    }

}