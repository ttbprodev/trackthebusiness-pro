import { Component } from "@angular/core";
import { NavController, NavParams, ViewController } from "ionic-angular";


@Component({
    selector: 'calculator-modal',
    templateUrl: './calculator-modal.html'
})
export class CalculatorModal {
    n1: number; n2: number; n5: number; n10: number; n20: number; n50: number; n100: number; n200: number; n500: number; n2000: number;
    v1: number; v2: number; v5: number; v10: number; v20: number; v50: number; v100: number; v200: number; v500: number; v2000: number;
    totalAmount: number = 0;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public viewCtrl: ViewController,
    ) {
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad CalculatorPage');
    }
    calculateDenom(denom) {
        switch (denom) {
            case 2000: {
                this.v2000 = this.n2000 * 2000;
                this.totalAmount = this.totalAmount + this.v2000;
                break;
            }
            case 500: {
                this.v500 = this.n500 * 500;
                this.totalAmount = this.totalAmount + this.v500;
                break;
            }
            case 100: {
                this.v100 = this.n100 * 100;
                this.totalAmount = this.totalAmount + this.v100;
                break;
            }
            case 50: {
                this.v50 = this.n50 * 50;
                this.totalAmount = this.totalAmount + this.v50;
                break;
            }
            case 20: {
                this.v20 = this.n20 * 20;
                this.totalAmount = this.totalAmount + this.v20;
                break;
            }
            case 10: {
                this.v10 = this.n10 * 10;
                this.totalAmount = this.totalAmount + this.v10;
                break;
            }
            case 5: {
                this.v5 = this.n5 * 5;
                this.totalAmount = this.totalAmount + this.v5;
                break;
            }
            case 2: {
                this.v2 = this.n2 * 2;
                this.totalAmount = this.totalAmount + this.v2;
                break;
            }
            case 1: {
                this.v1 = this.n1 * 1;
                this.totalAmount = this.totalAmount + this.v1;
                break;
            }
        }
        console.log("denominations.." + denom);
    }
    // this.calculateTotalAmount();

    //   }
    //   calculateTotalAmount() {
    //     console.log(this.totalAmount);
    //     // this.totalAmount = this.v1 + this.v2 + this.v5 + this.v10 + this.v20 + this.v50 + this.v100 + this.v200 + this.v500 + this.v2000;

    //   }
    //   calculateTotal() {
    //     console.log("calculations..");
    dismiss(data) {
        this.viewCtrl.dismiss(data);
    }
}
