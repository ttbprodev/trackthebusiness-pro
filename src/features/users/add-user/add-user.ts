import { NavParams, NavController, AlertController, ModalController, Platform, ViewController, ModalOptions, LoadingController, ToastController } from "ionic-angular";
import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators, FormArray } from "@angular/forms";
import * as XLSX from 'xlsx';

import { DataStore } from "../../../shared/utils/data-store";
import { Service } from "../../../shared/utils/service";
import { ListUsers } from "../list-users/list-users";
import { ExistingUsers } from "../existing-users/existing-users";
import { Constants } from "../../../shared/utils/constants";
@Component({
    selector: 'add-user',
    templateUrl: './add-user.html',
})
export class AddUser implements OnInit {
    isWeb: any;
    businesslist: Array<any> = [];
    formData: any;
    businessId: any;
    userData: any;
    userId: any;
    businessClicked: any;
    public form: FormGroup;
    usersExist = false;

    emailKeys = [];
    emailslist: string = "";
    addByDocument = false;
    arrayBuffer: any;
    docName = "";

    usersFromDoc = [];
    fileUrl: any;
    fileType: any;
    file: File;
    usersList: any;
    loading = this.loadingCtrl.create({
        spinner: 'bubbles',
        content: 'Please wait...'
    });

    constructor(
        public dataStore: DataStore,
        public service: Service,
        public navCtrl: NavController,
        public navParams: NavParams,
        private formBuilder: FormBuilder,
        public alertCtrl: AlertController,
        public modalCtrl: ModalController,
        public viewCtrl: ViewController,
        public plt: Platform,
        public loadingCtrl: LoadingController,
        public toastCtrl: ToastController
    ) {
        this.form = this.formBuilder.group({
            user: this.formBuilder.array([
                this.initUserFields()
            ]),
        });

        this.businesslist = this.dataStore.businessList;
        if (this.businesslist.length > 0) {
            this.dataStore.selectedBusiness = this.dataStore.selectedBusiness ? this.dataStore.selectedBusiness : this.businesslist[0];
        }
        console.log(this.businessId);
        this.userData = sessionStorage.getItem('userData');
        this.businessId = sessionStorage.getItem('businessId');
    }

    ngOnInit() {
        this.businessId = this.navParams.data;
        this.businessClicked = this.dataStore.businessList.find((business) => {
            return business.businessId === this.businessId;
        });
        this.setImageUrl(this.businessClicked);
        this.findPlatform();
        this.getUsersList(this.businessId);
    }
    findPlatform() {
        if (this.plt.is('core')) {
            this.isWeb = true;
            console.log("web platform");
        }
        else {
            this.isWeb = false;
        }
    }
    setImageUrl(business) {
        if (business) {
            business["imageUrl"] = "";
            business.image = business.image == "undefined" ? undefined : business.image;
            if (!business.image) {
                business.imageUrl = "./../../../assets/alphabates/" + business.businessName.substr(0, 1) + ".png";
            } else {
                business.imageUrl = Constants.BASE_URL + business.image;
            }
        }
    }
    initUserFields(): FormGroup {
        return this.formBuilder.group({
            email: ['', Validators.required],
            roles: ['', Validators.required],
            businessId: [this.businessId]
        });
    }
    addNewInputField(): void {
        const usersControl = <FormArray>this.form.controls.user;
        usersControl.push(this.initUserFields());
    }
    removeInputField(i: number): void {
        const usersControl = <FormArray>this.form.controls.user;
        usersControl.removeAt(i);
    }
    presentToast(toastMessage: string) {
        let toast = this.toastCtrl.create({
            message: toastMessage,
            duration: 3000,
            position: 'bottom'
        });
        toast.present();
    }

    addUsers(userDetails) {
        console.dir(userDetails);
        if (userDetails.constructor === Array) {
            this.usersList = userDetails;
        }
        else if (userDetails.constructor === Object) {
            userDetails.user[0].businessId = this.businessId;
            this.usersList = userDetails.user;
        }
        //this.loading.present();
        this.service.addingUser(this.usersList).subscribe(
            response => {
                console.log(response);
                if (response.status == 200) {
                    this.presentToast("Invitation email sent successfully.");
                    this.navCtrl.setRoot(ListUsers, this.businessId);
                }
                else {
                    alert("Please enter Correct details !!");
                    this.navCtrl.setRoot(AddUser, this.businessId);
                }
            },
            error => {
                try {
                    if (error.status == 500) {
                        alert("One or more fields entered incorrectly.");
                    }
                    alert(error);
                    let emailObj = error.json();
                    for (var k in emailObj) {
                        this.emailKeys.push(k);
                    }
                    console.log(this.emailKeys);
                    let emailStr: string = "";
                    for (let i = 0; i < this.emailKeys.length; i++) {
                        emailStr = emailStr + (this.emailKeys[i]);
                    }
                    let existingUsersModal = this.modalCtrl.create(ExistingUsers, { data: this.emailKeys, businessId: this.businessId });
                    existingUsersModal.onDidDismiss(data => {
                        console.log(data);
                        this.navCtrl.setRoot(ListUsers, this.businessId);
                    });
                    existingUsersModal.present();
                }
                catch (e) {
                    console.log(e);
                }
            }
        );
        //this.loading.dismiss();
    }
    uploadByDocument() {
        this.addByDocument = true;
    }
    onDocUpload(file) {
        // this.fileUrl = "";
        // this.fileUrl = file[0];
        this.file = null;
        this.file = file[0];
        try {
            this.docName = this.file.name;
            let fileReader = new FileReader();
            fileReader.onload = (e) => {
                this.arrayBuffer = fileReader.result;
                var data = new Uint8Array(this.arrayBuffer);
                var arr = new Array();
                for (var i = 0; i != data.length; ++i) {
                    arr[i] = String.fromCharCode(data[i]);
                }
                var bstr = arr.join("");
                var workbook = XLSX.read(bstr, { type: "binary" });
                var first_sheet_name = workbook.SheetNames[0];
                var worksheet = workbook.Sheets[first_sheet_name];
                let docObjArray = XLSX.utils.sheet_to_json(worksheet, { raw: true });
                this.usersFromDoc = docObjArray;
            }
            fileReader.readAsArrayBuffer(this.file);
        }
        catch (error) {
            console.log(error);
        }
    }
    uploadDocument() {
        this.usersFromDoc.map(v => v.businessId = this.businessId);
        for (let i = 0; i < this.usersFromDoc.length; i++) {
            delete this.usersFromDoc[i].__rowNum__;
        }
        this.addUsers(this.usersFromDoc);
    }
    cancelUploadByDocument() {
        this.addByDocument = false;
    }
    listUsers() {
        this.navCtrl.setRoot(ListUsers, this.businessId);
    }
    getUsersList(businessId) {
        this.service.ListingUsers(businessId).subscribe(
            response => {
                let empData = response.json();
                if (empData != 0) {
                    this.usersExist = true;
                }
                else {
                    this.usersExist = false;
                }
            });
    }
}