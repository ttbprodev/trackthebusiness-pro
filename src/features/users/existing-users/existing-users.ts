import { Component } from "@angular/core";
import { NavParams, ViewController, NavController, Platform } from "ionic-angular";
import { AddUser } from "../add-user/add-user";

@Component({
    selector: 'existing-users',
    templateUrl: './existing-users.html',
})
export class ExistingUsers {
    isWeb: any;
    usersList: any;
    businessId: any;

    constructor(
        public navParams: NavParams,
        public viewCtrl: ViewController,
        public navCtrl: NavController,
        public plt: Platform
    ) {
        this.usersList = this.navParams.get('data');
        this.businessId = this.navParams.get('businessId');
        this.findPlatform();
    }
    findPlatform() {
        if (this.plt.is('core')) {
          this.isWeb = true;
          console.log("web platform");
        }
        else {
          this.isWeb = false;
        }
      }
    dismiss() {
        this.viewCtrl.dismiss(AddUser, this.businessId);
        // this.navCtrl.setRoot();
      }
}