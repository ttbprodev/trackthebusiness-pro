import { Component } from "@angular/core";
import { NavParams, NavController, ViewController, Platform, LoadingController, ToastController } from "ionic-angular";
import { Service } from "../../../shared/utils/service";
import { ListUsers } from "../list-users/list-users";

@Component({
    selector: 'edit-user',
    templateUrl: './edit-user.html'
})
export class EditUser {
    isWeb: any;
    userData: any;
    formData: any;
    loading = this.loadingCtrl.create({
        spinner: 'bubbles',
        content: 'Please wait...'
    });

    constructor(
        navParams: NavParams,
        public service: Service,
        public navCtrl: NavController,
        public viewCtrl: ViewController,
        public plt: Platform,
        public loadingCtrl: LoadingController,
        public toastCtrl: ToastController
    ) {
        console.log();
        console.log('User Data: ', navParams.data);
        this.userData = navParams.data;
    }
    findPlatform() {
        if (this.plt.is('core')) {
            this.isWeb = true;
            console.log("web platform");
        }
        else {
            this.isWeb = false;
        }
    }
    presentToast(toastMessage: string) {
        let toast = this.toastCtrl.create({
            message: toastMessage,
            duration: 3000,
            position: 'bottom'
        });
        toast.present();
    }
    dismiss(singleUserData) {
        this.formData = new FormData();
        this.formData.append('businessId', singleUserData.businessId);
        this.formData.append('email', singleUserData.email);
        this.formData.append('roles', this.userData.roles);
        this.formData.append('userId', singleUserData.userId);
        //this.loading.present();
        this.service.editingUser(this.formData).subscribe(
            response => {
                this.presentToast("Updated Successfully.");
                // this.navCtrl.setRoot(ListUsers, singleUserData.businessId);
            },
            error => {
                alert(error);
            }
        );
        //this.loading.dismiss();
        this.viewCtrl.dismiss(singleUserData);
    }
    closeModal()
    {
        this.viewCtrl.dismiss();
    }
}