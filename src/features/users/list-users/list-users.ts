import { Component, OnInit } from "@angular/core";
import { DataStore } from "../../../shared/utils/data-store";
import { Service } from "../../../shared/utils/service";
import {
  NavParams,
  NavController,
  ToastController,
  AlertController,
  ModalController,
  Platform,
  LoadingController
} from "ionic-angular";
import { AddUser } from "../add-user/add-user";
import { Constants } from "../../../shared/utils/constants";
import { EditUser } from "../edit-user/edit-user";
@Component({
  selector: "list-users",
  templateUrl: "./list-users.html"
})
export class ListUsers implements OnInit {
  isWeb: any;
  businessId: any;
  enableSearch = false;
  enableEdit = true;
  myInput: any;
  seachQuery: string;
  empData;
  businessClicked: any;
  listUsers;
  loading = this.loadingCtrl.create({
    spinner: 'bubbles',
    content: 'Please wait...'
  });

  constructor(
    public dataStore: DataStore,
    public service: Service,
    public navCtrl: NavController,
    public navParams: NavParams,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public plt: Platform,
    public loadingCtrl: LoadingController
  ) {
    this.setImageUrl(this.businessClicked);
  }

  ngOnInit() {
    this.businessId = this.navParams.data;
    this.businessClicked = this.dataStore.businessList.find((business) => {
      return business.businessId === this.businessId;
    });
    this.setImageUrl(this.businessClicked);
    this.findPlatform();
    this.getUsersList(this.businessId);
  }
  findPlatform() {
    if (this.plt.is('core')) {
      this.isWeb = true;
      console.log("web platform");
    }
    else {
      this.isWeb = false;
    }
  }
  addUser() {
    this.navCtrl.setRoot(AddUser, this.businessId);
  }
  setImageUrl(business) {
    if (business) {
      business["imageUrl"] = "";
      business.image = business.image == "undefined" ? undefined : business.image;
      if (!business.image) {
        business.imageUrl = "./../../../assets/alphabates/" + business.businessName.substr(0, 1) + ".png";
      } else {
        business.imageUrl = Constants.BASE_URL + business.image;
      }
    }
  }

  deleteUser(empId) {
    let alert = this.alertCtrl.create({
      title: "Confirm delete",
      message: "Do you want to Delete User?",
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        },
        {
          text: "Delete",
          handler: () => {
            this.service.deletingUser(empId).subscribe(
              response => {
                if (response.status == 200) {
                  console.log(response);
                  this.presentToast("Deleted successfully");
                }
                this.getUsersList(this.businessId);
              },
              error => {
                this.presentToast("Service Error");
                console.log(error);
              }
            );
          }
        }
      ]
    });
    alert.present();
  }
  presentToast(toastMessage: string) {
    let toast = this.toastCtrl.create({
      message: toastMessage,
      duration: 3000,
      position: "bottom"
    });
    toast.present();
  }

  getUsersList(businessId) {
    //this.loading.present();
    this.service.ListingUsers(businessId).subscribe(
      response => {
        this.empData = response.json();
        console.log(this.empData);
        if (this.empData == 0) {
          this.navCtrl.setRoot(AddUser, this.businessId);
        }
        this.listUsers = this.empData;
        //this.loading.dismiss();
      });
  }

  searchUser() {
    this.enableSearch = !this.enableSearch;
  }

  getUsers(event) {
    var searchString = event.target.value;
    console.log(searchString);
    if (searchString && searchString.trim() != "") {
      this.listUsers = this.empData.filter(user => {
        return (user.email.toLowerCase().indexOf(searchString.toLowerCase()) > -1)
      })
    }
    else {
      this.listUsers = this.empData;
    }
  }

  editUser(singleUserData) {
    let editModal = this.modalCtrl.create(EditUser, singleUserData);
    editModal.present();
    editModal.onDidDismiss(data => {
      this.navCtrl.setRoot(ListUsers, singleUserData.businessId);
      console.log(data);
    });
  }
}
