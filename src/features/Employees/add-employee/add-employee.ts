import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController, Platform, LoadingController, NavParams } from 'ionic-angular';
import { NgForm, FormBuilder, Validators } from '@angular/forms';
import { Service } from '../../../shared/utils/service';
import { DataStore } from '../../../shared/utils/data-store';
import { EmployeeList } from '../employee-list/employee-list';
import * as XLSX from 'xlsx';

@Component({
    selector: 'add-employee',
    templateUrl: 'add-employee.html',
})
export class AddEmployee implements OnInit {
    @ViewChild('f') EmployeeForm: NgForm;
    isWeb: any;
    EmployeeDetails: any = [];
    formData: any;
    empId: any;
    firstName2: any;
    lastName2: any;
    email2: any;
    mobile2: any;
    address3: any;
    address4: any;
    country2: any;
    state2: any;
    city2: any;
    pincode2: any;
    employeeId2: any;
    roles2: any;
    joinedDate2: any;
    releasedDate2: any;
    dateOfBirth2: any;
    notes2: any;
    EmployeeListsData: any;
    addByDocument = false;
    arrayBuffer: any;
    docName = "";
    EmployeeFromDoc = [];
    fileUrl: any;
    fileType: any;
    file: File;
    countriesList = [];
    statesList = [];
    citiesList = [];
    employeeCounts: any;
    constructor(
        public navCtrl: NavController,
        public plt: Platform,
        private formBuilder: FormBuilder,
        private service: Service,
        public loadingCtrl: LoadingController,
        private dataStore: DataStore,
        public navParams: NavParams) {
        this.EmployeeDetails = this.formBuilder.group({
            firstName: ['', Validators.required, Validators.minLength(3), Validators.maxLength(50),],
            lastName: [''],
            email: ['', Validators.required, Validators.minLength(3), Validators.maxLength(50),],
            mobile: ['', Validators.required, Validators.minLength(3), Validators.maxLength(50),],
            employeeId: ['', Validators.required, Validators.minLength(3), Validators.maxLength(50),],
            roles: ['', Validators.required, Validators.minLength(3), Validators.maxLength(50),],
            dateOfBirth: ['', Validators.required, Validators.minLength(3), Validators.maxLength(50),],
        })
        this.getEmployeeList();
        this.getCountriesList();
        let EmployeeLists = this.navParams.data;
        this.empId = EmployeeLists.empId;
        if (EmployeeLists.country != undefined) {
            this.getStatesList(EmployeeLists.country);
        }
        if (EmployeeLists.state != undefined) {
            this.getCitiesList(EmployeeLists.state);
        }
        if (this.empId != undefined) {
            this.showEmployeeInfo(this.empId);
        }
    }
    ngOnInit() {
        this.findPlatform();

    }
    findPlatform() {
        if (this.plt.is('core')) {
            this.isWeb = true;
            console.log("web platform");
        }
        else {
            this.isWeb = false;
        }
    }
    ionViewWillEnter() {

    }
    getEmployeeList() {
        this.service.getEmployeesList().subscribe(
            response => {
                console.log(response.status == 200);
                // this.branchesList = response.json();
                if (response.json().length == 0) {
                    this.employeeCounts = response.json().length;
                    console.log(response.json().length);
                }
                else {
                    this.employeeCounts = response.json().length;
                    console.log(this.employeeCounts);
                }
            },
            error => {
                console.log(error);
            }
        )
    }
    addEmployee(EmployeeDetails) {
        this.formData = new FormData();
        // this.formData.append("bId",this.dataStore.branchesList[0].branchId);
        this.formData.append('firstName', EmployeeDetails["value"].firstName);
        this.formData.append('lastName', EmployeeDetails["value"].lastName);
        this.formData.append("email", EmployeeDetails["value"].email);
        this.formData.append("mobile", EmployeeDetails["value"].mobile);
        this.formData.append("address1", EmployeeDetails["value"].address1);
        this.formData.append("country", EmployeeDetails["value"].country);
        this.formData.append("state", EmployeeDetails["value"].state);
        this.formData.append("city", EmployeeDetails["value"].city);
        this.formData.append("pincode", EmployeeDetails["value"].pincode);
        this.formData.append("employeeId", EmployeeDetails["value"].employeeId);
        this.formData.append("roles", EmployeeDetails["value"].roles);
        this.formData.append("joinedDate", EmployeeDetails["value"].joinedDate);
        this.formData.append("releasedDate", EmployeeDetails["value"].releasedDate);
        this.formData.append("dateOfBirth", EmployeeDetails["value"].dateOfBirth)

        if (this.dataStore.selectedBranch.branchId != undefined) {
            this.formData.append("bId", this.dataStore.selectedBranch.branchId);
        }
        if (this.empId != undefined) {
            this.formData.append("empId", this.empId);
        }
        if (this.dataStore.selectedBusiness.businessId != undefined) {
            this.formData.append("buId", this.dataStore.selectedBusiness.businessId);
        }
        if (EmployeeDetails["value"].notes != undefined) {
            this.formData.append("notes", EmployeeDetails["value"].notes);
        }
        if (EmployeeDetails["value"].address2 != undefined) {
            this.formData.append("address2", EmployeeDetails["value"].address2);
        }


        this.service.addEmployee(this.formData).subscribe(
            response => {
                console.log(response);
                this.navCtrl.setRoot(EmployeeList);
            },
            error => {
                console.log(error);
            })
    }
    showEmployeeList() {
        this.navCtrl.setRoot(EmployeeList);
    }
    showEmployeeInfo(empId) {
        this.service.getEmployeeDetails(empId).subscribe(
            response => {
                var result = response.json();
                console.log("Result data for Employee view");
                this.populateEmployeeData(result);
            });
    }
    populateEmployeeData(result) {
        this.empId = result.empId;
        this.firstName2 = result.firstName;
        this.lastName2 = result.lastName;
        this.email2 = result.email;
        this.mobile2 = result.mobile;
        this.address3 = result.address1;
        this.address4 = result.address2;
        this.country2 = result.country;
        this.state2 = result.state;
        this.city2 = result.city;
        this.pincode2 = result.pincode;
        this.employeeId2 = result.employeeId;
        this.roles2 = result.employeeRoles[0].id;
        this.joinedDate2 = result.joinedDate;
        this.releasedDate2 = result.releasedDate;
        this.dateOfBirth2 = result.dateOfBirth;
        this.notes2 = result.notes;
    }
    getEmployee(empId) {
        this.service.editEmployees(empId).subscribe(
            res => {
                let result = res.json();
                this.EmployeeListsData = result;
            },
            err => {
                console.log(err);
            }
        )
    }
    getCountriesList() {
        this.service.getCountries().subscribe(
            res => {
                console.log(res.json());
                this.countriesList = res.json();
            },
            err => {
                console.log(err);
            }
        );
    }
    getStatesList(event) {
        this.statesList = [];
        this.service.getStates(event).subscribe(
            res => {
                console.log(res.json());
                this.statesList = res.json();
            },
            err => {
                console.log(err);
            }
        )
    }
    getCitiesList(event) {
        this.citiesList = [];
        this.service.getCities(event).subscribe(
            res => {
                console.log(res.json());
                this.citiesList = res.json();
            },
            err => {
                console.log(err);
            }
        )
    }
    uploadByDocument() {
        this.addByDocument = true;
    }
    onDocUpload(file) {
        // this.fileUrl = "";
        // this.fileUrl = file[0];
        this.file = null;
        this.file = file[0];
        try {
            this.docName = this.file.name;
            let fileReader = new FileReader();
            fileReader.onload = (e) => {
                this.arrayBuffer = fileReader.result;
                var data = new Uint8Array(this.arrayBuffer);
                var arr = new Array();
                for (var i = 0; i != data.length; ++i) {
                    arr[i] = String.fromCharCode(data[i]);
                }
                var bstr = arr.join("");
                var workbook = XLSX.read(bstr, { type: "binary" });
                var first_sheet_name = workbook.SheetNames[0];
                var worksheet = workbook.Sheets[first_sheet_name];
                let docObjArray = XLSX.utils.sheet_to_json(worksheet, { raw: true });
                this.EmployeeFromDoc = docObjArray;
            }
            fileReader.readAsArrayBuffer(this.file);
        }
        catch (error) {
            console.log(error);
        }
    }
    uploadDocument() {
        this.EmployeeFromDoc.map(v => v.bId = this.dataStore.selectedBranch.branchId);
        this.EmployeeFromDoc.map(v => v.buId = this.dataStore.selectedBusiness.businessId);
        for (let i = 0; i < this.EmployeeFromDoc.length; i++) {
            delete this.EmployeeFromDoc[i].__rowNum__;
        }
        this.service.addMultipleEmployees(this.EmployeeFromDoc).subscribe(
            res => {
                console.log(res);
                this.navCtrl.setRoot(EmployeeList);
            },
            err => {
                console.log(err);
            }
        )
    }
    cancelUploadByDocument() {
        this.addByDocument = false;
    }
}