import { Service } from "../../../shared/utils/service";
import { Component } from "@angular/core";
import { NavController, AlertController } from "ionic-angular";
import { AddEmployee } from "../add-employee/add-employee";
import { DataStore } from "../../../shared/utils/data-store";
import { DashboardView } from "../../dashboard-view/dashboard-view";
import { Location } from "@angular/common";

@Component({
    selector: 'employee-list',
    templateUrl: './employee-list.html'
})
export class EmployeeList {
    employeeList: any = [];
    IsAllChecked: any;
    IsItemChecked = false;
    showButton = false;
    selectedEmployeesCount = 0;
    selectedEmployeeId: any;
    currentEmployeeId: any;
    selectedEmployeesIds = [];
    businessId:any;

    constructor(
        private service: Service,
        public navCtrl: NavController,
        private alertCtrl: AlertController,
        public dataStore: DataStore,
        
    ) {
        if(this.dataStore.selectedBranch == null)
        {
            let alert = this.alertCtrl.create({
                title: 'Select',
                message: 'Please Select Branch First',
                cssClass: 'reset',
                buttons: [
                  {
                    text: 'OK',
                    handler: () => {
                      console.log('OK clicked');
                      this.navCtrl.setRoot(DashboardView);
                    }
                  }
                ]
              });
            alert.present()
            this.navCtrl.setRoot(DashboardView);
            // window.location.href="../../dashboard-view/dashboard-view.ts";
        }
        else{
            this.getEmployeeList();
        }
        
    }
    getEmployeeList() {
        this.service.getEmployeesList().subscribe(
            response => {
                console.log(response);
                this.employeeList = response.json();
                if (this.employeeList.length == 0) {
                    this.navCtrl.setRoot(AddEmployee);
                }
            },
            error => {
                console.log(error);
            }
        )
    }
    EmployeeEdit(empId) {
        console.log("change Employee id: " + empId);
    }
    CheckUncheckHeader(SelectedEmployee, empId) {
        this.IsAllChecked = true;
        if (SelectedEmployee == true) {
            this.selectedEmployeesCount = this.selectedEmployeesCount + 1;
            console.log("selectedEmployeesCount: " + this.selectedEmployeesCount);
            console.log(this.selectedEmployeesCount > 1 || this.selectedEmployeesCount < 1);
            console.log(!(this.selectedEmployeesCount > 1 || this.selectedEmployeesCount < 1));
        }
        else if (SelectedEmployee == false) {
            this.selectedEmployeesCount = this.selectedEmployeesCount - 1;
            console.log("selectedEmployeesCount: " + this.selectedEmployeesCount);
        }
        for (var i = 0; i < this.employeeList.length; i++) {
            if (!this.employeeList[i].selected) {
                this.IsAllChecked = false;
                this.IsItemChecked = true;
                break;
            }
            else if (this.employeeList[0].selected == true) {
                this.selectedEmployeeId = this.employeeList[i].empId;
            }
        }
    }
    CheckUncheckAll(IsAllChecked) {
        for (var i = 0; i < this.employeeList.length; i++) {
            this.employeeList[i].selected = this.IsAllChecked;
            if (IsAllChecked == true) {
                this.selectedEmployeesCount = this.selectedEmployeesCount + 1;
                console.log("selectedEmployeesCount: " + this.selectedEmployeesCount);

            } else if (IsAllChecked == false) {
                this.selectedEmployeesCount = this.selectedEmployeesCount - 1;
                console.log("selectedEmployeesCount: " + this.selectedEmployeesCount);
            }
        }
    }

    addNewEmployee() {
        this.navCtrl.setRoot(AddEmployee);
    }
    editSingleEmployee() {
        this.EmployeeEdit(this.selectedEmployeeId);
    }
    editEmployee(empId) {
        console.log(empId + " edit clicked..");
        this.service.getEmployeeDetails(empId).subscribe(
            response => {
                var result = response.json();
                console.log("Result data of Employees");
                this.navCtrl.setRoot(AddEmployee, result);
            },
            error => {
                console.log(error);
            });
    }
    deleteEmployee(empId) {
        let alert = this.alertCtrl.create({
            title: 'Confirm delete',
            message: 'Do you want to Delete this Employee?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: () => {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Delete',
                    handler: () => {
                        console.log(empId + ", " + " delete clicked.. ");
                        this.service.deleteEmployees(empId).subscribe(
                            response => {
                                var result = response.json();
                                console.log("Employee deleted Successfully..");
                                this.navCtrl.setRoot(EmployeeList);
                            },
                            error => {
                                console.log(error);
                            });
                    }
                }
            ]
        });
        alert.present();
    }
    deleteMultipleEmployees() {
        let EmployeesToDelete;
        for (var i = 0; i < this.employeeList.length; i++) {
            if (this.employeeList[i].selected) {
                this.selectedEmployeesIds.push(this.employeeList[i].empId);
                //console.log( JSON.stringify(this.selectedSchoolIds));
            }
        }
        EmployeesToDelete = (this.selectedEmployeesIds);

        this.service.deleteMultipleEmployees(EmployeesToDelete).subscribe(
            res => {
                if (res.status == 200) {
                    alert("Branches deleted Successfully!");
                    this.navCtrl.setRoot(EmployeeList);
                }
            },
            error => {
                console.log(error);
            }
        );
    }
}