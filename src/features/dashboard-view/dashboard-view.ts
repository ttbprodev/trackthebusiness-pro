
import { Component} from '@angular/core';
import { NavController, ToastController, NavParams, PopoverController, Platform } from 'ionic-angular';
import { Constants } from '../../shared/utils/constants';
import { Service } from '../../shared/utils/service';
import { DataStore } from '../../shared/utils/data-store';
import { BusinessView } from '../business/business-view/business-view';
import { ProductList } from '../products/product-list/product-list';
import { AddBranch } from '../branches/add-branch/add-branch';
import { BranchesList } from '../branches/branches-list/branches-list';
import { AddEmployee } from '../Employees/add-employee/add-employee';
import { AddProduct } from '../products/add-product/add-product';
import { EmployeeList } from '../Employees/employee-list/employee-list';

declare const cordova: any;
@Component({
  selector: 'dashboard-view',
  templateUrl: 'dashboard-view.html'
})
export class DashboardView {
  appName = Constants.APP_NAME;
  isWeb: any;
  productsData = [];
  orderList = [];
  employeeList = [];
  branchesList = [];
  businessList = [];
  businessId: any;
  numberOfBusiness = 0;
  numberOfProduts = 0;
  numberOfBranches = 0;
  numberOfSales = 0;
  numberOfEmployees = 0;
  numberOfEmpl = 0;
  chartOptions: any;
  businesslist: any;
  constructor(public navCtrl: NavController,
    public service: Service,
    public dataStore: DataStore,
    public navParams: NavParams,
    public popoverCtrl: PopoverController,
    private toastCtrl: ToastController,
    public plt: Platform) {
    this.dataStore.currentHeaderTitle = "Dashboard";
    this.businessId = this.navParams.get("businessId");
    this.getBusinessList();
    this.salesLineChart();
    this.findPlatform();
  }
  findPlatform() {
    if (this.plt.is('core')) {
      this.isWeb = true;
      console.log("web platform");
    }
    else {
      this.isWeb = false;
    }
  }
  getBusinessList() {
    this.businessList = this.dataStore.businessList;
    var list = this.businessList;
    if (list.length != 0) {
      this.dataStore.selectedBusiness = list[0];
      this.totalEmployees();
      this.totalBranches();
      this.totalProducts();
      this.getProductsList();
      this.getBranchesList();
    }
    else {
      this.navCtrl.setRoot(BusinessView);
    }
  }
  getBranchesList() {
    this.service.getBranchesList(this.dataStore.selectedBusiness.businessId).subscribe(
      response => {
        console.log(response);
        this.dataStore.branchesList = response.json();
        this.branchesList = this.dataStore.branchesList;
        console.log(this.branchesList);
      }
    )
  }
  branchSelect(value) {
    console.log(value);
    for(var i=0;i<this.branchesList.length;i++){
      if(this.branchesList[i].branchId == value){
        this.dataStore.selectedBranch = this.branchesList[i];
      }
      // console.log( this.branchesList[value]);
    }    
    this.totalEmployeesBrachWise();
  
  }

  salesLineChart() {
    this.chartOptions = {
      chart: {
        type: 'line'
      },
      title: {
        text: ''
      },
      xAxis: {
        categories: ['Jan,5', 'Jan,6', 'Jan,7', 'Jan,8', 'Jan,9', 'Jan,10', 'Jan,11']
      },
      yAxis: {
        title: {
          text: ''
        }
      },
      plotOptions: {
        line: {
          dataLabels: {
            enabled: true
          },
          enableMouseTracking: false
        }
      },
      series: [{
        name: 'sales',
        data: [30, 100, 136, 160, 202, 210, 225]
      }]
    }
  }


  getallfilter(value) {
    this.getproductsbyfilter(value);
    this.getOrdersbyfilter(value);
    this.getBranchesbyfilter(value);
  }

  getproductsbyfilter(event) {
    this.service.filterproducts(event).subscribe(response => {
      this.productsData = null;
      this.productsData = response.json();
      this.numberOfProduts = this.productsData.length;
      console.log(this.productsData);
    })
  }

  getOrdersbyfilter(event) {
    this.service.filterorders(event).subscribe(response => {
      this.orderList = response.json();
      this.numberOfEmployees = this.orderList.length;
      console.log(this.orderList);

    })
  }

  getBranchesbyfilter(event) {
    this.service.filterbranches(event).subscribe(response => {
      this.branchesList = response.json();
      this.numberOfBranches = this.branchesList.length;
      console.log(this.branchesList);
    })
  }
  presentToast(toastMessage: string) {
    let toast = this.toastCtrl.create({
      message: toastMessage,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }

  gotoBusiness() {
    this.navCtrl.setRoot(BusinessView);
  }
  gotoBranches() {
    this.navCtrl.setRoot(AddBranch);
  }
  gotoProduct() {
    this.navCtrl.setRoot(AddProduct);
  }
  gotoEmployee() {
    this.navCtrl.setRoot(AddEmployee);
  }
  viewBranches() {
    this.navCtrl.setRoot(BranchesList);
  }
  viewEmployees() {
    this.navCtrl.setRoot(EmployeeList);
  }
  viewProducts() {
    this.navCtrl.setRoot(ProductList);
  }
  viewSales() {
    this.navCtrl.setRoot(ProductList);
    this.navCtrl.parent.select(1);
  }
  getProductsList() {
    this.service.getProductlist(this.dataStore.selectedBusiness.businessId).subscribe(
      response => {
        this.productsData = response.json();
        this.numberOfProduts = this.productsData.length;

        console.log(this.productsData);
      });


    //   this.service.getInvoiceList().subscribe(
    //     response => {
    //         console.log(response);
    //         this.posData= response.json();
    //         this.numberOfProduts = this.posData.length;
    //     },
    //     error => {
    //         console.log(error);
    //     }
    // )
  }
  totalBranches() {
    this.service.totalNumberOfBranches(this.dataStore.selectedBusiness.businessId).subscribe(
      response => {
        var result = response.json();
        this.numberOfBranches = result;
        console.log(this.numberOfBranches);
      },
      error => {
        console.log(error);
      }
    );
  }
  totalProducts() {
    this.service.totalNumberOfProducts(this.dataStore.selectedBusiness.businessId).subscribe(
      response => {
        var result = response.json();
        this.numberOfProduts = result;
        console.log(this.numberOfProduts);
      },
      error => {
        console.log(error);
      }
    );
  }
  totalProductsBrachWise() {
    this.service.totalProductBranchWise(this.dataStore.selectedBranch.branchId).subscribe(
      response => {
        var result = response.json();
        this.numberOfEmployees = result;
        console.log(this.numberOfEmployees);
      },
      error => {
        console.log(error);
      }
    );
  }

  totalEmployees() {
    this.service.totalNumberOfEmployees(this.dataStore.selectedBusiness.businessId).subscribe(
      response => {
        var result = response.json();
        this.numberOfEmployees = result;
        console.log(this.numberOfEmployees);
      },
      error => {
        console.log(error);
      }
    );
  }
  totalEmployeesBrachWise() {
    this.service.totalEmpBranchWise(this.dataStore.selectedBranch.branchId).subscribe(
      response => {
        var result = response.json();
        this.numberOfEmployees = result;
        console.log(this.numberOfEmployees);
      },
      error => {
        console.log(error);
      }
    );
  }

  // totalSales(bid) {
  //   this.service.totalNumberOfSales(bid).subscribe(
  //     response => {
  //       var result = response.json();
  //       this.numberOfSales = result;
  //       console.log(this.numberOfSales);
  //     },
  //     error => {
  //       console.log(error);
  //     }
  //   );
  // }





}




