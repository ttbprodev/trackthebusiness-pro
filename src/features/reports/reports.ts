import { Component } from "@angular/core";
import { NavController, ToastController, NavParams, PopoverController, Platform } from 'ionic-angular';
import { Service } from '../../shared/utils/service';
import { DataStore } from '../../shared/utils/data-store';
import { Constants } from '../../shared/utils/constants';
// import { isWithinRange, isBefore } from 'date-fns';
import { OrderHistory } from "../orders/order-history/order-history";


declare const cordova: any;
@Component({
    selector: 'app-reports',
    templateUrl: './reports.html'
})
export class Reports {
    fromDate: any;
    toDate: any;
    branchesList = [];
    businessList = [];
    orderHistory = [];
    salesSummary = [];
    filterreportwise: any;
    numberOfBranches = 0;
    appName = Constants.APP_NAME;
    isWeb: any;
    invalidSelection: boolean = true;
    orderSummary = [];
    employeesList = [];
    bId;
    selectedbuId;
    selectedbrId;
    selectedEmpId;
    generateType;

    constructor(public navCtrl: NavController,
        public service: Service,
        public dataStore: DataStore,
        public navParams: NavParams,
        public popoverCtrl: PopoverController,
        private toastCtrl: ToastController,
        public plt: Platform) {

        this.getBusinessList();
        this.businessList = this.dataStore.businessList;
    }


    addfilter() {
        console.log(this.filterreportwise);
        if (this.filterreportwise == 'orders') {
            this.generateType = 'orders';

        } else this.generateType = 'sales';
    }
    // Business list
    getBusinessList() {
        this.service.getBusinessList().subscribe(response => {
            this.businessList = response.json();
        });
    }
    businessSelect(buId) {
        this.getBranchesList(buId);
        this.bId = buId;
    }
    // Branch List
    getBranchesList(buId) {
        this.branchesList = [];
        this.service.getBranchesList(buId).subscribe(
            response => {
                this.branchesList = response.json();
            }
        )
    }
    branchSelect(brId) {
        this.service.getEmployeesListByBranch(brId).subscribe(
            response => {
                this.employeesList = response.json();
            }
        )
    }
    employeeSelect(empId) {
        console.log("selected emp id: " + empId);
    }
    loadResults() {
        if (!this.fromDate || !this.toDate) {
            console.log('Date is Missing')
            return
        }

        // if (isBefore(this.toDate, this.fromDate)) {
        //     this.toastCtrl.create({
        //         message: 'Note: Start Date cannot be set in the future.',
        //         position: 'bottom',
        //         showCloseButton: true
        //     }).present();
        //     this.invalidSelection = false;
        // }
        const fromDate = new Date(this.fromDate);
        const toDate = new Date(this.toDate);
    }
    getLists() {
        let type = '';
        let id = '';
        if (this.selectedbrId == undefined) {
            type = 'business';
            id = this.selectedbuId;
        } else {
            type = 'branch';
            id = this.selectedbrId;
        }

        if (this.filterreportwise === "orders") {
            this.getOrderLists(id);
        } else this.getSalesLists(id, type);
    }
    getOrderLists(id) {
        this.service.getOrdersReports(id, this.fromDate, this.toDate).subscribe(
            response => {
                console.log(response);
                this.orderSummary = response.json();
                if (this.orderSummary.length == 0) {
                }
            },
            error => {
                console.log(error);
            }
        )
    }
    getSalesLists(id, type) {
        this.service.getSalesReports(id, this.fromDate, this.toDate, type).subscribe(
            response => {
                console.log(response);
                this.salesSummary = response.json();
                console.log(this.salesSummary);
            },
            error => {
                console.log(error);
            }
        )
    }
    //Reset Functon
    reset() {

    }
}