import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController, ViewController, Platform, NavParams } from 'ionic-angular';
import { NgForm, FormBuilder, Validators } from '@angular/forms';
import { Service } from '../../../shared/utils/service';
import { DataStore } from '../../../shared/utils/data-store';

@Component({
  selector: 'add-subcategory',
  templateUrl: 'add-subcategory.html'
})
export class AddSubCategory implements OnInit {
  categoryId: any;
  @ViewChild('f') categoryForm: NgForm;
  isWeb: any;
  SubCategoryDetails: any = [];
  formData: any
  selectedCategory: any;

  constructor(
    public navCtrl: NavController,
    public navParam: NavParams,
    private view: ViewController,
    public plt: Platform,
    private formBuilder: FormBuilder,
    private service: Service,
    private dataStore: DataStore, ) {
    this.categoryId = this.navParam.get('selectedCategoryId');
    this.SubCategoryDetails = this.formBuilder.group({
      subCategoryName: [Validators.required, Validators.minLength(3), Validators.maxLength(50),],
    })
  }
  ngOnInit() {
    this.findPlatform();
  }
  findPlatform() {
    if (this.plt.is('core')) {
      this.isWeb = true;
      console.log("web platform");
    }
    else {
      this.isWeb = false;
    }
  }
  AddSubCategory(SubCategoryDetails) {
    this.formData = new FormData();
    this.formData.append('cId', this.categoryId);
    this.formData.append('name', SubCategoryDetails["value"].subCategoryName);
    this.service.addSubCategory(this.formData).subscribe(
      response => {
        let serverResponse = response.json();
        if (serverResponse.status == 200) {
          console.log(serverResponse);
          alert(serverResponse);
          // this.navCtrl.setRoot(AddSubCategory);
          this.view.dismiss("Close");
        }
      },
      err => {
        console.log(err);
      }
    )
  }
  close() {
    this.view.dismiss("Close");
  }
}
