import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController, ViewController, Platform } from 'ionic-angular';
import { NgForm, FormBuilder, Validators } from '@angular/forms';
import { Service } from '../../../shared/utils/service';
import { DataStore } from '../../../shared/utils/data-store';
import { AddProduct } from '../add-product/add-product';

@Component({
  selector: 'add-category',
  templateUrl: 'add-category.html'
})
export class AddCategory implements OnInit {
  @ViewChild('f') categoryForm: NgForm;
  isWeb: any;
  CategoryDetails: any = [];
  formData: any

  constructor(public navCtrl: NavController,
    private view: ViewController,
    public plt: Platform,
    private formBuilder: FormBuilder,
    private service: Service,
    private dataStore: DataStore,
  ) {
    this.CategoryDetails = this.formBuilder.group({
      categoryName: [Validators.required, Validators.minLength(3), Validators.maxLength(50),],
    })
  }
  ngOnInit() {
    this.findPlatform();
  }
  findPlatform() {
    if (this.plt.is('core')) {
      this.isWeb = true;
      console.log("web platform");
    }
    else {
      this.isWeb = false;
    }
  }
  addCategory(CategoryDetails) {
    this.formData = new FormData();
    this.formData.append('buId', this.dataStore.selectedBusiness.businessId);
    this.formData.append('name', CategoryDetails["value"].categoryName);
    this.service.addCategory(this.formData).subscribe(
      response => {
        let serverResponse = response.json();
        if (serverResponse.status == 200) {
          console.log(serverResponse);
          this.navCtrl.setRoot(AddProduct);
          this.view.dismiss("Close");
        }
      },
      err => {
        console.log(err);
      }
    )
  }
  close() {
    this.view.dismiss("Close");
  }
}
