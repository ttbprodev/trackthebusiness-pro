import { Component } from "@angular/core";
import { NavController, NavParams, ViewController } from "ionic-angular";

@Component({
    selector: 'products-cart',
    templateUrl: './products-cart.html'
})
export class ProductsCart {
    selectedProducts = [];

    billNo: any;
    billTotal = 0;

    constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
        this.selectedProducts = this.navParams.get('data');
        this.billNo = (Math.random() * 100000).toFixed(0);
        for (let i = 0; i < this.selectedProducts.length; i++) {
            this.billTotal = this.billTotal + parseInt(this.selectedProducts[i].sellingPrice);
        }
    }
    dismiss() {
        this.viewCtrl.dismiss();
    }
    sellProducts() {

    }
    resetCart() {
        this.selectedProducts = [];
        this.billTotal = 0;
    }
    saveCart() {

    }
    sellAndPrint() {

    }
}