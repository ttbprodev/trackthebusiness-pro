import { Component, OnInit, ViewChild } from "@angular/core";
import { Validators, FormBuilder, FormControl, NgForm } from "@angular/forms";
import { NavParams, NavController, Platform, ToastController, LoadingController, ModalController } from "ionic-angular";
import { ProductList } from "../product-list/product-list";
import { Service } from "../../../shared/utils/service";
import { DataStore } from "../../../shared/utils/data-store";
import { Constants } from "../../../shared/utils/constants";
import * as XLSX from 'xlsx';
import { AddCategory } from "../add-category/add-category";
import { AddSubCategory } from "../add-subcategory/add-subcategory";

@Component({
    selector: 'add-product',
    templateUrl: 'add-product.html'
})
export class AddProduct implements OnInit {
    @ViewChild('f') productForm: NgForm;
    ProductDetails: any = [];
    formData: any;
    productId: any;
    businessId: any;
    imagedata: any[] = [];
    imageUrl: any;
    images: any;
    isWeb: any;
    UnitPrice: any;
    productName2: any;
    categoryName2: any;
    categoryName: any;
    subcategoryName2: any;
    subcategoryName: any;
    productDesc2: any;
    barcode2: any;
    availableQuantity2: any;
    price2: any;
    sGst2: any;
    cGst2: any;
    aditionalPrice2: any;
    totalPrice: any;
    productReword2: any;
    productId2: any;
    sellingPrice2: any;
    SGstAmount: any;
    CGstAmount: any;
    AdditionalPriceAmount: any;
    categoryId: any;
    logoSize: number;
    logoName: string;
    nameValidate: any;
    ProductNameCtrl: FormControl;
    DescriptionCtrl: FormControl;
    AvailablityCtrl: FormControl;
    PriceCtrl: FormControl;
    AdditionalCostCtrl: FormControl;
    SellingPriceCtrl: FormControl;
    addByDocument = false;
    docName = "";
    productsFromDoc = [];
    fileUrl: any;
    fileType: any;
    arrayBuffer: any;
    file: File;
    pnameKey = [];
    PrdDetail = [];
    subcategoryId: any;
    sGstType: any;
    AdditionalAmount: any;
    edited: boolean = false;
    categoriesList: any;
    subCategoriesList: any;
    selectedCategory: any;
    selectedCategoryId: any;
    buId: any;
    productsCount:any;

    constructor(public navCtrl: NavController,
        public navParams: NavParams,
        private formBuilder: FormBuilder,
        public plt: Platform,
        private toastCtrl: ToastController,
        private service: Service, public loadingCtrl: LoadingController,
        private dataStore: DataStore,
        public modalCtrl: ModalController) {
        this.edited = false;
        this.ProductDetails = this.formBuilder.group({
            businessId: [null],
            productName: ['', Validators.required, Validators.minLength(3), Validators.maxLength(50),],
            availableQuantity: ['', Validators.required, Validators.minLength(1), Validators.maxLength(50)],
            price: ['', Validators.required, Validators.minLength(1), Validators.maxLength(50)],
            totalPrice: ['', Validators.required, Validators.minLength(0), Validators.maxLength(50)],
            files: ['', Validators.required],
            productId: [null]
        });
        this.getProductList();
        let Prdetails = this.navParams.data;
        this.productId = Prdetails.productId;
        if (Prdetails.productId != undefined) {
            if (Prdetails.category[0].categoryId != undefined) {
                this.getSubCategoriesList(Prdetails.category[0].categoryId);
            }
        }
        if (this.productId != undefined) {
            this.showProductInfo(this.productId);
        }
        // if(Prdetails.result.subCategories[0].subcategoryId != undefined)
        // {
        //     this.showProductInfo(Prdetails.result.subCategories[0].subcategoryId)
        // }
    }
    ionViewWillEnter() {
        this.getCategoriesList();
    }
    getProductList()
    {
        this.service.getProduct().subscribe(
            response => {
                console.log(response.status == 200);
                
                if (response.json().length == 0) {
                    this.productsCount = response.json().length;
                    console.log(response.json().length);
                }
                else {
                    this.productsCount = response.json().length;
                    console.log(this.productsCount);
                }
            });
    }
    presentToast(toastMessage: string) {
        let toast = this.toastCtrl.create
            ({
                message: toastMessage,
                duration: 2000,
                position: 'bottom'
            });
        toast.present();
    }
    addProduct(ProductDetails) {
        this.formData = new FormData();
        this.formData.append('buId', this.dataStore.selectedBusiness.businessId);
        this.formData.append('productName', ProductDetails["value"].productName);
        this.formData.append("productDesc", ProductDetails["value"].productDesc);
        this.formData.append("barcode", ProductDetails["value"].barcode);
        this.formData.append("availableQuantity", ProductDetails["value"].availableQuantity);
        this.formData.append("image", this.images);
        this.formData.append("price", ProductDetails["value"].price);
        this.formData.append("sGst", ProductDetails["value"].sGst);
        this.formData.append("cGst", ProductDetails["value"].cGst);
        this.formData.append("aditionalPrice", ProductDetails["value"].aditionalPrice);
        this.formData.append("totalPrice", this.totalPrice);
        this.formData.append("unitPrice", ProductDetails["value"].price);
        if (this.productId2 != undefined) {
            this.formData.append("productId", this.productId2);
        }
        if (ProductDetails["value"].categoryName != undefined) {
            this.formData.append('cId', ProductDetails["value"].categoryName);
        }
        if (ProductDetails["value"].subcategoryName != undefined) {
            this.formData.append('scId', ProductDetails["value"].subcategoryName);
        }
        if (ProductDetails["value"].productReword != undefined) {
            this.formData.append('productReword', ProductDetails["value"].productReword);
        }
        for (var i = 0; i < this.imagedata.length; i++) {
            this.formData.append("files", this.imagedata[i]);
        }
        this.productId = ProductDetails["value"].productId;
        this.service.addProduct(this.formData).subscribe(
            response => {
                let serverResponse = response.json();
                this.presentToast(serverResponse.message);
                if (serverResponse.status == 200) {
                    console.log(serverResponse);
                    this.navCtrl.setRoot(ProductList);
                }
                // else {
                //     this.navCtrl.parent.select(0, this.businessId);
                // }
                this.productForm.reset();
            },
            error => {
                this.presentToast("Service Error..");
                console.log(error);
            });
    }
    validateProductName(productName) {
        if (productName != "") {
            this.service.getProductName(productName).subscribe(
                response => {
                    console.log(response);
                    this.nameValidate = (response.text());
                    if (this.nameValidate == "") {
                        this.nameValidate = "";
                    }
                });
        }
    }
    myfunction1(eventValue) {
        console.log("The Additional Price:" + eventValue);
        this.AdditionalAmount = eventValue;
    }
    calculateSellingPrice(event) {
        try {
            this.price2 = this.price2 ? this.price2 : 0;
            this.sGst2 = this.sGst2 ? this.sGst2 : 0;
            this.SGstAmount = (this.price2 * this.sGst2 / 100);
            this.cGst2 = this.cGst2 ? this.cGst2 : 0;
            this.CGstAmount = (this.price2 * this.cGst2 / 100);
            this.aditionalPrice2 = this.aditionalPrice2 ? this.aditionalPrice2 : 0;
            this.totalPrice = ((parseFloat(this.price2) + parseFloat(this.SGstAmount) + parseFloat(this.CGstAmount) + parseFloat(this.aditionalPrice2))).toFixed(2);

        } catch (error) {
            this.totalPrice = 0;
        }
    }
    onChanged1(categoryId) {

        if (categoryId == "") {

            let profileModal = this.modalCtrl.create(AddCategory, { categoryId: this.categoryId });
            profileModal.onDidDismiss(data => {
                console.log(data);
            });
            profileModal.present();
        }
        // else {
        //     // this.subCategoriesList = this.categoriesList.filter
        //     this.selectedCategoryId = selectedOptionName;
        //     this.searchSubcatogory(selectedOptionName, this.categoriesList);
        // }
        this.service.getSubCategory(categoryId).subscribe(
            response => {
                this.subCategoriesList = response.json();
            },
            err => {
                console.log(err);
            }

        )

    }
    searchSubcatogory(nameKey, categoriesList) {
        for (var i = 0; i < categoriesList.length; i++) {
            if (categoriesList[i].categoryId == parseInt(nameKey)) {
                this.subCategoriesList = categoriesList[i].subCategories;
                console.log(this.subCategoriesList);
            }
        }
    }
    getSubCategoriesList(categoryId) {
        this.service.getSubCategory(categoryId).subscribe(
            response => {
                this.subCategoriesList = response.json();
            },
            err => {
                console.log(err);
            }

        )
    }
    getCategoriesList() {
        this.service.getCategory().subscribe(
            response => {
                this.categoriesList = response.json();
            },
            err => {
                console.log(err);
            }

        )
    }
    onChanged2() {
        if (this.subcategoryName2 === 'AddNew') {
            let profileModal = this.modalCtrl.create(AddSubCategory, { selectedCategoryId: this.categoryName2 });
            profileModal.onDidDismiss(data => {
                console.log(data);
            });
            profileModal.present();
        }
    }
    addModeCategory() {
        this.edited = true;
    }
    // addModelSubCategory() {
    //     // this.edited = true;
    //     let profileModal = this.modalCtrl.create(AddSubCategory, { categoryId: this.categoryId });
    //     profileModal.onDidDismiss(data => {
    //         console.log(data);
    //     });
    //     profileModal.present();

    // }
    showProductInfo(productId) {
        this.service.getProductById(productId).subscribe(
            response => {
                var result = response.json();
                console.log("Result data for Product view");
                this.populateProductsData(result);
            });
    }
    populateProductsData(result) {
        this.productName2 = result.productName;
        this.categoryName2 = result.category[0].categoryId;
        this.subcategoryName2 = result.subCategories[0].subcategoryId;
        this.productDesc2 = result.productDesc;
        this.barcode2 = result.barcode;
        this.availableQuantity2 = result.availableQuantity;
        this.images = result.image;
        this.imageUrl = Constants.BASE_URL + result.image;
        this.price2 = result.price;
        this.UnitPrice = result.price;
        this.sGst2 = result.sGst;
        this.cGst2 = result.cGst;
        this.aditionalPrice2 = result.aditionalPrice;
        this.totalPrice = result.unitPrice;
        this.sellingPrice2 = result.sellingPrice;
        this.productReword2 = result.productReword;
        this.productId2 = result.productId;
        this.logoName = result.image;
    }
    ngOnInit() {
        this.findPlatform();

    }
    findPlatform() {
        if (this.plt.is('core')) {
            this.isWeb = true;
            console.log("web platform");
        }
        else {
            this.isWeb = false;
        }
    }
    logoSelect(event) {
        console.log(event);
    }
    onFileUpload(event) {
        this.images = "";
        this.imageUrl = "";
        this.imagedata = event;
        console.log(this.imagedata);
        console.log(event);
        try {
            this.logoSize = Math.floor(event[0].size / 1024);
            this.logoName = event[0].name;
            this.imageUrl = event[0];
        }
        catch { }
        console.log("Image size: " + this.logoSize + " Kb");
    }
    uploadByDocument() {
        this.addByDocument = true;
    }
    onDocUpload(file) {
        // this.fileUrl = "";
        // this.fileUrl = file[0];
        this.file = null;
        this.file = file[0];
        try {
            this.docName = this.file.name;
            let fileReader = new FileReader();
            fileReader.onload = (e) => {
                this.arrayBuffer = fileReader.result;
                var data = new Uint8Array(this.arrayBuffer);
                var arr = new Array();
                for (var i = 0; i != data.length; ++i) {
                    arr[i] = String.fromCharCode(data[i]);
                }
                var bstr = arr.join("");
                var workbook = XLSX.read(bstr, { type: "binary" });
                var first_sheet_name = workbook.SheetNames[0];
                var worksheet = workbook.Sheets[first_sheet_name];
                let docObjArray = XLSX.utils.sheet_to_json(worksheet, { raw: true });
                this.productsFromDoc = docObjArray;
            }
            fileReader.readAsArrayBuffer(this.file);
        }
        catch (error) {
            console.log(error);
        }
    }
    uploadDocument() {
        this.productsFromDoc.map(v => v.buId = this.dataStore.selectedBusiness.businessId);
        for (let i = 0; i < this.productsFromDoc.length; i++) {
            delete this.productsFromDoc[i].__rowNum__;
        }
        this.service.addMultipleProducts(this.productsFromDoc).subscribe(
            res => {
                console.log(res);
                this.navCtrl.setRoot(ProductList);
            },
            err => {
                console.log(err);
            }
        )
    }
    // uploadDocument() {
    //     this.productsFromDoc.map(v => v.businessId = this.dataStore.selectedBusiness.businessId);
    //     console.log("The Product List outside loop :", this.productsFromDoc);
    //     let productNamevalidate = /^[a-zA-Z]+$/;
    //     let quantityValidate = /^((?!(0))[0-9]*[0-9])$/;
    //     let pricevalidate = /^((?!(0))[0-9]*[0-9])$/;
    //     for (let i = 0; i < this.productsFromDoc.length; i++) {
    //         if (this.productsFromDoc[i].productName.length != "" || this.productsFromDoc[i].productName.match(productNamevalidate) || this.productsFromDoc[i].availableQuantity.match(quantityValidate) || this.productsFromDoc[i].price.test(pricevalidate)) {
    //             this.PrdDetail.push({
    //                 'productName': this.productsFromDoc[i].productName,
    //                 'categoryName': this.productsFromDoc[i].categoryName,
    //                 'subcategoryName': this.productsFromDoc[i].subcategoryName,
    //                 'productDesc': this.productsFromDoc[i].productDesc,
    //                 'barcode': this.productsFromDoc[i].barcode,
    //                 'availableQuantity': this.productsFromDoc[i].availableQuantity,
    //                 'image': this.productsFromDoc[i].image,
    //                 'price': this.productsFromDoc[i].price,
    //                 'unitPrice': this.productsFromDoc[i].price,
    //                 'sGst': this.productsFromDoc[i].sGst,
    //                 'cGst': this.productsFromDoc[i].cGst,
    //                 'aditionalPrice': this.productsFromDoc[i].aditionalPrice,
    //                 'productReword': this.productsFromDoc[i].productReword,
    //                 'totalPrice': this.productsFromDoc[i].totalPrice,
    //                 'sellingPrice': this.productsFromDoc[i].totalPrice = (this.productsFromDoc[i].price + (this.productsFromDoc[i].price * this.productsFromDoc[i].sGst / 100) + (this.productsFromDoc[i].price * this.productsFromDoc[i].cGst / 100) + this.productsFromDoc[i].aditionalPrice),
    //                 'businessId': this.dataStore.selectedBusiness.businessId,
    //             })
    //         }
    //     }
    //     this.service.addMultipleProducts(this.PrdDetail).subscribe(
    //         response => {
    //             let serverResponse = response.json();
    //             // this.presentToast(serverResponse.message);
    //             let existingProducts = JSON.parse(serverResponse.message);
    //             for (var k in existingProducts) {
    //                 this.pnameKey.push(k);
    //             }
    //             if (this.pnameKey) {
    //                 let existingProductModal = this.modalCtrl.create(ExistingProduct, { data: this.pnameKey, businessId: this.dataStore.selectedBusiness.businessId });
    //                 existingProductModal.onDidDismiss(data => {
    //                     console.log(data);
    //                     this.navCtrl.setRoot(ProductList, this.dataStore.selectedBusiness.businessId);
    //                 });
    //                 existingProductModal.present();

    //             } else {
    //                 if (serverResponse.status == 200) {
    //                     this.navCtrl.setRoot(ProductList);
    //                 }

    //             }

    //             // else if (serverResponse == 409) {
    //             //     let productName = JSON.parse(serverResponse.message);
    //             //     for (var k in productName) {
    //             //         this.pnameKey.push(k);
    //             //     }
    //             //     console.log(this.pnameKey);

    //             //     let PnameStr: String = "";
    //             //     for (let i = 0; i < this.pnameKey.length; i++) {
    //             //         PnameStr = PnameStr + (this.pnameKey[i]);
    //             //     }
    //             //     let existingProductModal = this.modalCtrl.create(ExistingProduct, { data: this.pnameKey, businessId: this.dataStore.selectedBusiness.businessId });
    //             //     existingProductModal.onDidDismiss(data => {
    //             //         console.log(data);
    //             //         this.navCtrl.setRoot(ProductList, this.dataStore.selectedBusiness.businessId);
    //             //     });
    //             //     existingProductModal.present();
    //             // }
    //         },
    //         error => {
    //             alert(error);
    //         }
    //     );
    // }
    cancelUploadByDocument() {
        this.addByDocument = false;
    }
    showProductList() {
        this.navCtrl.setRoot(ProductList);
    }

}

