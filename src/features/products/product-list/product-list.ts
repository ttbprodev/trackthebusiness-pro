import { Component } from '@angular/core';
import { NavController, ToastController, AlertController, LoadingController, Platform, ViewController, ModalController } from "ionic-angular";
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Service } from '../../../shared/utils/service';
import { AddProduct } from '../add-product/add-product';
import { Constants } from '../../../shared/utils/constants';
import { ProductsCart } from '../products-cart/products-cart';
import { DataStore } from '../../../shared/utils/data-store';

@Component({
  selector: 'product-list',
  templateUrl: 'product-list.html'
})
export class ProductList {
  public isSearchbarisOpened = false;
  productsData = [];
  productId: any;
  isWeb: any;
  imageUrl: any;
  search = [];
  unitPrice: any;
  availableQuantity: any;
  totalPrice: any;
  aditionalPrice: any;
  selectedProductsCount = 2;
  cartProducts = [];
  productSelected = false;
  itemChecked = false;
  IsAllChecked: any;
  IsItemChecked = false;
  showButton = false;
  selectedProductCount = 0;
  currentProductId: any;
  selectedProductsIds = [];


  constructor(
    private view: ViewController,
    private alertCtrl: AlertController,
    public navCtrl: NavController,
    public http: Http,
    public service: Service,
    private toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    public plt: Platform,
    public dataStore: DataStore,
    public modalCtrl: ModalController) {
    this.imageUrl = Constants.BASE_URL;
    this.getProducts();
    this.findPlatform();
  }
  findPlatform() {
    if (this.plt.is('core')) {
      this.isWeb = true;
      console.log("web platform");
    }
    else {
      this.isWeb = false;
    }
  }
  getProducts() {
    this.service.getProduct().subscribe(
      response => {
        this.productsData = response.json();
        if (this.productsData.length == 0) {
          this.navCtrl.setRoot(AddProduct);
        }
      });
  }
  formatProductList() {
    for (let i = 0; i < this.productsData.length; i++) {
      if (this.isWeb)
        this.productsData[i]["showDetails"] = true;
      else
        this.productsData[i]["showDetails"] = false;
    }
  }
  getItems(event) {
    var search = event.target.value;
    console.log(search)
    if (search && search.trim() != "") {
      this.service.getProductByName(search).subscribe(response => {
        this.productsData = response.json()
          .filter(item => item.productName.toLowerCase().startsWith(search.toLowerCase()))
        this.formatProductList();
        console.log(this.productsData);
      })
    }
    else {
      this.getProducts();
    }

  }
  itemSelected(product: any) {
    if (!this.isWeb) // not web do this
      product.showDetails = !product.showDetails;
  }
  presentToast(toastMessage: string) {
    let toast = this.toastCtrl.create({
      message: toastMessage,
      duration: 2000,
      position: 'bottom'
    });
    toast.present();
  }
  AddProduct() {
    let loading = this.loadingCtrl.create({
      spinner: 'bubbles',
      content: 'Please wait...'
    });
    loading.present();
    setTimeout(() => {
      loading.dismiss();
    }, 300);
    this.navCtrl.parent.select(0);
  }
  addNewProduct() {
    this.navCtrl.setRoot(AddProduct);
  }
  editProduct(productId) {
    console.log(productId + " edit clicked..");
    this.service.getProductById(productId).subscribe(
      response => {
        var result = response.json();
        console.log("Result data of Products");
        this.navCtrl.setRoot(AddProduct, result);
      },
      error => {
        console.log(error);
      });
  }
  deleteProduct(productId) {
    let alert = this.alertCtrl.create({
      title: 'Do You Want To Delete this Product',
      buttons: [
        {
          text: 'Yes',
          handler: () => {
            console.log('Yes clicked');
            this.service.deleteProduct(productId).subscribe(
              response => {
                let result = JSON.parse(response.text());
                this.presentToast(result.message);
                if (result != 0) {
                  this.navCtrl.setRoot(ProductList);
                }
                else {
                  this.navCtrl.parent.select(0);
                }

              });
          }
        }
        , {
          text: 'No',
          role: 'No',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    alert.present();
    this.navCtrl.setRoot(ProductList);
  }
  CheckUncheckHeader(productSelected, productId) {
    this.IsAllChecked = true;
    if (this.IsAllChecked == true) {
      this.currentProductId = productId;
    }
    else {

    }
    if (productSelected == true) {
      this.selectedProductCount = this.selectedProductCount + 1;
      console.log("selectedProductCount if one selected: " + this.selectedProductCount);
      // this.currentBranchId = this.branchesList.branchId;

    } else if (productSelected == false) {
      this.selectedProductCount = this.selectedProductCount - 1;
      console.log("selectedProductCount else selected: " + this.selectedProductCount);
    }
    for (var i = 0; i < this.productsData.length; i++) {
      if (!this.productsData[i].selected) {
        this.IsAllChecked = false;
        this.IsItemChecked = true;
        break;
      }
      else if (this.productsData[i].selected == productId) {
        this.currentProductId = this.productsData[i].productId;
      }
    }
  }
  CheckUncheckAll(IsAllChecked) {
    this.selectedProductCount = 0;
    for (let i = 0; i < this.productsData.length; i++) {
      this.productsData[i].selected = this.IsAllChecked;

      if (IsAllChecked == true) {
        this.selectedProductCount = this.selectedProductCount + 1;
        console.log("selectedProductCount all selected: " + this.selectedProductCount);

      } else if (IsAllChecked == false) {
        if (this.selectedProductCount >= 0) {
          console.log("selectedProductCount none if selected: " + this.selectedProductCount);
        }
        else {
          this.selectedProductCount = this.selectedProductCount - 1;
          console.log("selectedProductCount none else selected: " + this.selectedProductCount);
        }
      }
    }
  }

  deleteMultipleProduct() {
    // this.selectedBranchesIds = [];
    let ProductsToDelete;
    for (var i = 0; i < this.productsData.length; i++) {
      if (this.productsData[i].selected) {
        this.selectedProductsIds.push(this.productsData[i].productId);
        //console.log( JSON.stringify(this.selectedSchoolIds));
      }
    }
    ProductsToDelete = (this.selectedProductsIds);
    this.service.deleteMultipleProducts(ProductsToDelete).subscribe(
      res => {
        if (res.status == 200) {
          alert("Product deleted Successfully!");
          this.navCtrl.setRoot(ProductList);
        }
      },
      error => {
        console.log(error);
      }
    );
  }
  presentCartModal() {
    if (this.cartProducts.length == 0) {
      alert("Your cart is Empty.");
    } else {
      let profileModal = this.modalCtrl.create(ProductsCart, { data: this.cartProducts });
      profileModal.onDidDismiss(data => {
        console.log(data);
      });
      profileModal.present();
    }
  }

  addProductsToCart(event) {
    if (event.itemChecked == true) {
      let count = 0;
      if (this.cartProducts.filter((item) => item.productId === event.productId)) {
        // this.cartProducts.map = count++;
      }
      this.cartProducts.push({ productName: event.productName, productId: event.productId, sellingPrice: event.sellingPrice });
      console.log(this.cartProducts);
      console.log(this.cartProducts.length);

    } else if (event.itemChecked == false) {
      this.cartProducts.splice(this.cartProducts.indexOf(event.productName), 1);
      console.log(this.cartProducts);
      console.log(this.cartProducts.length);
    }
  }
  selectProduct() {
    this.productSelected = !this.productSelected;
  }
}
