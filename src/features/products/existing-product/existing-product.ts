
import { Component } from "@angular/core";
import { NavParams, ViewController, NavController, Platform } from "ionic-angular";
import { AddProduct } from "../add-product/add-product";
import { DataStore } from "../../../shared/utils/data-store";


@Component({
    selector: 'existing-product',
    templateUrl: './existing-product.html',
})
export class ExistingProduct {
    isWeb: any;
    productList: any;
    businessId: any;

    constructor(
        public navParams: NavParams,
        public viewCtrl: ViewController,
        public navCtrl: NavController,
        public plt: Platform,
        public dataStore:DataStore
    ) {
        this.productList = this.navParams.get('data');
        this.dataStore.selectedBusiness.businessId = this.navParams.get('businessId');
        this.findPlatform();
    }
    findPlatform() {
        if (this.plt.is('core')) {
          this.isWeb = true;
          console.log("web platform");
        }
        else {
          this.isWeb = false;
        }
      }
      dismiss() {
        this.viewCtrl.dismiss(AddProduct, this.dataStore.selectedBusiness.businessId);
      }
}