import { Component } from "@angular/core";
import { NavParams, ViewController, NavController, Platform } from "ionic-angular";
import { AddCustomer } from "../add-customer/add-customer";

@Component({
    selector: 'existing-customers',
    templateUrl: './existing-customers.html',
})
export class ExistingCutomers {
    isWeb: any;
    cutomerDataList: any;
    cutomerNameData: any;
    businessId: any;

    constructor(
        public navParams: NavParams,
        public viewCtrl: ViewController,
        public navCtrl: NavController,
        public plt: Platform
    ) {
        this.cutomerNameData = this.navParams.get('data');
        this.cutomerDataList = this.navParams.get('name');
        this.businessId = this.navParams.get('businessId');
        this.findPlatform();
    }
    findPlatform() {
        if (this.plt.is('core')) {
          this.isWeb = true;
          console.log("web platform");
        }
        else {
          this.isWeb = false;
        }
      }
    dismiss() {
        this.viewCtrl.dismiss(AddCustomer, this.businessId);
      }
}