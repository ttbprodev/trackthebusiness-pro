import { Component } from "@angular/core";
import { AlertController, NavController, ToastController, LoadingController, ModalController, ViewController } from "ionic-angular";
import { Service } from "../../../shared/utils/service";
import { AddCustomer } from "../add-customer/add-customer";

@Component({
    selector: 'customer-list',
    templateUrl: './customer-list.html'
})
export class CustomerList {
    public isSearchbarisOpened = false;
    customerData = [];
    customerId: any;
    businessId = 1;
    isWeb: any;
    listCustomer = [];
    IsAllChecked: any;
    IsItemChecked = false;
    showButton = false;
    selectedCustomerCount = 0;
    currentCustomerId: any;
    selectedCustomersIds = [];
    constructor(
        private alertCtrl: AlertController,
        public navCtrl: NavController,
        public service: Service,
        private toastCtrl: ToastController,
        public loadingCtrl: LoadingController,
        public modalCtrl: ModalController,
        public viewCtrl: ViewController) {

        this.getCustomers();
    }
    getCustomers() {
        this.service.getCustomer().subscribe(
            response => {
                this.customerData = response.json();
                this.listCustomer = this.customerData;
                if (this.customerData.length == 0) {
                    this.navCtrl.setRoot(AddCustomer);
                } else {
                    for (let i = 0; i < this.customerData.length; i++) {
                        if (this.isWeb)
                            this.customerData[i]["showDetails"] = true;
                        else
                            this.customerData[i]["showDetails"] = false;
                    }
                    console.log(this.customerData);
                }
            });
    }

    CheckUncheckHeader(selectedCustomer, customerId) {
        this.IsAllChecked = true;
        if (this.IsAllChecked == true) {
            this.currentCustomerId = customerId;
        }
        else {

        }
        if (selectedCustomer == true) {

            this.selectedCustomerCount = this.selectedCustomerCount + 1;
            console.log("selectedBranchesCount if one selected: " + this.selectedCustomerCount);
        } else if (selectedCustomer == false) {
            this.selectedCustomerCount = this.selectedCustomerCount - 1;
            console.log("selectedBranchesCount: " + this.selectedCustomerCount);
        }
        for (var i = 0; i < this.customerData.length; i++) {
            if (!this.customerData[i].selected) {
                this.IsAllChecked = false;
                this.IsItemChecked = true;
                break;
            }
            else if (this.customerData[i].selected == customerId) {
                this.currentCustomerId = this.customerData[i].custId;
            }
        }
    }
    CheckUncheckAll(IsAllChecked) {
        this.selectedCustomerCount = 0;
        for (var i = 0; i < this.customerData.length; i++) {
            this.customerData[i].selected = this.IsAllChecked;

            if (IsAllChecked == true) {
                this.selectedCustomerCount = this.selectedCustomerCount + 1;
                console.log("selectedCustomerCount: all selected: " + this.selectedCustomerCount);

            } else if (IsAllChecked == false) {
                if (this.selectedCustomerCount >= 0) {
                    console.log("selectedCustomerCount none if selected: " + this.selectedCustomerCount);
                }
                else {
                    this.selectedCustomerCount = this.selectedCustomerCount - 1;
                    console.log("selectedCustomerCount none else selected: " + this.selectedCustomerCount);
                }
            }
        }
    }
    AddCustomer() {
        this.navCtrl.setRoot(AddCustomer);
    }
    editSingleCustomer() {
        this.editCustomer(this.currentCustomerId);
    }
    editCustomer(custId) {
        console.log(custId + " edit clicked..");
        this.service.getCustomerById(custId).subscribe(
            response => {
                var result = response.json();
                console.log("Result data of Customer");
                this.navCtrl.setRoot(AddCustomer, result);
            },
            error => {
                console.log(error);
            });
    }
    deleteCustomerInfo(customerId) {
        let alert = this.alertCtrl.create({
            title: 'Confirm delete',
            message: 'Do you want to Delete this Customer?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: () => {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Delete',
                    handler: () => {
                        console.log(customerId + ", " + " delete clicked.. ");
                        this.service.deleteCustomer(customerId).subscribe(
                            response => {
                                var result = response.json();
                                console.log("Customer deleted Successfully..");
                                this.navCtrl.setRoot(CustomerList);

                            },
                            error => {
                                console.log(error);
                            });
                    }
                }
            ]
        });
        alert.present();
        this.navCtrl.setRoot(CustomerList);
    }
    deleteMultipleCustomer() {
        let cutsomersToDelete;
        for (var i = 0; i < this.customerData.length; i++) {
            if (this.customerData[i].selected) {
                this.selectedCustomersIds.push(this.customerData[i].custId);
                //console.log( JSON.stringify(this.selectedSchoolIds));
            }
        }
        cutsomersToDelete = (this.selectedCustomersIds);

        this.service.deleteMultipleCustomers(cutsomersToDelete).subscribe(
            res => {
                if (res.status == 200) {
                    alert("Customer deleted Successfully!");
                    this.navCtrl.setRoot(CustomerList);
                }
            },
            error => {
                console.log(error);
            }
        );
    }
    getItems(event) {
        var searchString = event.target.value;
        console.log(searchString);
        if (searchString && searchString.trim() != "") {
            this.listCustomer = this.customerData.filter(customer => {
                return (customer.customerName.toLowerCase().startsWith(searchString.toLowerCase()))
            })
        }
        else {
            this.listCustomer = this.customerData;
        }
    }

    itemSelected(customer: any) {
        if (!this.isWeb) // not web do this
            customer.showDetails = !customer.showDetails;
    }
    presentToast(toastMessage: string) {
        let toast = this.toastCtrl.create({
            message: toastMessage,
            duration: 2000,
            position: 'bottom'
        });
        toast.present();
    }
}