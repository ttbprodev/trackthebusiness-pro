import { Component, ViewChild } from "@angular/core";
import { ViewController, NavParams, LoadingController, Nav, NavController, ToastController, AlertController, Platform } from "ionic-angular";
import { Service } from '../../../shared/utils/service';
import { Http } from '@angular/http';
import { AddCustomer } from "../add-customer/add-customer";
import { CustomerList } from "../customer-list/customer-list";
import { FormGroup, Validators, FormBuilder } from "@angular/forms";
import { DataStore } from "../../../shared/utils/data-store";


@Component({
    selector: 'customer-model',
    templateUrl: 'customer-model.html',
})
export class CustomerModel {
    @ViewChild(Nav) nav: Nav;
    isWeb: any;
    formData: any;
    editCustomer = false;
    BusinessId: any;
    CustomerName: any;
    CustomerMobile: any;
    CustomerEmailAddress: any;
    CustomerPhoneHome: any;
    customerPhoneOffice: any;
    CustomerAddress: any;
    customerId: any;
    CustomerId: any;
    customerData: any;
    CustomerId1: any;
    CustomerName1: any;
    CustomerPhoneHome1: any;
    CustomerPhoneOffice: any;
    CustomerAddress1: any;
    onClickEdit: true;
    filterList: any;
    private customerForm: FormGroup;
    constructor(private view: ViewController,
        public http: Http,
        public service: Service,
        private navParams: NavParams,
        private loadingCtrl: LoadingController,
        private navCtrl: NavController,
        private toastCtrl: ToastController,
        private alertCtrl: AlertController,
        private formBuilder: FormBuilder,
        public dataStore: DataStore,
        public plt: Platform, ) {
        {
            this.customerId = navParams.get('id');
            this.customerData = navParams.get('customerData');
            if (this.customerId != undefined) {
                this.showCustomerInfo(this.customerId, this.customerData);

            }
        }
    }

    ngOnInit() {
        this.findPlatform();
    }
    findPlatform() {
        if (this.plt.is('core')) {
            this.isWeb = true;
            console.log("web platform");
        }
        else {
            this.isWeb = false;
        }
    }
    presentToast(toastMessage: string) {
        let toast = this.toastCtrl.create({
            message: toastMessage,
            duration: 2000,
            position: 'bottom'
        });
        toast.present();
    }
    deleteCustomerInfo() {
        let alert = this.alertCtrl.create({
            title: 'Do You Want To Delete this Customer ?',
            buttons: [{
                text: 'Yes',
                handler: () => {
                    console.log('Yes clicked');
                    this.service.deleteCustomer(this.customerId).subscribe(
                        response => {
                            if (response.status == 200) {
                                let result = JSON.parse(response.text());
                                this.presentToast(result.message);
                                // this.navCtrl.setRoot(CustomerList);
                                this.close();
                            }
                        },
                        error => {  
                            console.log(error);
                        });
                }
            },
            {
                text: 'No',
                role: 'No',
                handler: () => {
                    console.log('Cancel clicked');
                }
            }]
        });
        alert.present();
        // this.navCtrl.push(CustomerList);
    }

    showCustomerInfo(customerId, customerData) {
        for (var i = 0; i < customerData.length; i++) {
            if (customerId == customerData[i].custId) {
                let result = customerData[i];
                this.CustomerId = result.custId;
                this.CustomerName = result.customerName;
                this.CustomerMobile = result.customerMobile;
                this.CustomerEmailAddress = result.customerEmailAddress;
                this.CustomerPhoneHome = result.customerPhoneHome;
                this.CustomerPhoneOffice = result.customerPhoneOffice;
                this.CustomerAddress = result.customerAddress;
            }
        }
    }
    editCustomerInfo() {
        this.editCustomer = true;
    }
    customerDetails(customerData) {
        console.log(customerData.value);
        this.formData = new FormData();
        this.formData.append('businessId', this.dataStore.selectedBusiness.businessId);
        this.formData.append('custId', this.customerId);
        this.formData.append('customerName', customerData.value.customerName);
        this.formData.append('customerMobile', customerData.value.customerMobile);
        this.formData.append('customerEmailAddress', customerData.value.customerEmailAddress);
        this.formData.append('customerPhoneHome', customerData.value.customerPhoneHome);
        this.formData.append('customerPhoneOffice', customerData.value.customerPhoneOffice);
        this.formData.append('customerAddress', customerData.value.customerAddress);
        this.service.addNewCustomer(this.formData).subscribe(
            response => {
                let serverResponse = response.json();
                this.presentToast(serverResponse.message);
                if (serverResponse.status == 200) {
                    this.close();
                }
            },
            error => {
                this.presentToast("Service Error");
                console.log(error);
            });

    }

    close() {
        this.view.dismiss("Close");
    }
}









