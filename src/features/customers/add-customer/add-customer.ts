
import { Component, OnInit } from "@angular/core";
import { Validators, FormBuilder, FormGroup, FormArray } from "@angular/forms";
import { Contacts, Contact, ContactField, ContactName, ContactFieldType, IContactFindOptions, ContactFindOptions } from '@ionic-native/contacts';
import { NavParams, NavController, Platform, ToastController, LoadingController, AlertController, ModalController } from "ionic-angular";
import { Service } from "../../../shared/utils/service";
import { DataStore } from "../../../shared/utils/data-store";
import { CustomerList } from "../customer-list/customer-list";
import { AutoCompleteComponent } from 'ionic2-auto-complete';
import * as XLSX from 'xlsx';
import { CompleteTestService } from '../../../shared/utils/complete-test-service/complete-test-service';
import { ExistingCutomers } from "../existing-customers/existing-customers";


declare const cordova: any;
@Component({
    selector: 'add-customer',
    templateUrl: './add-customer.html'
})
export class AddCustomer implements OnInit {
    isListOpen = false;
    ourtype: ContactFieldType[] = ["displayName"];
    contactsFound = [];
    CustomerInfo: any = [];
    formData: any;
    customerId: any;
    custId:any;
    isWeb: any;
    CustomerName: any;
    CustomerType: any;
    CustomerMobile: any;
    CustomerPhoneHome: any;
    CustomerPhoneOffice: any;
    CustomerEmailAddress: any;
    CustomerAddress: any;
    BusinessId: any;
    businessId: any;
    autocomplete: AutoCompleteComponent;
    contactName = [];
    searchString: any;
    filteredContact: any;
    public form: FormGroup;
    addByDocument = false;
    docName = "";
    customerFromDoc = [];
    mobileKeys = [];
    numberNamedata = [];
    customerNameKeys = [];
    fileUrl: any;
    fileType: any;
    arrayBuffer: any;
    cutomerdataList: any;
    cutomerNameData: any;
    // cutomerMobileData:any;
    CustDetails = [];
    customerCounts:any;
    file: File;
    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public formBuilder: FormBuilder,
        public completeTestService: CompleteTestService,
        public plt: Platform,
        private toastCtrl: ToastController,
        private service: Service,
        private alertCtrl: AlertController,
        public loadingCtrl: LoadingController,
        public dataStore: DataStore,
        private contacts: Contacts,
        public modalCtrl: ModalController,


    ) {

        this.searchString = this.searchString;
        this.plt.ready().then((readySource) => {
            console.log('Platform ready from', readySource);
        });
        this.customerListcounts();
        let customerDetails = this.navParams.data;
        this.customerId = customerDetails.custId;
        if (this.customerId != undefined) {
            this.showCustomerInfo();
        }
    }
    customerListcounts()
    {
        this.service.getCustomer().subscribe(
            response => {
                console.log(response.status == 200);
                // this.branchesList = response.json();
                if (response.json().length == 0) {
                    this.customerCounts = response.json().length;
                    console.log(response.json().length);
                }
                else {
                    this.customerCounts = response.json().length;
                    console.log(this.customerCounts);
                }
            },
            error => {
                console.log(error);
            }
        );
    }
    search(q) {
        if (cordova) {
            const option: IContactFindOptions = {
                filter: q
            }
            this.contacts.find(this.ourtype, option).then(conts => {
                this.contactsFound = conts;
            })
        }
    }
    async findContacts(event) {
        try {
            var searchString = event.target.value;
            const option = new ContactFindOptions();
            option.filter = searchString;
            option.hasPhoneNumber = true;
            const fields: ContactFieldType[] = ['name'];
            this.filteredContact = await this.contacts.find(fields, option);
            console.log('filtered contact:', this.filteredContact);

        }
        catch (e) {
            console.error(e);
        }
    }

    customersFields(): FormGroup {
        return this.formBuilder.group({
            businessId: [null],
            custId: [null],
            customerName: ['', Validators.required, Validators.minLength(3), Validators.maxLength(50)],
            customerMobile: ['', Validators.required, Validators.minLength(10), Validators.maxLength(13)],
            customerPhoneHome: ['', Validators.required, Validators.minLength(8), Validators.maxLength(10)],
            customerPhoneOffice: ['', Validators.required, Validators.minLength(8), Validators.maxLength(10)],
            customerEmailAddress: ['', Validators.required, Validators.pattern('[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}')],
            customerAddress: ['', Validators.required, Validators.minLength(0), Validators.maxLength(50)],
        });
    }
    contactSelected(i) {
        this.isListOpen = true;
        this.CustomerName = this.filteredContact[i].name.formatted;
        this.CustomerMobile = this.filteredContact[i].phoneNumbers[0].value;
        console.log(this.CustomerName);
        console.log(this.CustomerMobile);

    }
    ngOnInit() {
        this.findPlatform();
    }
    findPlatform() {
        if (this.plt.is('core')) {
            this.isWeb = true;
            console.log("web platform");
        }
        else {
            this.isWeb = false;
        }
    }
    showLoader() {
        let loading = this.loadingCtrl.create({
            spinner: 'bubbles',
            content: 'Please wait...'
        });
        loading.present();
        setTimeout(() => {
            loading.dismiss();
        }, 1000);
    }
    presentToast(toastMessage: string) {
        let toast = this.toastCtrl.create({
            message: toastMessage,
            duration: 3000,
            position: 'bottom'
        });
        toast.present();
    }
    addCustomer(customerInfo) {
        this.formData = new FormData();
        this.formData.append('buId', this.dataStore.selectedBusiness.businessId);
        this.formData.append('customerName', customerInfo["value"].customerName);
        this.formData.append('customerType', customerInfo["value"].customerType);
        this.formData.append('customerMobile', customerInfo["value"].customerMobile);
        // this.formData.append("customerEmailAddress", customerInfo["value"].customerEmailAddress);
        // this.formData.append("customerPhoneHome", customerInfo["value"].customerPhoneHome);
        // this.formData.append("customerPhoneOffice", customerInfo["value"].customerPhoneOffice);
        // this.formData.append("customerAddress", customerInfo["value"].customerAddress);
        this.formData.append('customerType', customerInfo["value"].customerType);
        if(this.custId != undefined)
        {
            this.formData.append("custId",this.custId)
        }
        if(customerInfo["value"].customerEmailAddress != undefined)
        {
            this.formData.append('customerEmailAddress',customerInfo["value"].customerEmailAddress)
        }
        if(customerInfo["value"].customerPhoneHome != undefined)
        {
            this.formData.append('customerPhoneHome',customerInfo["value"].customerPhoneHome)
        }
        if(customerInfo["value"].customerPhoneOffice != undefined)
        {
            this.formData.append('customerPhoneOffice',customerInfo["value"].customerPhoneOffice)
        }
        if(customerInfo["value"].customerAddress != undefined)
        {
            this.formData.append('customerAddress',customerInfo["value"].customerAddress)
        }
        this.service.addNewCustomer(this.formData).subscribe(
            response => {
                let serverResponse = response.json();
                this.presentToast(serverResponse.message);
                if (response.status == 200) {
                    console.log(serverResponse);
                    this.navCtrl.setRoot(CustomerList, this.businessId);
                }
                else {
                    this.navCtrl.setRoot(AddCustomer, this.businessId);
                }
            },
            error => {
                this.presentToast("Service Error");
                console.log(error);
            });
    }
    showCustomerInfo() {
        this.service.getCustomerById(this.customerId).subscribe(
            response => {
                var result = response.json();
                console.log("Result data for Customer view");
                this.populateCustomerData(result);
            });
    }
    populateCustomerData(result) {
        this.custId=result.custId;
        this.CustomerName = result.customerName;
        this.CustomerMobile = result.customerMobile;
        this.CustomerEmailAddress = result.customerEmailAddress;
        this.CustomerPhoneHome = result.customerPhoneHome;
        this.CustomerPhoneOffice = result.customerPhoneOffice;
        this.CustomerAddress = result.customerAddress;
        this.CustomerType = result.customerType;
    }
    uploadByDocument() {
        this.addByDocument = true;
    }
    onDocUpload(file) {
        this.fileUrl = "";
        this.fileUrl = file[0];

        this.file = file[0];
        try {
            this.docName = this.fileUrl.name;
            let fileReader = new FileReader();
            fileReader.onload = (e) => {
                this.arrayBuffer = fileReader.result;
                var data = new Uint8Array(this.arrayBuffer);
                var arr = new Array();
                for (var i = 0; i != data.length; ++i) {
                    arr[i] = String.fromCharCode(data[i]);
                }
                var bstr = arr.join("");
                var workbook = XLSX.read(bstr, { type: "binary" });
                var first_sheet_name = workbook.SheetNames[0];
                var worksheet = workbook.Sheets[first_sheet_name];
                let docObjArray = XLSX.utils.sheet_to_json(worksheet, { raw: true });
                this.customerFromDoc = docObjArray;
            }
            fileReader.readAsArrayBuffer(this.file);
        }
        catch (error) {
            console.log(error);
        }
    }
    uploadDocument() {
        this.customerFromDoc.map(v => v.buId = this.dataStore.selectedBusiness.businessId);
        for (let i = 0; i < this.customerFromDoc.length; i++) {
            delete this.customerFromDoc[i].__rowNum__;
        }
        this.service.addMultipleCustomers(this.customerFromDoc).subscribe(
            res => {
                console.log(res);
                this.navCtrl.setRoot(CustomerList);
            },
            err => {
                console.log(err);
            }
        )
    }
    cancelUploadByDocument() {
        this.addByDocument = false;
    }
    // uploadDocument() {
    //     this.customerFromDoc.map(v => v.businessId = this.dataStore.selectedBusiness.businessId);
    //     console.log("The customers list outside loop :", this.customerFromDoc);
    //     let CustomerMobilePattern = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
    //     let CustomerEmailPattern = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    //     let phoneNovalidate = /^\d[0-9]{10}$/;
    //     let customerNamevalidate = /^[0-9a-zA-Z]+$/;

    //     for (let i = 0; i < this.customerFromDoc.length; i++) {
    //         if (this.customerFromDoc[i].customerName.length != 0 || this.customerFromDoc[i].customerName.match(customerNamevalidate) || this.customerFromDoc[i].customerMobile.match(CustomerMobilePattern) || this.customerFromDoc[i].customerEmailAddress.test(CustomerEmailPattern) || this.customerFromDoc[i].customerPhoneHome.match(phoneNovalidate) || this.customerFromDoc[i].customerAddress.length != 0) {
    //             this.CustDetails.push({
    //                 'customerName': this.customerFromDoc[i].customerName,
    //                 'customerMobile': this.customerFromDoc[i].customerMobile,
    //                 'customerPhoneHome': this.customerFromDoc[i].customerPhoneHome,
    //                 'customerPhoneOffice': this.customerFromDoc[i].customerPhoneOffice,
    //                 'customerEmailAddress': this.customerFromDoc[i].customerEmailAddress,
    //                 'customerAddress': this.customerFromDoc[i].customerAddress,
    //                 'customerType':this.customerFromDoc[i].customerType,
    //             });
    //         }

    //     }
    //     this.service.addMultipleCustomers(this.CustDetails).subscribe(
    //         response => {
    //             let serverResponse = response.json();
    //             this.presentToast(serverResponse.Message);
    //             if (serverResponse.status == 200) {
    //                 this.navCtrl.setRoot(CustomerList);
    //             }
    //             else {
    //                 let custNumbers = JSON.parse(serverResponse.message);
    //                 // let customerNoObj = response.json();
    //                 for (var key in custNumbers) {
    //                     this.mobileKeys.push(key);
    //                     this.customerNameKeys.push(custNumbers[key]);
    //                     // let custNumbers[key];
    //                 }
    //                 console.log(this.mobileKeys);
    //                 console.log(this.customerNameKeys);
    //                 let mobileStr: string = "";
    //                 let customerNameStr:string="";

    //                 for (let i = 0; i < this.numberNamedata.length; i++) {
    //                     mobileStr = mobileStr + (this.mobileKeys[i]);
    //                     customerNameStr = customerNameStr + (this.customerNameKeys[i]);
    //                 }
    //                 let existingCustomerModal = this.modalCtrl.create(ExistingCutomers, { data: this.mobileKeys, name:this.customerNameKeys, businessId: this.businessId });
    //                 existingCustomerModal.onDidDismiss(data => {
    //                     console.log(data);
    //                     this.navCtrl.setRoot(CustomerList, this.businessId);
    //                 });
    //                 existingCustomerModal.present();
    //             }
    //         },
    //         error => {

    //             alert(error);

    //         }
    //     );
    // }
    // onclickCancel() {
    //     this.navCtrl.setRoot(CustomerList);
    // }
    showCutomersList() {
        this.navCtrl.setRoot(CustomerList);
    }

}
