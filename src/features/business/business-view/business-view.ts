import { Component, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, NgForm } from "@angular/forms";
import { NavParams, NavController, Platform, ToastController, AlertController, LoadingController } from "ionic-angular";
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Service } from "../../../shared/utils/service";
import { DataStore } from "../../../shared/utils/data-store";
import { Constants } from "../../../shared/utils/constants";
import { DashboardView } from "../../dashboard-view/dashboard-view";
import { Observable } from "rxjs";

@Component({
    selector: 'business-view',
    templateUrl: './business-view.html',
})
export class BusinessView implements OnInit {
    @ViewChild('f') businessForm: NgForm;

    businessInfo: any = [];
    imageData: any[] = [];
    businessListData: any;
    formData: any;
    userId: any;
    businessId: any;
    userData: any;
    isWeb: any;
    images: any;
    imageUrl: any;
    logoName: any;
    logoSize: number = 0;
    logoType: string;
    BusinessName: any;
    Phone: any;
    Email: any;
    Gst: any;
    City: any;
    Pincode: any;
    base64Image: string;
    loading = this.loadingCtrl.create({
        spinner: 'bubbles',
        content: 'Please wait...'
    });

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public formBuilder: FormBuilder,
        public plt: Platform,
        private toastCtrl: ToastController,
        private service: Service,
        private alertCtrl: AlertController,
        public loadingCtrl: LoadingController,
        public dataStore: DataStore,
        private camera: Camera
    ) {
        this.userData = this.dataStore.loginMetaData;
        this.Email = this.userData.email;
        this.userId = this.userData.userId;
        this.businessId = navParams.get('businessId');
        if (this.businessId != undefined) {
            this.showBusinessInfo(this.businessId);
        }
    }

    showLoader() {
        let loading = this.loadingCtrl.create({
            spinner: 'bubbles',
            content: 'Please wait...'
        });
        loading.present();
        setTimeout(() => {
            loading.dismiss();
        }, 1000);
    }

    ngOnInit() {
        this.findPlatform();
    }

    findPlatform() {
        if (this.plt.is('core')) {
            this.isWeb = true;
            console.log("web platform");
        }
        else {
            this.isWeb = false;
        }
    }

    presentToast(toastMessage: string) {
        let toast = this.toastCtrl.create({
            message: toastMessage,
            duration: 3000,
            position: 'bottom'
        });
        toast.present();
    }

    addBusiness(businessInfo) {
        this.formData = new FormData();
        this.formData.append('uId', this.userId);
        this.formData.append('businessName', businessInfo["value"].businessName);
        this.formData.append("phone", businessInfo["value"].phone);
        this.formData.append("email", businessInfo["value"].email);
        this.formData.append("gst", businessInfo["value"].gst);
        this.formData.append("city", businessInfo["value"].city);
        this.formData.append("pincode", businessInfo["value"].pincode);
        this.formData.append("image", this.images);
        if (this.businessId != undefined) {
            this.formData.append("businessId", this.businessId);
        }
        if (this.base64Image == undefined) {
            for (var i = 0; i < this.imageData.length; i++) {
                this.formData.append("files", this.imageUrl);
            }
        } else {
            this.uploadBase64(this.base64Image, this.logoName);
        }

        // this.loading.present();
        this.service.addNewBusiness(this.formData).subscribe(
            response => {
                let serverResponse = response.json();
                this.presentToast(serverResponse.message);
                if (serverResponse.status == 200) {
                    this.navCtrl.setRoot(DashboardView);
                }
            },
            error => {
                this.presentToast("Service Error");
                console.log(error);
            });
        // this.loading.dismiss();
    }

    onFileUpload(event) {
        this.images = "";
        this.imageUrl = "";
        this.imageData = event;
        console.log(this.imageData);
        console.log(event);
        try {
            this.logoSize = Math.floor(event[0].size / 1024);
            this.logoName = event[0].name;
            this.imageUrl = event[0];
        }
        catch (e) {
            console.log(e)
        }
        console.log("Image size: " + this.logoSize + " Kb");
    }

    showBusinessInfo(bid) {
        // this.loading.present();
        this.service.getBusiness(bid).subscribe(
            response => {
                var result = response.json();
                console.log("Result data for business view");
                this.populateBusinessData(result);
            });
        // this.loading.dismiss();
    }

    populateBusinessData(result) {
        this.BusinessName = result.businessName;
        this.Phone = result.phone;
        this.Gst = result.gst;
        this.City = result.city;
        this.Pincode = result.pincode;
        this.businessId = result.businessId;
        this.logoName = result.image;
        this.images = result.image;
        this.imageUrl = Constants.BASE_URL + result.image;
    }

    deletingBusiness() {
        let alert = this.alertCtrl.create({
            title: 'Confirm delete',
            message: 'Do you want to Delete Business?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: () => {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Delete',
                    handler: () => {
                        // this.loading.present();
                        this.service.deleteBusiness(this.businessId).subscribe(
                            reponse => {
                                console.log("Deleted successfully");
                                this.navCtrl.setRoot(DashboardView);
                                this.presentToast(reponse.json().message);
                            },
                            error => {
                                this.presentToast("Service Error");
                                console.log(error);
                            }
                        );
                        // this.loading.dismiss();
                    }
                }
            ]
        });
        alert.present();
    }

    /* =========== Camera Integration ========== */

    takePictureFromCamera() {
        const options: CameraOptions = {
            quality: 100,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            allowEdit: true,
            correctOrientation: true,
            saveToPhotoAlbum: true,
            targetWidth: 720,
            targetHeight: 720
        }
        this.camera.getPicture(options).then((imageData) => {
            this.base64Image = 'data:image/jpeg;base64,' + imageData;
        }, (err) => {
            console.log(err);
        });
        let d = new Date();
        this.logoName = d.getTime() + ".jpg";
        this.images = "";
    }

    takePictureFromGallery() {
        const options: CameraOptions = {
            quality: 100,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            allowEdit: true,
            correctOrientation: true,
            saveToPhotoAlbum: true,
            targetWidth: 720,
            targetHeight: 720
        }

        this.camera.getPicture(options).then((imageData) => {
            this.base64Image = 'data:image/jpeg;base64,' + imageData;
        }, (err) => {
            console.log(err);
        });
        let d = new Date();
        this.logoName = d.getTime() + ".jpg";
        this.images = "";
    }

    uploadBase64(base64: string, filename: string) {
        const blob = this.convertBase64ToBlob(base64);
        this.formData.append("files", blob, filename);
    }
    private convertBase64ToBlob(base64: string) {
        const info = this.getInfoFromBase64(base64);
        const sliceSize = 512;
        const byteCharacters = window.atob(info.rawBase64);
        const byteArrays = [];

        for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            const slice = byteCharacters.slice(offset, offset + sliceSize);
            const byteNumbers = new Array(slice.length);

            for (let i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            byteArrays.push(new Uint8Array(byteNumbers));
        }

        return new Blob(byteArrays, { type: info.mime });
    }

    private getInfoFromBase64(base64: string) {
        const meta = base64.split(',')[0];
        const rawBase64 = base64.split(',')[1].replace(/\s/g, '');
        const mime = /:([^;]+);/.exec(meta)[1];
        const extension = /\/([^;]+);/.exec(meta)[1];

        return {
            mime,
            extension,
            meta,
            rawBase64
        };
    }

}