import { Component, ViewChild, OnInit } from "@angular/core";
import { DataStore } from "../../../shared/utils/data-store";
import { Service } from "../../../shared/utils/service";
import { NavController, ModalController, NavParams, Platform, ToastController, AlertController, LoadingController } from "ionic-angular";
import { NewSales } from "../../pos/new-pos/new-pos";
import { NewOrder } from "../../orders/new-order/new-order";
import { Http } from "@angular/http";
import { FilterModel } from "../../orders/filter-model/filter-model";


@Component({
    selector: 'invoice-list',
    templateUrl: './invoice-list.html'
})

export class InvoiceLIst {

    invoiceList = [];
    isWeb: any = false;
    selectedBranchId: any;
    constructor(public navCtrl: NavController,
        public platform: Platform,
        public http: Http,
        public service: Service,
        public dataStore: DataStore,
        public modalCtrl: ModalController,
        public loadingCtrl: LoadingController,
        public navParams: NavParams) {
        this.invoiceSummaryList();

    }

    ngOnInit() {
        this.findPlatform();
    }


    findPlatform() {
        if (this.platform.is('core')) {
            this.isWeb = true;
        }
        else {
            this.isWeb = false;
        }
    }

    invoiceSummaryList() {
           this.selectedBranchId= this.dataStore.selectedBranch.branchId;
        this.service.getInvoiceList(this.selectedBranchId).subscribe(
            response => {
                console.log(response);
                this.invoiceList = response.json();
                if (this.invoiceList.length == 0) {
                    // this.navCtrl.setRoot(NewSales);
                }
            },
            error => {
                console.log(error);
            }
        )
    }
    AddNewOrder() {
        this.navCtrl.setRoot(NewSales);
    }

}