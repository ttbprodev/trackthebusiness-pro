import { Component, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators, FormControl } from "@angular/forms";
import { DataStore } from "../../../shared/utils/data-store";
import { Service } from "../../../shared/utils/service";
import { NavController, ModalController, NavParams, Platform, DateTime } from "ionic-angular";
import { AddCustomer } from "../../customers/add-customer/add-customer";



@Component({
    selector: 'new-invoice',
    templateUrl: './new-invoice.html'
})

export class Invoice {
    saleId:any;
    billNo;
    salesDate;
    dueDate;
    customerName;
    userName:any;
    grandTotal:any;
    totalCgst:any;
    totalSgst:any;
    posData:any=[];
      constructor(
        public dataStore: DataStore,
        public navCtrl: NavController,
        public modalCtlr: ModalController,
        public navParams: NavParams,
        public plt: Platform) {
            let posData = this.navParams.get('posData');
            this.billNo = posData.billNo;
            this.salesDate=posData.salesDate;
            this.dueDate=posData.dueDate;
            this.customerName=posData.customerName;
            this.userName=posData.userName;
            this.grandTotal=posData.grandTotal;
            this.totalCgst =posData.totalCgst;
            this.totalSgst=posData.totalSgst;
    }
}