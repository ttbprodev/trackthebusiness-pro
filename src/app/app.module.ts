import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule, PopoverController } from 'ionic-angular';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Contacts } from '@ionic-native/contacts';
import { AutoCompleteModule } from 'ionic2-auto-complete';
import { MyApp } from './app.component';
import { TTBHome } from '../features/ttb-home/ttb-home';
import { ProductList } from '../features/products/product-list/product-list';
import { Service } from '../shared/utils/service';
import { DataStore } from '../shared/utils/data-store';
import { BusinessView } from '../features/business/business-view/business-view';
import { LoginPage } from '../shared/components/login/login';
import { AddProduct } from '../features/products/add-product/add-product';
import { NewOrder } from '../features/orders/new-order/new-order';
import { OrderSummary } from '../features/orders/order-summary/order-summary';
import { OrderHistory } from '../features/orders/order-history/order-history';
import { FilterModel } from '../features/orders/filter-model/filter-model';
import { AddUser } from '../features/users/add-user/add-user';
import { ListUsers } from '../features/users/list-users/list-users';
import { SetPassword } from '../shared/components/set-password/set-password';
import { DashboardView } from '../features/dashboard-view/dashboard-view';
import { AddCustomer } from '../features/customers/add-customer/add-customer';
import { CustomerList } from '../features/customers/customer-list/customer-list';
import { CustomerModel } from '../features/customers/customer-model/customer-model';
import { EditUser } from '../features/users/edit-user/edit-user';
import { ExistingUsers } from '../features/users/existing-users/existing-users';
import { CompleteTestService } from '../shared/utils/complete-test-service/complete-test-service';
import { PriceTag } from '../features/orders/price-tag/price-tag';
import { Registration } from '../shared/components/registration/registration';
import { ExistingProduct } from '../features/products/existing-product/existing-product';
import { ExistingCutomers } from '../features/customers/existing-customers/existing-customers';
import { Camera } from '@ionic-native/camera';
import { ProductsCart } from '../features/products/products-cart/products-cart';
import { AddBranch } from '../features/branches/add-branch/add-branch';
import { ChartModule } from 'angular2-highcharts';
import * as highCharts from 'highcharts';
import { BranchesList } from '../features/branches/branches-list/branches-list';
import { BranchProducts } from '../features/branches/branch-products/branch-products';
import { AppHeader } from '../shared/components/header/app-header';
import { AddCategory } from '../features/products/add-category/add-category';
import { AddSubCategory } from '../features/products/add-subcategory/add-subcategory';
import { AddEmployee } from '../features/Employees/add-employee/add-employee';
import { EmployeeList } from '../features/Employees/employee-list/employee-list';
import { PopoverPage } from '../shared/components/header/header-popover/header-popover';
import { SettingPopover } from '../shared/components/header/setting-popover/setting-popover';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NewSales } from '../features/pos/new-pos/new-pos';
import { PosModal } from '../features/pos/pos-modal/pos-modal';
import { from } from 'rxjs/observable/from';
import { Signup } from '../shared/components/signup/signup';
import { Invoice } from '../features/invoice/new-invoice/new-invoice';
import { Reports } from '../features/reports/reports';

import { CalculatorModal } from '../features/pos/calculator-modal/calculator-modal';
import { InvoiceLIst } from '../features/invoice/invoice-list/invoice-list';

 
@NgModule({
  declarations: [
    MyApp,
    TTBHome,
    ProductList,
    AddProduct,
    AddCategory,
    AddSubCategory,
    BusinessView,
    LoginPage,
    NewOrder,
    OrderSummary,
    OrderHistory,
    PriceTag,
    AddProduct,
    AddUser,
    ListUsers,
    EditUser,
    SetPassword,
    DashboardView,
    AddCustomer,
    ExistingCutomers,
    CustomerList,
    CustomerModel,
    FilterModel,
    ExistingUsers,
    Registration,
    ExistingProduct,
    ProductsCart,
    AddBranch,
    BranchesList,
    BranchProducts,
    AppHeader,
    AddEmployee,
    EmployeeList,
    PopoverPage,
    SettingPopover,
    NewSales,
    PosModal,
    Signup,
    Invoice,
    Reports,
    CalculatorModal,
    InvoiceLIst
  ],
  imports: [
    BrowserModule,
    AutoCompleteModule,
    IonicModule.forRoot(MyApp),
    ReactiveFormsModule,
    FormsModule,
    HttpModule,
    ChartModule.forRoot(highCharts),
    NgxDatatableModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    TTBHome,
    ProductList,
    AddProduct,
    AddCategory,
    AddSubCategory,
    BusinessView,
    LoginPage,
    NewOrder,
    OrderSummary,
    OrderHistory,
    PriceTag,
    FilterModel,
    AddUser,
    ListUsers,
    EditUser,
    SetPassword,
    DashboardView,
    AddCustomer,
    CustomerList,
    ExistingCutomers,
    FilterModel,
    CustomerModel,
    ExistingUsers,
    Registration,
    ExistingProduct,
    ProductsCart,
    AddBranch,
    BranchesList,
    BranchProducts,
    AppHeader,
    AddEmployee,
    EmployeeList,
    PopoverPage,
    SettingPopover,
    NewSales,
    PosModal,
    Signup,
    Invoice,
    Reports,
    CalculatorModal,
    InvoiceLIst
  ],
  providers: [
    StatusBar,
    AutoCompleteModule,
    SplashScreen,
    CompleteTestService,
    Service,
    DataStore,
    Camera,
    Contacts,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
  ]
})
export class AppModule {
}
