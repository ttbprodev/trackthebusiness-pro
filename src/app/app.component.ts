import { Component, ViewChild, OnInit } from '@angular/core';
import { Nav, Platform, LoadingController, ToastController, App, MenuController, AlertController, ModalController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { List } from 'ionic-angular';
import 'rxjs/add/operator/map';
import { Service } from '../shared/utils/service';
import { BusinessView } from '../features/business/business-view/business-view';
import { ListUsers } from '../features/users/list-users/list-users';
import { DataStore } from '../shared/utils/data-store';
import { Constants } from '../shared/utils/constants';
import { LoginPage } from '../shared/components/login/login';
import { SetPassword } from '../shared/components/set-password/set-password';
import { DashboardView } from '../features/dashboard-view/dashboard-view';
import { CustomerList } from '../features/customers/customer-list/customer-list';
import { AddUser } from '../features/users/add-user/add-user';
import { Injectable } from '@angular/core';
import { BranchesList } from '../features/branches/branches-list/branches-list';
import { EmployeeList } from '../features/Employees/employee-list/employee-list';
import { ProductList } from '../features/products/product-list/product-list';
import { OrderSummary } from '../features/orders/order-summary/order-summary';
import { NewSales } from '../features/pos/new-pos/new-pos';
import { PosModal } from '../features/pos/pos-modal/pos-modal';
import { Reports } from '../features/reports/reports';
import { InvoiceLIst } from '../features/invoice/invoice-list/invoice-list';
import { Invoice } from '../features/invoice/new-invoice/new-invoice';

@Component({

  templateUrl: './app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  @ViewChild(List) list: List;
  toggle() {
    this.businessItemsShown = !this.businessItemsShown;
  }
  headerTitle: string;
  rootPage: any;
  splitPaneState: boolean;
  pages: Array<{ title: string, icon: any, component: any }>;
  businessNames: Array<any> = [];
  businesslist: Array<any> = [];
  icons: string[];
  businessItemsShown = false;
  productsData: any;
  menuSelect: any;
  businessName: any;
  imageUrl: any;
  empData = 0;
  activePage: any;
  businessId: any;
  email: any;
  empId: any;
  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public service: Service,
    public menuCtrl: MenuController,
    public dataStore: DataStore,
    public loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    public app: App,
    public menu: MenuController,
    private alertCtrl: AlertController,
    public modalCtrl: ModalController, ) {
    this.splitPaneState = false;
    if (location.search != "") {
      this.email = location.search.split('?')[1].split("&")[0].split("emaill=")[1];
      this.empId = location.search.split('?')[1].split("&")[1].split("empId=")[1];
    }
    if (this.email != null && this.email != "" && this.email != undefined && this.empId != null && this.empId != "" && this.empId != undefined) {
      this.dataStore.currentHeaderTitle = "";
      this.rootPage = SetPassword;
    }
    else {
      this.dataStore.currentHeaderTitle = "";
      this.rootPage = LoginPage;
    }
    console.log("Hello, email: " + this.email + " and empId: " + this.empId);

  }

  ngOnInit() {
    // let HeaderTitle2 = this.nav.getActive();
    // this.dataStore.currentHeaderTitle2 = HeaderTitle2.component.name;

  }
  refreshBusinessList() {
    this.getBusinessList();
    this.businessItemsShown = false;
    this.initializeApp();
  }

  getSplitPane() {
    if (this.dataStore.loginMetaData) {
      if (this.platform.width() > 900) {
        this.splitPaneState = true;
      } else {
        this.splitPaneState = false;
      }
    } else {
      this.splitPaneState = false;
    }
    return this.splitPaneState;
  }

  addnewbusiness() {
    this.menuCtrl.close();
    this.nav.setRoot(BusinessView);
  }
  getBusinessList() {
    this.service.getBusinessList().subscribe(
      response => {
        this.dataStore.businessList = response.json();
        this.setImageUrl(this.dataStore.businessList[0]);
      },
      error => {
        alert("Service Error");
        console.log(error);
      });
    this.businesslist = this.dataStore.businessList;
    if (this.activePage) {
      this.nav.setRoot(this.activePage);
    }
    else {
      if (this.businesslist.length > 0) {
        this.dataStore.selectedBusiness = this.dataStore.selectedBusiness ? this.dataStore.selectedBusiness : this.businesslist[0];
        this.nav.setRoot(DashboardView);

      } else {
        this.nav.setRoot(BusinessView);
      }
    }
    let hasBusinessPresent = false;
    for (var i = 0; i < this.businesslist.length; i++) {
      let business = this.businesslist[i];
      if (business.businessId == this.dataStore.selectedBusiness.businessId) {
        hasBusinessPresent = true;
      }
    }
    this.dataStore.selectedBusiness = this.dataStore.selectedBusiness && hasBusinessPresent ? this.dataStore.selectedBusiness : this.businesslist[0];
    this.setImageUrl(this.dataStore.selectedBusiness);
  }


  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();

    });
    this.getBusinessList();
    this.pages = [
      { title: 'Dashboard', icon: 'speedometer', component: DashboardView },
      { title: 'Branches', icon: 'albums', component: BranchesList },
      { title: 'Employees', icon: 'contacts', component: EmployeeList },
      { title: 'Orders', icon: 'albums', component: OrderSummary },
      { title: 'Products', icon: 'cart', component: ProductList },
      { title: 'Contacts', icon: 'logo-usd', component: CustomerList },
      { title: 'Pos', icon: 'logo-usd', component: NewSales },
      { title: 'Invoice', icon: 'chatbubbles', component: InvoiceLIst },
      { title: 'Promotion', icon: 'chatbubbles', component: CustomerList },
      { title: 'Reports', icon: 'chatbubbles', component: Reports },
    ];
    this.activePage = this.pages[0];

  }

  openPage(page) {
    this.nav.setRoot(page.component);
    this.activePage = page;
    if (this.activePage.title == 'Sign Out') {
      this.activePage = '';
      this.signoutConfirm();
    }
    //  else if (this.activePage.title == 'Pos') {
    //   this.itemDetailPosModal();
    // }
    else {
      this.dataStore.currentHeaderTitle = this.activePage.title;
    }
  }
  signoutConfirm() {
    let alert = this.alertCtrl.create({
      title: 'Confirm Signout!',
      message: 'Are you sure want to signout?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.dataStore.currentHeaderTitle = '';
            this.nav.setRoot(LoginPage);
          }
        }
      ]
    });
    alert.present();
  }
  checkActive(page) {
    return page == this.activePage;
  }
  setImageUrl(business) {
    if (business) {
      business["imageUrl"] = "";
      business.image = business.image == "undefined" ? undefined : business.image;
      if (!business.image) {
        business.imageUrl = "../assets/alphabates/" + business.businessName.substr(0, 1) + ".png";
      } else {
        business.imageUrl = Constants.BASE_URL + business.image;
      }
    }
  }
  presentToast(toastMessage: string) {
    let toast = this.toastCtrl.create({
      message: toastMessage,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }
  businessSelect(businessIndex) {
    console.log(businessIndex);
    this.toggle();
    this.setImageUrl(this.dataStore.selectedBusiness);
    this.dataStore.selectedBusiness = this.businesslist[businessIndex];
    this.presentToast(this.dataStore.selectedBusiness.businessName + " selected");
    this.menuCtrl.close();
    this.setImageUrl(this.businesslist[businessIndex]);

  }
  businessEdit(businessIndex) {
    console.log(businessIndex);
    this.toggle();
    this.menuCtrl.close();
    this.setImageUrl(this.businesslist[businessIndex]);
    this.nav.setRoot(BusinessView, { businessId: this.businesslist[businessIndex].businessId });
  }


  // add Users
  addusers(businessId) {
    this.getUsersList(businessId);
    this.menuCtrl.close();
  }
  getUsersList(businessId) {
    this.service.ListingUsers(businessId).subscribe(
      response => {
        this.empData = response.json();
        console.log(this.empData);
        if (this.empData == 0) {
          this.nav.setRoot(AddUser, businessId);
        }
        else {
          this.nav.setRoot(ListUsers, businessId);
        }
      });
  }

  itemDetailPosModal() {
    // if (this.pages[6].title == "pos") {
    let posModal = this.modalCtrl.create(PosModal, { cssClass: "pos-modal" });
    posModal.onDidDismiss(data => {
      console.log(data);
    });
    posModal.present();
    // }

  }

}
